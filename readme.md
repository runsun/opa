# Opa - A SynWrite plugin for OpenSCAD

**Opa** is a [SynWrite](http:www.uvviewsoft.com/synwrite) plugin 
written with python for 
[OpenSCAD](http://www.openscad.org/). 

Key features:

  * **Tab-Arguments**
  * **Realtime user api**
  * **Pop-up Api menu**
  * **Toggle-block**
  
  * **Click-change** 
   
  * **Call-tip**
  * **Tab-aided completion**
  * **PolyGen**
  * **Remote include**
  
  
# Requirements:

  * [OpenSCAD](http://www.openscad.org/)
  * Python 2.7 
  * **SynWrite**: same or newer than *6.19.2165*
    (try: http://uvviewsoft.com/bb/ )
        
## In-Doc UI

**In-Doc UI** (IDU) is an innovative technique to write UI (User 
Iterface) like checkboxes, radioboxes, buttons, etc, in ASCII format 
in the comments of your SCAD code, allowing users to have a set of 
user-defined controls right in the doc. 
                                                         
When your scad file is opened in the **SynWrite**, these controls are linked 
to the *on_click* event handler of the **Opa** python plugin behind the 
**SynWrite**. When clicked, they will: modify the IDU settings, or re-write 
your SCAD code to change parameters (for example, changing `width=3;` to 
`width=4;`), or takes some actions like saving the file, moving to next 
value from a list of parameters, etc. 

Since **OpenSCAD** comes with a feature called *Automatic reload 
and preview* that can be turned on in the menu, every saving of a SCAD 
file in **SynWrite** (if it is also opened in **OpenSCAD**) triggers the 
**OpenSCAD** to have a preview of your code. 
  
The combination of these setups allows for *script parameterization* of 
of your SCAD code, which is a great help to **OpenSCAD** developers. 

For what you need to have this done, see the section **IDU Details**. 
  
For the inspiration, see the future development of **OpenSCAD** in the 
area of *script parameterization* in:

 * https://github.com/openscad/openscad/wiki/Project:-Form-based-script-parameterization
 * https://github.com/openscad/openscad/issues/722#issuecomment-69521189
 * http://files.openscad.org/video/screencast-parameter-window.webm


## IDU Details

The IDU makes use of OpenSCAD block comment: `/*  */` as an IDU block with
a little twick on the `/*` part:

  `/*. blah blah */`
  
  `/*= blah blah */`
  
  `/*^ blah blah */`
  
  or there multiline block versions:
  
  ```/*.
  
     blah
     blah  
     
     */
  ```  
  
The block header could have an id: `/*[id].`, `/*[id]=` or `/*[id]^`
where id is a label.

There are several types of IDU blocks: *Init*, *var*, *multivar*

### The Init block

  The first line of your doc is to set IDU global settings. These
  values are enforced in all IDU blocks unless otherwise specified:
  
  ```
   /*==[idu] [ ]_on [ ]_u_turn [ ]_autosave ==*/
   
  ```
  
  * [ ]_on: checked: all idu blocks are handled
            unchecked: all idu blocks are not handled, means
            mouse click will have no effect. 
  * [ ]_u_turn: 
  * [ ]_autosave: automatic save of doc after click completion
  
### The Var block

  ```          
  width=3; /*..=[3,4,5] {+|-|+3|-2|>|<} ..*/
  
  ```             
  The new value will be set by modifying 'width=3' to 'width=4' (for example) 
  
  ```
     /*..h=[3,4,5] {+|-|+3|-2|>|<} ..*/
  ```
  The new value will be set by writing a line 'h=3;' in the idu data area. 
  
                            
  
                         
  [3,4,5]: list of numbers
  {...}: button(s)
        {+}: click to increase var width by 1
        {-}: click to decrease var width by 1
        {+n}: inc by n
        {-n}: dec by n
        {>}: pick the next val on the right from [3,4,5] 
        {<}: pick the next val on the left from [3,4,5] 
        
         
               
  
## IDU workflow

ctrl-click: turn it on/off. When off: (1) Clicks on idu controls are not
handled; (2) values in idu blocks are not taken.

    