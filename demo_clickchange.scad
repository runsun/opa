//. Tools
module Axes(){
  rLine=0.1;
  color("red",0.3) hull() {
    sphere(rLine); translate([10,0,0]) sphere(rLine);}
  color("green",0.3) hull() {
    sphere(rLine); translate([0,10,0]) sphere(rLine);}
  color("blue",0.3) hull() {
    sphere(rLine); translate([0,0,10]) sphere(rLine);}
}
Axes();
//. Setup
$fn=10;
//. Test  Code

/*render(){*/
  /*hull(){*/
    translate( [5,0,0] ){ 
      color("red") sphere(2);
      } 
    translate( [0,0,0] ) 
      color("green") sphere(2);
    translate( [0,5.0,4] ) 
      color("blue") sphere(2);  
  /*}*/
/*}*/

//. Template

/*
render(){
  hull(){
    translate( [0,0,0] ){ 
      color("red") sphere(2);
      } 
    translate( [0,0,0] ) 
      color("green") sphere(2);
    translate( [0,0,0] ) 
      color("blue") sphere(2); 
  }
}
*/
abcdefA,.1234567890-=q`~
sphere(r=3, d=1, $fa=5, $fs=4, $fn=2);    
cube(size=1, center=True);
cube(size=3, center=True);
sphere(r=3, d=2, $fa=10, $fs=x, $fn=5);|