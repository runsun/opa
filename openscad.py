'''
For OpenSCAD language 
'''

#. Keywords

'''
#######################################################
# Keywords

# Keywords taken from OpenSCAD lexer with modifications:
# - Add "render" to action
# - Move "intersection_for", "linear_extrude", "rotate_extrude" 
#    from CORE to ACTION
# - Create another group: VAR
#
# This is to be loaded by the .setup() of OpenscadCommandMenu class 
# in on_2keys of __init__.py in the following manner:

  def setup(self):
  
    self.pages[0].name = 'Object'
    self.add_radioboxes('obj'
          , opcad.BUILTIN_OBJ_W_HOTKEY, isShowHeader=0)    
    self.pages[0].add_section( 'func_hint', typ='one-item'
    , isShowHeader=1, hasTopLine=1)
    
    self.add_page('Action').add_radioboxes('action'
          , opcad.BUILTIN_ACTION_W_HOTKEY).isShowHeader=0
    self.add_page('Function').add_radioboxes('func'
          , opcad.BUILTIN_CORE_W_HOTKEY).isShowHeader=0

#######################################################
'''
BUILTIN_VAR= ("$fa", "$fn", "$fs", "$t", "$vpd", "$vpr", "$vpt")
BUILTIN_CORE= ("$children", "abs", "acos", "asin", "atan", "atan2", 
"ceil", "children", "chr", "concat", "cos", "cross", "echo", "exp", 
"floor", "len", "let",  "ln", "log", "lookup", "max", "min", "norm", 
"parent_module", "pow", "rands", "round", "search", "sign", "sin", 
"sqrt", "str", "tan", "version", "version_num")  
BUILTIN_ACTION=("color", "difference", "hull", "intersection",
"intersection_for", "linear_extrude","minkowski", "mirror",
"multmatrix", "offset", "projection", "render", "resize", "rotate",
"rotate_extrude", "scale", "translate", "union")  
BUILTIN_OBJ=("circle", "cube", "cylinder", "polygon", "polyhedron"
, "sphere", "square", "text") 

BUILTIN_CORE_W_HOTKEY= ("$children", "&abs", "&acos", "&asin", "&atan", "&atan2", 
"&ceil", "&children", "&chr", "&concat", "&cos", "&cross", "&echo", "e&xp", 
"&floor", "&len", "&let",  "&ln", "&log", "&lookup", "ma&x", "&min", "&norm", 
"&parent_module", "po&w", "rands", "&round", "&search", "&sign", "&sin", 
"s&qrt", "&str", "&tan", "&version", "version_n&um")
BUILTIN_ACTION_W_HOTKEY=("&color", "&difference", "&hull", "&intersection",
"intersection_&for", "&linear_extrude","min&kowski", "&mirror",
"multmatri&x", "&offset", "r&ender", "resi&ze", "&rotate",
"rot&ate_extrude", "&scale", "&translate", "&union")  
BUILTIN_OBJ_W_HOTKEY=("&circle", "c&ube", "c&ylinder", "&polygon", "poly&hedron"
, "&sphere", "s&quare", "&text")    

