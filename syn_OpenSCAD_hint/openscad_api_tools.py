'''
openscad_api_tools.py

This is an OpenSCAD api managing tool, designed to be used 
by SynWrite editor as a plug-in to display calltip to the 
output panel in SynWrite. It's imported by the plugin's 
__init__.py (in the same folder) in the following manner:

  import sys, os
  from sw import *

  this_dir = os.path.dirname(os.path.abspath(__file__))
  sys.path.insert(0, this_dir)
  import openscad_api_tools as opapi

  class Command:
    def on_key(self, ed_self, key, state):
       
      if state=="s" and key==57:  # when "(" is entered
      
        xy = ed.get_caret_xy()
        word_data = ed.get_word(xy[0], xy[1] )  #=>(offset, len, word)
        offset = word_data[0]
        unitname = word_data[2]               # func or module name
        api= opapi.get_api(unitname)
        app_log( LOG_ADD, api )

by: Runsun Pan 2016.01
'''


# Used in self.get_consistent_typename(t)
TYPE_MAP = ( 
  # For utype
   ( 's', ('s','str','Str','STR', 'STRING','string', 'String') )
  ,( 'i', ('i','int','Int','INT', 'integer', 'Integer','INTEGER') )
  ,( 'n', ('n','num','Num','NUM', 'number', 'Number','NUMBER') )
  ,( 'li', ('l','li','Li','LI', 'list','List','LIST') )
  ,( 'b', ('b','B','bool','Bool','BOOL','boolean','Boolean','BOOLEAN') )
  # For cat
  ,( 'func', ('f','func','Func','FUNC','function','Function', 'FUNCTION') )
  ,( 'mod',('m','mod','MOD','Mod','module','Module','MODULE'))
  ,( 'var',('v','var','Var','VAR','variable','Variable','VARIABLE'))
  )

class Unit(dict):
  ''' Unit is a dict wrapper. It could be for functions, modules
      or variables. Variables include the function's or module's
      argument. 
  
        size = Unit( name='size', cat='var', optional_args) 
      
      Arguments 'name' and 'cat' are required. 
      
      Update data with either way:
        
        size.name = 'new name'
        size['name'] = 'new name'
        
      Clone:
      
        size2 = size()
          
      Clone with new data :
      
        size3 = size(default=2)  
        
  '''
  def __init__( self
              , name
              , cat # var|func|mod
              , utype= None  # "#","i","f","s","l" or "#|i|f", etc  
              , optional= True  # True|False
              , default=None 
              , tags=[]
              , args=[] # list of Unit instances 
              , ret= None
              , ver = None
              , rel=[]  # related, list of str
              , hint=''
              , doc=''
              ):
      loc = locals()
      for k in loc.keys(): 
        if k!='self': self.__dict__[k] = loc[k]
  
  def __setitem__(self,k,v): 
    self.__dict__[k]= self.get_consistent_typename(k,v)
    
  def __getitem__(self,k): return self.__dict__[k]

  def __setattr__(self,k,v): 
    self.__dict__[k]= self.get_consistent_typename(k,v)
  
  def __getattr__(self,k):
    if k=='attr_names':
      return ['name', 'cat', 'utype', 'tags', 'optional', 'default'
                      , 'hint', 'doc','args','ret', 'ver', 'rel']
    else:
      return self.__dict__[k]


  def __str__(self):    
    return ('{'+ ', '.join( [ '"%s": %s'%(k, self.__dict__[k])
                             for k in self.attr_names ] ) 
            +'}')   
             #for k in ['name', 'cat', 'utype', 'tags', 'optional', 'default'
             #         , 'hint', 'doc','args','ret', 'ver', 'rel'] ] ) +'}'
                
  def __repr__(self): return self.__str__()           
                         
  def get_arg_str(self): # for cat="var"
    '''
        sphere= Unit( name='sphere'
                    , args=[ common_units('r')(default=1)
                           , common_units('d')
                           ]
                    )
        sphere.args[0].get_arg_str()
        ==> "r:num= 1"
    '''    
    s = '%(argname)s:%(type)s%(df)s'
    df = self.default
    arg_str= s%( { "argname": self.name #arg.get("name")
               , "type" : self.utype #arg.get("type")
               , "df" : df!=None and ("=" + str(df)) or ""
               }
             )
    return arg_str          
        
  def get_arg_by_name(self, argname):
      return [ arg for arg in self.args if arg.name==argname ][0]
      
  def get_consistent_typename(self,k,v):
      if (k=='utype' or k=='cat') and v!=None:
        if len(v.split('|')) > 1:
          return '|'.join(
                  [ self.get_consistent_typename(k,vv)
                    for vv in v.split('|')
                  ] )   
        for kk,vs in TYPE_MAP:
          if v in vs:
            return kk
      else:
        return v  
          
  def get_args_str(self): # for cat="func"|"mod"
    ''' 
        sphere= Unit( name='sphere'
                    , args=[ common_units('r')(default=1)
                           , common_units('d')
                           ]
                    )
        sphere.get_args()          
        ==> "r:num= 1, d:num"
        
        sphere.args[0].as_arg_str()
        ==> "r:num= 1"
        
        tests:
        func1= Func('func1')
        print('func1= ', func1)
        print('func2= ', func1(name='func2'))
        print('func3= ', func1(name='func2')(name='func3'))
    '''    

    args= self.args
    return ", ".join( [ arg.get_arg_str() for arg in args ] )

  def get_api_str(self):
    s = "%(unitname)s( %(args)s )%(ret)s"
    rtn = self.cat=="func" and ("=> " + self.ret) or ""
    return s%( { "unitname": self.name
               , "args": self.get_args_str()
               , "ret": rtn
               }
             )  
  
  def __call__(self, name='', cat='', **kargs):
    '''
        Make the instanse callable to create a clone with new data:
        
         r = Unit(name="r",default=1)
         r2 = r(name="r2",default=2)
         r3 = r(name="r3")(default=3)
         
       More tests:
        func1= Func('func1', tags=["xxx"])
        func2 = func1(name='func2')
        func3 = func2('func3')
        func4 = Func('func4-0')(ver=1)(name='func4-1')
        print('\n>>> func1= ', func1)
        print('\n>>> func2= ', func2)
        print('\n>>> func3= ', func3)
        print('\n>>> func1= ', func1)
        print('\n>>> func4= ', func4)
        func4.name= 'func4-2'
        print('\n>>> func4= ', func4)
        func4['name']= 'func4-3'
        print('\n>>> func4= ', func4)  
         
    '''   
    #    print( '\n----------\n'
    #        , 'in __call__, self.name= ', self.name
    #        , ', self.tags= ', self.tags
    #        , '\n' ) 
    #loc = locals()
    #del(loc["self"])   # all args given except 'self'
    
    u = Unit( name, cat ) # Create a new Unit
    
    for k in self.attr_names:   # Copy all values from self 
      u[k] = self.__dict__[k]
      
    for k in kargs.keys(): # Update new data from arguments
      if kargs[k]!=None:
        u[k] = kargs[k]

    #print( 'u after update = ', u)
           
    return u              

def Var( name, **kargs ): 
 
    u= Unit(name,cat='')
    for k,v in kargs.items():
        u[k] = v
    u.cat= 'var'
    u.args= None
    u.ret = None 
    return u

def Num( name, **kargs ): return Var(name, **kargs)(utype='num')
def Bool( name, **kargs ): return Var(name, **kargs)(utype='bool')
def Li( name, **kargs ): return Var(name, **kargs)(utype='list')
     
def Func( name, **kargs ): 
 
    u= Unit(name,cat='')
    for k,v in kargs.items():
        u[k] = v
    u.cat= 'func'
    u.optional = None
    u.default = None
    return u

def Mod( name, **kargs ):  

    u= Unit(name,cat='')
    for k,v in kargs.items():
        u[k] = v
    u.cat= 'mod'
    u.utype= None
    u.optional = None
    u.default = None
    u.ret = None
    return u
  

#
# openscad_api is defined in openscad_api_data.py, which 
# requires the tools above, so we import it only after 
# the above tools are defined
#
from openscad_api_data import *
  
def get_openscad_api( unitname ):
  if unitname in openscad_api.keys():
    unit = openscad_api[ unitname ]   
    #print( '/// In get_api(), get unit "'+unit.name+'" : ', str(dir(unit)) )
    return openscad_api[ unitname ].get_api_str()
  
