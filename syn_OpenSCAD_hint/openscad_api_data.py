'''
openscad_api_data.py

This is an OpenSCAD api data, to be used by openscad_api_tools.py
in SynWrite editor as a plug-in to display calltip.

by: Runsun Pan 2016.01
'''
from openscad_api_tools import *

common_units={

  "r":    Var( name="r"
              , utype= "num"  # str|list  
              , optional=True  # True|False
              , default= None 
              , hint='Radius of object'
              )
  ,"d":   Var( name="d"
              , utype= "num"  # str|list  
              , optional=True  # True|False
              , default= None 
              , hint='Diameter of object. Overwrite r.'
              )
  ,"d":   Var( name="d"
              , utype= "num"  # str|list  
              , optional=True  # True|False
              , default= None 
              , hint='Diameter of object. Overwrite r.'
              )
  ,"h":   Var( name="h"
              , utype= "num"  # str|list  
              , optional=True  # True|False
              , default= None 
              , hint='Height of object.'
              )
  ,"center": Var( name= 'center'
               , utype= 'bool'
               , optional = True 
               , default = 'false'
               , hint= "Set true to center the obj at (0,0,0)"
               , doc =''    
               ) 

  ,"$fs": Var( name = "$fs"
            , utype =  "num"
            , tags= ['Special Variable']
            , optional= True   # True|False
            , default= 2
            , rel = ['$fs','$fn']
            , hint = "Special variable for the minimum size of a fragment."
            , doc = '''Because of this variable very small circles have a smaller number of fragments than specified using $fa. The default value is 2. The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
            )
            
  ,"$fn": Var( name = "$fn"
            , utype =  "num"
            , tags= ['Special Variable']
            , optional= True   # True|False
            , default= 0
            , rel = ['$fs','$fn']
            , hint = "Special variable controlling the number of fragments." 
            , doc = '''Usually 0. When this variable has a value greater than zero, the other two variables are ignored and full circle is rendered using this number of fragments. The default value is 0.'''
            )
}    

fa = Num( name = "$fa"
        , tags= ['Special Variable', 'angle']
        , optional= True   # True|False
        , default= 12
        , rel = ['$fs','$fn']
        , hint="Special variable for the minimum angle of a fragment."
        , doc='''Even a huge circle does not have more fragments than 360 divided by this number. The default value is 12 (i.e. 30 fragments for a full circle). The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
        )
            
fs = Num( name = "$fs"
        , tags= ['Special Variable']
        , optional= True   # True|False
        , default= 2
        , rel = ['$fa','$fn']
        , hint = "Special variable for the minimum size of a fragment."
        , doc = '''Because of this variable very small circles have a smaller number of fragments than specified using $fa. The default value is 2. The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
        )
fn = Num( name = "$fn"
        , tags= ['Special Variable']
        , optional= True   # True|False
        , default= 0
        , rel = ['$fs','$fa']
        , hint = "Special variable controlling the number of fragments." 
        , doc = '''Usually 0. When this variable has a value greater than zero, the other two variables are ignored and full circle is rendered using this number of fragments. The default value is 0.'''
        )     
center = Bool( name= 'center'
               , optional = True 
               , default = 'false'
               , hint= "Set true to center the obj at (0,0,0)"
               , doc =(
                  'false: 1st (positive) octant, one corner at (0,0,0)',)
               )           
                    
openscad_api={

  "cube": 
    Mod( name='cube'
       , tags='obj, 3d'
       , args=[
            Var( name= 'size'
               , utype= 'list|num'
               , optional = True 
               , default = [1,1,1]
               , hint= 'Size of the object'
               , doc= (
                    'single value, cube with all sides this length;' 
                   ,'3 value array [x,y,z], cube with dimensions x,y,z.'
                   )
               )
            , center
#           ,Bool( name= 'center'
#               , optional = True 
#               , default = False
#               , hint= "Set true to center the obj at (0,0,0)"
#               , doc =(
#                  'false: 1st (positive) octant, one corner at (0,0,0)',)
#               ) 
        ]        
      ) #cube

  ,"sphere": 
    Mod( name='sphere'
       , tags='obj, 3d'
       , args=[
                #common_units['r']( default=1 )
                Num('r', default=1)
              , Num('d') #common_units['d']#( default=1 )
              , fa #common_units["$fa"]
              , fs #common_units["$fs"]
              , fn #common_units["$fn"]
              ]        
      ) #sphere  
  , "cylinder":
    Mod( name='cylinder'
       , tags='obj, 3d'
       , args=[
                Num('h', default=1)
              , Num('r1', default=1) 
              , Num('r2', default=1) 
              , Num('r') 
              , Num('d') 
              , Num('d1') 
              , Num('d2') 
              , center #Bool('center') #common_units['center']
              , fa
              , fs #common_units["$fs"]
              , fn #common_units["$fn"]
              ]        
      ) #cylinder
  , "square":
    Mod( name= 'square'
        ,tags= 'obj,2d'
        ,args= [Var( name= 'size'
                   , utype= 'list|num'
                   , optional = True 
                   , default = [1,1]
                   , hint= 'Size of the object'
                   , doc= (
                        'single value, cube with all sides this length;' 
                       ,'3 value array [x,y,z], cube with dimensions x,y,z.'
                       )
                   )
                , center     
                ]
       ) #square        
  , "scale":
    Mod("scale"
        , args=[ Li('v', default=[1,1,1]) ]
        )
  , "resize":
    Mod("resize"
        , args=[ Li('newsize', default=[1,1,1]) ]
        )        
  , "rotate":
    Mod("rotate"
        , args=[ Num('a'), Li('v') ]
        )
  , "translate":
    Mod("translate"
        , args = [ Li('v') ]
        )
  , "mirror":
     Mod("mirror"
         ,args = [Li( 'v') ]                     
        )  
} 

