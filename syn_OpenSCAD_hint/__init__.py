'''  
  SynWrite plugin for OpenSCAD function hint
  
  By Runsun Pan, 2016/01
  
  
# Py api categories
  http://sourceforge.net/p/synwrite/wiki/python%20API/
# py api funcs home page
  http://sourceforge.net/p/synwrite/wiki/python%20API%20functions/  
# py func dialog
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20dialog/  
# py func misc
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20misc/  
# Install python plugin:
  http://sourceforge.net/p/synwrite/wiki/Plugins%20manual%20installation/ 
# Py event handler:
  http://sourceforge.net/p/synwrite/wiki/py%20event%20names/  
# app_log(id,..) : id constant
  http://sourceforge.net/p/synwrite/wiki/py%20log%20id/   
# msg_box(id) constant
  http://sourceforge.net/p/synwrite/wiki/py%20msgbox%20id/
# Py Editor class
  http://sourceforge.net/p/synwrite/wiki/py%20Editor%20class/
# SynWrite addon repos
  http://sourceforge.net/projects/synwrite-addons/files/PyPlugins/  
        
'''

from sw import *

import os
this_dir = os.path.dirname(os.path.abspath(__file__))
#folder_Settings = sw.app_ini_dir()  # => "...\SynWrite\Setting"
import sys
sys.path.insert(0, this_dir)
import openscad_api_tools as opapi


KEY_SHIFT = 's16'
KEY_CTRL  = 'c17'
KEY_ALT   = 'a18'
KEY_ENTER = '13'
KEY_TAB   = '9'
KEY_CAP   = '20'
KEY_EQUAL = '187'  # =
KEY_APOS  = '222'  # '  (apostrophe)
KEY_QUOTE = 's222' # "
KEY_BRACE   = 's219' # {
KEY_BRACE_C = 's221' # }
KEY_BRACKET  = '219'  # [
KEY_BRACKET_C= '221'  # ]
KEY_PAREN    = 's57'  # (  (parenthesis)
KEY_PAREN_C  = 's48'  # )

BLOCKS= { KEY_APOS: KEY_APOS
        , KEY_QUOTE: KEY_QUOTE
        , KEY_BRACE: KEY_BRACE_C
        , KEY_BRACKET: KEY_BRACKET_C
        , KEY_PAREN: KEY_PAREN_C
        }

BLOCK_OPENERS = BLOCKS.keys()
BLOCK_CLOSERS = BLOCKS.values()
                
unitName = ''   # mod or func name
unitApi = None  # An instance of Unit class

isInArg= False
inArgBlocks = []  # Start counting blocks ONLY when isInArg.
'''      
              isInArg  blocks
  abc           False   []
  abc(          True    []
  abc(s="       True    ['s']   // in string block
  abc(s="x"     True    []      // left string block
  abc(a=[       True    ['[']   // in [ ] 
  abc(a=["      True    ['[','s']   // in [ ] and string blocks   

'''
def reduceBlocks(): 
  global inArgBlocks
  inArgBlocks= inArgBlocks[:-1]
  
class Command:
  def on_click(self, ed_self, state):   

    pass  
    # Need version 6.19.2165. As of 1/8/2016, 
    # download from http://uvviewsoft.com/bb/   
    
#    xy = ed.get_caret_xy()
#    #print( 'Clicked! state = %s, xy = '%(state,xy) )
#    word_data = ed.get_word(xy[0], xy[1] )  #=>(offset, len, word)
#    offset = word_data[0]
#    word = word_data[2]  
#    print( 'Clicked! line/col=%s/%s, state="%s", word=%s'%(xy[1],xy[0],state,word_data))
  
  def on_key(self, ed_self, key, state):
    #pass      
       
  #def old_on_key(self):
    # For debug:
    # entered = "Entered: state= %s, key= %s, chr= %s"%(state, key, chr(key) )
    # app_log( LOG_ADD, entered)

    global isInArg
    global inArgBlocks
    k = str(state) + str(key)
    
    if isInArg:
      
      if k in (KEY_QUOTE,KEY_APOS):
        
        if inArgBlocks and k == inArgBlocks[-1]: reduceBlocks()
        else:  inArgBlocks.append( k )
          
      elif k in BLOCK_OPENERS:
        
        inArgBlocks.append( k )
        
      elif k in BLOCK_CLOSERS:
      
        if inArgBlocks and k==BLOCKS[inArgBlocks[-1]]:
              reduceBlocks()
        elif not inArgBlocks and k==KEY_PAREN_C :
          isInArg = False
          
#      app_log( LOG_ADD, 
#             "k= %s, isInArg=%s, inArgBlocks=%s"%(k,isInArg,inArgBlocks)
#             )    
    
    elif k == KEY_PAREN:
    
      global unitName, unitApi
      
      isInArg = True
      xy = ed.get_caret_xy()
      word_data = ed.get_word(xy[0], xy[1] )  #=>(offset, len, word)
      offset = word_data[0]
      unitName = word_data[2]               # func or module name
      #app_log( LOG_ADD, '"%s" at %s'%(word, offset) )
      #app_log( LOG_ADD, str( openscad_api[word] ) )
      api= opapi.get_openscad_api(unitname)
      if api: 
        unitApi = api
        app_log( LOG_ADD, str(api) )
      #if word in oapi.openscad_api.keys():
      #  app_log( LOG_ADD, oapi.get_unit_api( unitname= word ) )
      
        
    app_log( LOG_ADD, 
           "k= %s, isInArg=%s, inArgBlocks=%s"%(
              k,isInArg,inArgBlocks )
           )     
  
       



  
  










