
'''
Design an AscUI in-doc control for OpenSCAD:
(to be used in on_caret_move() event handler in SynWrite)

An ascui block:  //.> ... //<

  { }_Label   // a checkBox
  ( )_Label   // a radioBox
  [_Label_]   // a button
  [_x|y|z_]   // collection of buttons
  [ ]         // a textbox
        
//.>
 
 { } Save_Upon_Change
 ( )

//<

2. 
'''
import re, pprint, doctest, collections

import os,sys
this_dir = os.path.dirname(os.path.abspath(__file__))
#folder_Settings = sw.app_ini_dir()  # => "...\SynWrite\Setting"
sys.path.insert(0, this_dir)
from ascui_define import *
#from openscad_parser import *
import openscad_parser as o_p
tokenize = o_p.tokenize 

IS_DEBUG= 0

def conout(*args):
  global IS_DEBUG
  if IS_DEBUG: 
    args = ' '.join( [ str(x) for x in args] )
    print(args) 
  

class LastUpdatedOrderedDict(collections.OrderedDict):
    '''Store items in the order the keys were last added
       https://docs.python.org/2/library/collections.html
    '''                      
    def __setitem__(self, key, value):
        if key in self:
            del self[key]
        OrderedDict.__setitem__(self, key, value)
        
class Options(collections.OrderedDict):
    '''  
    
    '''                      
    def __setitem__(self,k,v): 
      words=('cb_','rb_','bt_')
      if not k[:3] in words:
        raise KeyError(
              ('Option name in InDocUi.options must starts with '+
              'one of %s.'%( ','.join('"%s"'%x for x in words)) + 
              ' You gave: "%s"'%k)
           )
      else:
        #self.__dict__[k]=v  
        collections.OrderedDict.__setitem__(self, k,v)
           
    def __getitem__(self,k): 
      words=('cb_','rb_','bt_')
      if not k[:3] in words:
        raise KeyError(
              ('Option name in InDocUi.options must starts with '+
              'one of %s.'%( ','.join('"%s"'%x for x in words)) + 
              ' You gave: "%s"'%k)
           )
      else:
        return collections.OrderedDict.__getitem__(self, k)                          
    
    def __repr__(self):
      '''
        Return something like:
        'cb_on=1, cb_autosave=1, cb_uturn=0'
      '''  
      
      s = ', '.join(  '%s=%s'%(x,y) for x,y in self.items() )
      
#      s =(collections.OrderedDict.__repr__(self)
#          .replace("Options([('",'')
#          .replace("', ", ",")
#          .replace("', ", ",")
#          .replace(')])','')
#         ) 
      return s    
                                    
      
class InDocUi(object):
  def __init__(self, idu_str='', ed=None, **kargs):
    #d= self.__dict__
    #setattr(d, 'options', collections.OrderedDict() )
    self.options = Options() #collections.OrderedDict() 
    self.data = collections.OrderedDict() 
    #d.data= LastUpdatedOrderedDict() 
  def on(self, val=1):
      self.options['cb_on']= val
      
    
  #def load(self, idu_str):
        

    
def toggle_box(ed, callback=None, callback_scope={}, idu=None): 
  '''  
    To be called by on_click() to check if the caret falls within
    a checkbox or radiobox. If yes, toggle the checkmark or radiomark
    
  '''             
  #conout( 'Entered toggle_box(), idu=', idu )
  doc = ed.get_text_all() 
  
  if doc :
    i= ed.get_caret_pos() -1     
    #nC,nL =  ed.get_caret_xy()   
    #nC= nC-1
       # The offset index obtained by get_caret_pos() points
      # to the char after the caret, so to obtain the one
      # before the caret, needs to -1 
    #conout ('doc[i]="%s"'%doc[i])              
    def toggle(i, chkmark):
      #conout ('Entered toggle()')
      #txt = doc[i] 
      #conout ('\ni = %s, doc[i] = txt= "%s"'%(i,txt))  
      if i>=0:
        newval = doc[i]==' ' and chkmark[-1] or ' '
        ed.replace( i,1, newval)
          # If chkmark is a RADIO_MARK which is set to r'\*', we need
          # to get the last char, so chkmark[-1]  
        if chkmark == CHK_MARK:   
          #nC, nL = ed.get_caret_xy()
          #varname = ed.get_text_line(-1)[ nC+2: ].split(' ')[0]
          varname = doc[ i+3: ].split(' ')[0] 
          conout( 'varname="%s", newval = "%s"'%(varname,newval) )
          idu.options['cb_'+varname] = (newval!=' ' and 1) or 0  
        if callable(callback):
          call(callback, callback_scope)
      ed.set_sel(i+3,0)
                   
    case1= ('\\'+doc[i], '\\'+doc[i+2]) # ( _)
    case2= ('\\'+doc[i-1],'\\'+doc[i+1]) # (_ )
    #conout( 'case1,2 = "%s", "%s"'%(case1, case2))
    #conout( 'doc[i-1:i+3] = "%s"'%doc[i-1:i+3] )
    if case1==CHK_BOX: toggle(i+1,CHK_MARK)          
    elif case2==CHK_BOX: toggle(i,CHK_MARK)          
    elif case1==RADIO_BOX: toggle(i+1,RADIO_MARK)          
    elif case2==RADIO_BOX: toggle(i,RADIO_MARK)          
      
#    cases = ('( )','[ ]', '(x)','[x]')
#    #    if case1 in cases: 
#    #      conout(' case1 : ', case1)
#    #    elif case2 in cases:
#    #      conout(' case2 : ', case2)
#      
#    i = (
#        (case1 in cases and (i+1))  
#        or ((case2 in cases and i))
#        or -1
#        )
#        
#    txt = doc[i] 
#    #conout ('\ni = %s, doc[i] = txt= "%s"'%(i,txt))  
#    if i>=0:
#      ed.replace( i,1, doc[i]==' ' and 'x' or ' ')
#      if callable(callback):
#        call(callback, callback_scope)


  



import collections

    
    
#def tokenizer(s, token_set=SCANNER_RULES['IDU_BLK'] ):
#    r'''
#    # Following:
#    # https://docs.python.org/3.2/library/re.html#writing-a-tokenizer
#    #                 
#                  01234567890123456789012345678901234567890123456789
#    >>> blk_str= '/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/'
#    
#    -----------------------------------------------------
#    
#    >>> tk_blk = SCANNER_RULES['IDU_BLK']
#    >>> [x[0] for x in tk_blk]  # doctest: +NORMALIZE_WHITESPACE 
#    ['IDU_BLK', 'SKIP']
#        
#    >>> tks= tokenizer( blk_str, tk_blk ) 
#    
#    >> tks #doctest: +ELLIPSIS 
#    <generator object tokenizer at 0x...> 
#    
#    >>> tks=list(tks)   
#    >>> tks # doctest: +NORMALIZE_WHITESPACE
#    [Token(typ='IDU_BLK', 
#           value='/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/', 
#           line=1, col=0)]
#                              
#    -----------------------------------------------------
#           
#    >>> tk_def = SCANNER_RULES['IDU_PART']  
#    
#    >>> [x[0] for x in tk_def]    # doctest: +NORMALIZE_WHITESPACE
#    ['NUM_LIST', 'RADIO_DATA', 'RADIO_BOX', 'CHK_BOX', 
#    'BTN', 'SKIP', 'IDU_BEG', 'IDU_END']     
#    
#    >>> tks_content = list( tokenizer(blk_str, tk_def ) )
#    >>> tks_content # doctest: +NORMALIZE_WHITESPACE
#    [Token(typ='NUM_LIST', value='[ 3, -4, 5.5 ]', line=1, col=4), 
#     Token(typ='BTN', value='{+|-|>}', line=1, col=18), 
#     Token(typ='CHK_BOX', value='[ ]_autosave', line=1, col=25), 
#     Token(typ='CHK_BOX', value='[x]_on', line=1, col=38)] 
#    
#    -----------------------------------------------------
#    String containing multiple idu blocks:
#    
#    >>> s = "/*= [2,3]*/width=3.5; /*.[ ]_autosave*/"
#    >>> list( tokenizer(s, tk_def ) ) # doctest: +NORMALIZE_WHITESPACE
#    [Token(typ='NUM_LIST', value='[2,3]', line=1, col=4), 
#     Token(typ='CHK_BOX', value='[ ]_autosave', line=1, col=25)]                 
#    
#    -----------------------------------------------------
#    String containing multiple idu blocksin multiline. --- NOTE that 
#    tokenizer() is unable to tell there's a '\n' in the given string. 
#    This will be handled by the idu_tokenize():
#    
#    >>> s2 = """/*= [2,3]*/width=3.5;
#    ... b=3; 
#    ... c=4; /*.[ ]_autosave*/ xxx"""
#    
#    >>> list( tokenizer(s2, tk_def ) ) # doctest: +NORMALIZE_WHITESPACE
#    [Token(typ='NUM_LIST', value='[2,3]', line=1, col=4), 
#     Token(typ='CHK_BOX', value='[ ]_autosave', line=1, col=36)]               
#     
#    -----------------------------------------------------
#    String containing idu block that spans multiple line 
#           
#    >>> s3 = r"""/*= [2,3]*/width=3.5; /*.[ ]_autosave
#    ... $c=-34;[ 2,3,4 ]{+|-|>} [x]_on*/ xxx"""
#    
#    >>> list( tokenizer(s3, tk_def ) ) # doctest: +NORMALIZE_WHITESPACE
#    [Token(typ='NUM_LIST', value='[2,3]', line=1, col=4), 
#     Token(typ='CHK_BOX', value='[ ]_autosave', line=1, col=25), 
#     Token(typ='NUM_LIST', value='[ 2,3,4 ]', line=1, col=45), 
#     Token(typ='BTN', value='{+|-|>}', line=1, col=54), 
#     Token(typ='CHK_BOX', value='[x]_on', line=1, col=62)] 
#        
#    -----------------------------------------------------
#    The function, idu_tokenize(), return different format:
#     
#    > tks_dict= idu_tokenize( blk_str, token_set=tk_def )
#    > tks_dict # doctest: +NORMALIZE_WHITESPACE
#    {'CHK_BOX': [(1, 25, 1, 36, '[ ]_autosave'), (1, 38, 1, 43, '[x]_on')], 
#     'NUM_LIST': [(1, 4, 1, 17, '[ 3, -4, 5.5 ]')], 
#     'BTN': [(1, 18, 1, 24, '{+|-|>}')]}         
#      
#    '''  
#    Token = collections.namedtuple('Token', ['typ', 'value', 'line', 'col'])
#
#    keywords = {} #'//='} #, 'THEN', 'ENDIF', 'FOR', 'NEXT', 'GOSUB', 'RETURN'}
#    #token_set = SCANNER_RULES['IDU_BLK']  
#    #conout( 'token_set = ', token_set )
#    token_specification = token_set 
#    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
#    get_token = re.compile(tok_regex).match
#    line = 1
#    pos = line_start = 0
#    mo = get_token(s)
#    while mo is not None:
#        typ = mo.lastgroup
#        if typ == 'NEWLINE':
#            line_start = pos
#            line += 1
#        elif typ != 'SKIP':
#            val = mo.group(typ)
#            if typ == 'ID' and val in keywords:
#                typ = val
#            yield Token(typ, val, line, mo.start()-line_start)
#        pos = mo.end()
#        mo = get_token(s, pos)
#    if pos != len(s):
#        raise RuntimeError('Unexpected character %r on line %d' %(s[pos], line))
 
#TK_RTN_LIST=0
#TK_RTN_BY_TOKEN=1
#TK_RTN_BY_RNG =2                                       
        
def tokenize2 (s, token_name='IDU_BLK'): #, return_mode= TK_RTN_BY_RNG  ):
    r'''
    # Following:
    # https://docs.python.org/3.2/library/re.html#writing-a-tokenizer
    #                 
                  01234567890123456789012345678901234567890123456789
    >>> blk_str= '/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/'
    
    -----------------------------------------------------
    Find idu blocks:
    
    >>> tk_blk = SCANNER_RULES['IDU_BLK']
    >>> [x[0] for x in tk_blk]  # doctest: +NORMALIZE_WHITESPACE 
    ['IDU_BLK', 'SKIP']
        
    >>> tks= tokenize( blk_str, 'IDU_BLK' ) 
    
    >> tks #doctest: +ELLIPSIS 
    <generator object tokenizer at 0x...> 
    
    >>> list(tks) # doctest: +NORMALIZE_WHITESPACE
    [Token(typ='IDU_BLK', rng=(0, 48), rel_rng=(0, 0, 0, 48), 
       txt='/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/')]
                         
    -----------------------------------------------------
    Same string, find idu parts:    
       
    >>> [x[0] for x in SCANNER_RULES['IDU_PART'] ] # doctest: +NORMALIZE_WHITESPACE
    ['NUM_LIST', 'RADIO_DATA', 'RADIO_BOX', 'CHK_BOX', 
    'BTN', 'SKIP', 'IDU_BEG', 'IDU_END']     
    
    >>> tks_content = list( tokenize(blk_str, 'IDU_PART' ) )
    >>> tks_content # doctest: +NORMALIZE_WHITESPACE 
        [Token(typ='NUM_LIST', rng=(4, 18), rel_rng=(0, 4, 0, 18), 
               txt='[ 3, -4, 5.5 ]'), 
         Token(typ='BTN', rng=(18, 25), rel_rng=(0, 18, 0, 25), 
               txt='{+|-|>}'), 
         Token(typ='CHK_BOX', rng=(25, 37), rel_rng=(0, 25, 0, 37), 
               txt='[ ]_autosave'), 
         Token(typ='CHK_BOX', rng=(38, 44), rel_rng=(0, 38, 0, 44), 
               txt='[x]_on')]
        
    >>> rng = tks_content[-1].rng
    >>> rng
    (38, 44)
    >>> blk_str[rng[0]:rng[1]]
    '[x]_on'
            
                  
    -----------------------------------------------------
    String containing multiple idu blocks:

             01234567890123456789012345678901234567890123456789
    >>> s = "/*= [2,3]*/width=3.5; /*.[ ]_autosave*/"
    >>> list( tokenize(s, 'IDU_PART' ) ) # doctest: +NORMALIZE_WHITESPACE
    [Token(typ='NUM_LIST', rng=(4, 9), rel_rng=(0, 4, 0, 9), txt='[2,3]'), 
     Token(typ='CHK_BOX', rng=(25, 37), rel_rng=(0, 25, 0, 37), txt='[ ]_autosave')]
                  
    >>> list( tokenize(s, 'IDU_BLK' ) ) # doctest: +NORMALIZE_WHITESPACE
    [Token(typ='IDU_BLK', rng=(0, 11), rel_rng=(0, 0, 0, 11), 
           txt='/*= [2,3]*/'), 
     Token(typ='IDU_BLK', rng=(22, 39), rel_rng=(0, 22, 0, 39), 
           txt='/*.[ ]_autosave*/')]
         
    -----------------------------------------------------
    String containing multiple idu blocks in multiline. 
    
                01234567890123456789012345678901234567890123456789
    >>> s2 = """/*= [2,4]*/width=3.5;
    ... b=3; 
    ... c=4; /*.[ ]_autosave*/ xxx"""
    
        01234567890123456789012345678901234567890123456789

    >>> list( tokenize(s2, 'IDU_PART' ) ) # doctest: +NORMALIZE_WHITESPACE
    [Token(typ='NUM_LIST', rng=(4, 9), rel_rng=(0, 4, 0, 9), txt='[2,4]'), 
     Token(typ='CHK_BOX', rng=(36, 48), rel_rng=(2, 8, 2, 20), txt='[ ]_autosave')]
    
    >>> list( tokenize(s2, 'IDU_BLK' ) ) # doctest: +NORMALIZE_WHITESPACE
    [Token(typ='IDU_BLK', rng=(0, 11), rel_rng=(0, 0, 0, 11), 
           txt='/*= [2,4]*/'), 
     Token(typ='IDU_BLK', rng=(33, 50), rel_rng=(2, 5, 2, 22), 
           txt='/*.[ ]_autosave*/')]
        
    -----------------------------------------------------
    String containing idu block that spans multiple line 

                 01234567890123456789012345678901234567890123456789
    >>> s3 = r"""/*= [2,3]*/width=3.5; /*.[ ]_autosave
    ... $c=-34;[ 2,3,4 ]{+|-|>} [x]_on*/ xxx"""

        01234567890123456789012345678901234567890123456789
    
    >>> list( tokenize(s3, 'IDU_PART' ) ) # doctest: +NORMALIZE_WHITESPACE 
    [Token(typ='NUM_LIST', rng=(4, 9), rel_rng=(0, 4, 0, 9), txt='[2,3]'), 
     Token(typ='CHK_BOX', rng=(25, 37), rel_rng=(0, 25, 0, 37), txt='[ ]_autosave'), 
     Token(typ='NUM_LIST', rng=(45, 54), rel_rng=(1, 7, 1, 16), txt='[ 2,3,4 ]'), 
     Token(typ='BTN', rng=(54, 61), rel_rng=(1, 16, 1, 23), txt='{+|-|>}'), 
     Token(typ='CHK_BOX', rng=(62, 68), rel_rng=(1, 24, 1, 30), txt='[x]_on')]    
                                 
    Note that the rel_rng of 2nd block, (1, 22, 2, 32), shows it's multi-line:
     
    >>> list( tokenize(s3, 'IDU_BLK' ) ) # doctest: +NORMALIZE_WHITESPACE 
    [Token(typ='IDU_BLK', rng=(0, 11), rel_rng=(0, 0, 0, 11), 
           txt='/*= [2,3]*/'), 
     Token(typ='IDU_BLK', rng=(22, 70), rel_rng=(0, 22, 1, 32), 
           txt='/*.[ ]_autosave\n$c=-34;[ 2,3,4 ]{+|-|>} [x]_on*/')]    
      
    '''     
    Token = collections.namedtuple('Token'
            , [ 'typ'   # 'CHK_BOX' | 'BTN' ...
              , 'rng'   # absolute range: (ibeg,iend)
              , 'rel_rng'  # relative range: (iBegLine, cBeg, iEndLine, cEnd)
              , 'txt'   # the text matched: '[ ]_autosave'
              ])
            
    keywords = {} #'//='} #, 'THEN', 'ENDIF', 'FOR', 'NEXT', 'GOSUB', 'RETURN'}
    #token_set = SCANNER_RULES['IDU_BLK']  
    #conout( 'token_set = ', token_set )
    if not token_name in SCANNER_RULES.keys():
      raise KeyError( 'token_name given("%s") must be one of %s'%(token_name
                      , str(SCANNER_RULES.keys()) ) )
    token_specification = SCANNER_RULES[token_name] 
    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
    get_token = re.compile(tok_regex).match
    iline = 1
    pos = line_start = 0
    mo = get_token(s)
    while mo is not None:
        typ = mo.lastgroup
        if typ == 'NEWLINE':
            line_start = pos
            iline += 1
        elif typ != 'SKIP':
            txt = mo.group(typ)
            if typ == 'ID' and txt in keywords:
                typ = txt 
                
            abs_cBeg = mo.start()-line_start
            abs_cEnd = abs_cBeg + len( txt )
            abs_iline = iline
            lines = s[:abs_cBeg].split('\n')
            nline = len(lines) 
            c= len(lines[-1])
            txts = txt.split('\n')   
            ilastline = nline+len(txts)-1
            cEnd = len(txts)==1 and ( c+len(txt) ) or len(txts[-1])

            tk= Token(typ
              , ( abs_cBeg, abs_cEnd ) #'rng'   # absolute range: (ibeg,iend)
              , ( nline-1, c, ilastline-1, cEnd) #'rel_rng'  # relative range: (iBegLine, cBeg, iEndLine, cEnd)
              , txt #'txt'   # the text matched: '[ ]_autosave'
              ) 
            yield tk  
            
        pos = mo.end()
        mo = get_token(s, pos) 
        
    if pos != len(s):
        print('Something wrong in tokenize(). pos=%s, type=%s'%(pos, type(pos)))
        raise RuntimeError('Unexpected character "%r" on line %d:\n"%s"' %(s[pos], iline, s[iline]))

def tokenize_to_rng_dict(s, rule): #token_name='IDU_BLK'):
    '''
            01234567890123456789012345678901234567890123456789
    >>> s= '/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/'
    
    -----------------------------------------------------
    Find idu blocks:

    >>> tokenize_to_rng_dict( s, 'IDU_BLK' ) # doctest: +NORMALIZE_WHITESPACE 
    OrderedDict([((0, 48), 
      Token(typ='IDU_BLK', rng=(0, 48), rel_rng=(0, 0, 0, 48),
            txt='/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/'))])
    
    >>> tokenize_to_rng_dict( s, 'IDU_PART' ) # doctest: +NORMALIZE_WHITESPACE 
    OrderedDict([((4, 18), 
                 Token(typ='NUM_LIST', rng=(4, 18), rel_rng=(0, 4, 0, 18), 
            txt='[ 3, -4, 5.5 ]')), 
      ((18, 25), Token(typ='BTN', rng=(18, 25), rel_rng=(0, 18, 0, 25), 
            txt='{+|-|>}')), 
      ((25, 37), Token(typ='CHK_BOX', rng=(25, 37), rel_rng=(0, 25, 0, 37), 
            txt='[ ]_autosave')), 
      ((38, 44), Token(typ='CHK_BOX', rng=(38, 44), rel_rng=(0, 38, 0, 44), 
            txt='[x]_on'))])
                                                                               
    '''      
    #tk = collections.namedtuple(token_name, 
    #                           ['typ', 'value', 'line', 'col'])

    #li = list(tokenize(s,token_name))   
    d = collections.OrderedDict()
    for x in tokenize(s,rule) :
      d[x.rng]=x
    return d
   
def tokenize_to_typ_dict(s, rule): #token_name='IDU_BLK'):
    '''
            01234567890123456789012345678901234567890123456789
    >>> s= '/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/'
    
    -----------------------------------------------------
    Find idu blocks:

    >>> tokenize_to_typ_dict( s, 'IDU_BLK' ) # doctest: +NORMALIZE_WHITESPACE
    OrderedDict([('IDU_BLK', 
      [Token(typ='IDU_BLK', rng=(0, 48), rel_rng=(0, 0, 0, 48), 
        txt='/*==[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on==*/')])])    
    
    >>> tokenize_to_typ_dict( s, 'IDU_PART' ) # doctest: +NORMALIZE_WHITESPACE 
    OrderedDict([('NUM_LIST', 
      [Token(typ='NUM_LIST', rng=(4, 18), rel_rng=(0, 4, 0, 18), 
        txt='[ 3, -4, 5.5 ]')]), ('BTN', 
      [Token(typ='BTN', rng=(18, 25), rel_rng=(0, 18, 0, 25), 
        txt='{+|-|>}')]), 
    ('CHK_BOX', 
      [Token(typ='CHK_BOX', rng=(25, 37), rel_rng=(0, 25, 0, 37), 
          txt='[ ]_autosave'), 
       Token(typ='CHK_BOX', rng=(38, 44), rel_rng=(0, 38, 0, 44), 
          txt='[x]_on')])])
                                                                               
    '''      
    #tk = collections.namedtuple(token_name, 
    #                           ['typ', 'value', 'line', 'col'])

    #li = list(tokenize(s,token_name))   
    d = collections.OrderedDict()
    for x in tokenize(s,rule) :
      if not x.typ in d.keys(): d[x.typ]=[]
      d[x.typ].append( x )
    return d 

#def idu_tokenize(s, token_type='IDU_PART'): #, token_set=SCANNER_RULES['IDU_BLK'] ):
#  '''       
#    Return a dict w/ keys = token name ('CHK_BOX', 'BTN' ...)
#    and values are list of a IDU_BLK named tuple like:
#         
#      IDU_PART(rng=(29, 41), iL=1, cB=29, iLE=1, cE=41, txt='[ ]_autosave'
#          
#    where IDU_PART: the str entered as token_type:
#                    idu_tokenize(s, token_type='IDU_PART')
#                    This str must be one of the keys of the dict SCANNER_RULES
#          rng: indices range where this block covers within given string s
#          iL : index for line number
#          cB : index for col number
#          iLE: index for last line number
#          cE : index for col where it ends in last line
#          txt: the matched idu block string
#          
#    See doctest below for usage.       
#  
#    >> tk_def = SCANNER_RULES['IDU_PART']  
#            01234567890123456789012345678901234567890123456789
#    >>> s= 'b=3; /*=[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/' 
#    
#    >>> d= idu_tokenize( s, token_type='IDU_PART' ) 
#    >>> d # doctest: +NORMALIZE_WHITESPACE
#    {'CHK_BOX': 
#        [IDU_PART(rng=(29, 41), iL=1, cB=29, iLE=1, cE=41, txt='[ ]_autosave'), 
#         IDU_PART(rng=(42, 48), iL=1, cB=42, iLE=1, cE=48, txt='[x]_on')], 
#     'NUM_LIST': 
#       [IDU_PART(rng=(8, 22), iL=1, cB=8, iLE=1, cE=22, txt='[ 3, -4, 5.5 ]')], 
#     'BTN': [IDU_PART(rng=(22, 29), iL=1, cB=22, iLE=1, cE=29, txt='{+|-|>}')]}    
#    
#    >>> blk0= d['CHK_BOX'][0]
#    >>> blk0.rng,  blk0.txt
#    ((29, 41), '[ ]_autosave')
#    
#    >>> def getBlkAt(d,i):
#    ...   for ctrl in d.keys(): 
#    ...      for blk in d[ctrl]:
#    ...          if blk.rng[0]<=i and i<blk.rng[1]:
#    ...             return blk
#    
#    >>> b=getBlkAt(d,i=24)
#    >>> b 
#    IDU_PART(rng=(22, 29), iL=1, cB=22, iLE=1, cE=29, txt='{+|-|>}')
#    >>> b.rng
#    (22, 29)
#    
#    >>> getBlkAt(d,i=3)  # Return nothing
#          
#    ----------------------------------------------
#    
#    >>> s2= ' blah blah blah '
#    >>> failed= idu_tokenize( s2, token_type='IDU_PART' ) #, token_set=tk_def ) 
#    
#    >>> str(failed) # Return nothing (= None)
#    'None'
#    
#    ----------------------------------------------
#    String containing multiple idu blocks:
#    
#             01234567890123456789012345678901234567890123456789
#    >>> s3 = "/*= [2,3]*/width=3.5; /*.[ ]_autosave*/"   
#    
#    >>> idu_tokenize( s3, token_type='IDU_PART'  ) # doctest: +NORMALIZE_WHITESPACE 
#    {'CHK_BOX': 
#        [IDU_PART(rng=(25, 37), iL=1, cB=25, iLE=1, cE=37, txt='[ ]_autosave')], 
#     'NUM_LIST': [IDU_PART(rng=(4, 9), iL=1, cB=4, iLE=1, cE=9, txt='[2,3]')]}
#         
#
#    -----------------------------------------------------
#    String containing multiple idu blocksin multiline. --- NOTE that 
#    tokenizer() is unable to tell there's a '\n' in the given string. 
#    This will be handled by the idu_tokenize():
#    
#                01234567890123456789012345678901234567890123456789
#    >>> s4 = """/*= [2,3]*/width=3.5;
#    ... b=3; 
#    ... c=4; /*.[ ]_autosave*/ xxx""" 
#    
#        01234567890123456789012345678901234567890123456789
#    
#    >>> idu_tokenize( s4, token_type='IDU_PART'  ) # doctest: +NORMALIZE_WHITESPACE   
#    {'CHK_BOX': 
#        [IDU_PART(rng=(36, 48), iL=3, cB=8, iLE=3, cE=20, txt='[ ]_autosave')], 
#     'NUM_LIST': [IDU_PART(rng=(4, 9), iL=1, cB=4, iLE=1, cE=9, txt='[2,3]')]}    
#    
#     
#    ----------------------------------------------
#    
#    multi-line string:
#       
#               01234567890123456789012345678901234567890123456789
#    >>> s5= """/*= [2,3]*/width=3.5; /*.[ ]_autosave
#    ... $c=-34;[4,5,6]{+|-|>} [x]_on*/ xxx"""
#    
#        01234567890123456789012345678901234567890123456789
#                                      
#    >>> idu_tokenize(s5, token_type='IDU_PART') # doctest: +NORMALIZE_WHITESPACE
#    {'CHK_BOX': 
#        [IDU_PART(rng=(25, 37), iL=1, cB=25, iLE=1, cE=37, txt='[ ]_autosave'), 
#         IDU_PART(rng=(60, 66), iL=2, cB=22, iLE=2, cE=28, txt='[x]_on')], 
#     'NUM_LIST': 
#        [IDU_PART(rng=(4, 9), iL=1, cB=4, iLE=1, cE=9, txt='[2,3]'), 
#         IDU_PART(rng=(45, 52), iL=2, cB=7, iLE=2, cE=14, txt='[4,5,6]')], 
#     'BTN': 
#        [IDU_PART(rng=(52, 59), iL=2, cB=14, iLE=2, cE=21, txt='{+|-|>}')]} 
#    
#  '''    
#  token_set = SCANNER_RULES[ token_type ] 
#  tokens = tokenizer( s,token_set )
#  #conout( 'In idu_tokenize(), len(s.split("\n"))= ',len(s.split("\n")))  
#  #conout( 's = ',s)
#  tokens = list(tokens)
#  if tokens:
#    d={}
#    for token in tokens:   # Token(typ='BTN', value='{+|-|>}', line=1, col=18)
#      typ,txt,abs_nline, abs_c = token
#      if not typ in d.keys(): d[typ]=[]
#      lines = s[:abs_c].split('\n')
#      nline = len(lines) 
#      c= len(lines[-1])
#      txts = txt.split('\n')      
#      #conout( '-- txts = ', txts )
#      nlastline = nline+len(txts)-1
#      cEnd = len(txts)==1 and ( c+len(txt) ) or len(txts[-1])
#      blk_info = collections.namedtuple(
#                token_type, 'rng, iL, cB, iLE, cE, txt')
#      d[typ].append( blk_info._make(( 
#                      ( abs_c, abs_c+len(txt) )
#                     , nline 
#                     , c     
#                     , nlastline
#                     , cEnd
#                     , txt
#                     ))
#                   )                
##      d[typ].append( ( abs_c, abs_c+len(txt)
##                     , nline 
##                     , c     
##                     , nlastline
##                     , cEnd
##                     , txt
##                     )
##                   )  
#      #conout('d= ', d)                  
#    return d                    
#  else:
#    return None
#        
   
    
#def get_ascui_indices():
#  '''
#    Find and return ascui block indices
#    => [ (noBeg,noEnd),  ...]
#     
#  '''
#  lines   = get_text_all().split('\n')
#  noBeg= -1
#  isInAscui= False
#  ascuis = [ ]
#  for n,line in enumerate( lines ):
#    if line.startswith( IDU_BEG ) and not isInAscui:
#        noBeg = n
#        isInAscui= True
#    elif line.startswith( IDU_END ) and isInAscui:
#        #noEnd = n
#        ascuis.append( (noBeg, n) ) 
#        isInAscui = False
#        noBeg = 0
#  return ascuis
#  
      
#def get_idu_blocks( s ):
#  r"""   
#    Return list of idu blocks info, each block is:
#      (line#, col#, content)
#     
#    ------------------------------------------      
#              01234567890123456789012345678901234567890123456789
#    >>> s= '''/*= [2,3]*/width=3.5; /*.[ ]_autosave*/ 
#    ... $123bc =  -34.0;/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/'''
#
#        012345678901234567890123456789012345678901234567890123456789
#               
#    >>> blks= get_idu_blocks(s) 
#    >>> blks # doctest: +NORMALIZE_WHITESPACE
#    [IDU_BLK(rng=(0, 11), iL=1, cB=0, iLE=1, cE=11, txt='/*= [2,3]*/'), 
#     IDU_BLK(rng=(22, 39), iL=1, cB=22, iLE=1, cE=39, txt='/*.[ ]_autosave*/'), 
#     IDU_BLK(rng=(57, 102), iL=2, cB=16, iLE=2, cE=61, 
#     txt='/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/')]
#        
#    ------------------------------------------
#    
#    >>> s2= '''/*= [2,3]*/width=3.5; /*.[ ]_autosave*/ 
#    ... cube();
#    ... $123bc =  -34.0;/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/'''
#               
#    >>> get_idu_blocks(s2) # doctest: +NORMALIZE_WHITESPACE
#    [IDU_BLK(rng=(0, 11), iL=1, cB=0, iLE=1, cE=11, txt='/*= [2,3]*/'), 
#     IDU_BLK(rng=(22, 39), iL=1, cB=22, iLE=1, cE=39, txt='/*.[ ]_autosave*/'), 
#     IDU_BLK(rng=(65, 110), iL=3, cB=16, iLE=3, cE=61, 
#       txt='/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/')]
#                                             
#       
#    [(1, 0, 1, 11, '/*= [2,3]*/'), 
#     (1, 22, 1, 39, '/*.[ ]_autosave*/'), 
#     (3, 16, 3, 61, '/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/')]                                                 
#    
#    ------------------------------------------
#    String that has block spanning lines:
#         
#               01234567890123456789012
#    >>> s3= '''/*= [2,3]*/width=3.5; /*.[ ]_autosave
#    ... [ 3, -4, 5.5 ]{+|-|>}
#    ... [x]_on*/''' 
#    
#        012345678901234567890  
#                   
#    >>> get_idu_blocks(s3) # doctest: +NORMALIZE_WHITESPACE  
#    [IDU_BLK(rng=(0, 11), iL=1, cB=0, iLE=1, cE=11, txt='/*= [2,3]*/'), 
#     IDU_BLK(rng=(22, 68), iL=1, cB=22, iLE=3, cE=8, 
#      txt='/*.[ ]_autosave\n[ 3, -4, 5.5 ]{+|-|>}\n[x]_on*/')]
#    
#        
#    [(1, 0, 1, 11, '/*= [2,3]*/'), 
#     (1, 22, 3, 8, '/*.[ ]_autosave\n[ 3, -4, 5.5 ]{+|-|>}\n[x]_on*/')]
#     
#  """
#  d = idu_tokenize(s,token_type='IDU_BLK' ) 
#  return d.get('IDU_BLK',[])  
#                                    
       
def get_idu_at_i(ed, rule): #idu_token='IDU_BLK'):
    '''return (ntBlk_at_i, ntPart_at_i) '''
    
    doc = ed.get_text_all()
    i = ed.get_caret_pos()  
    #conout ( 'i = ', i )
    idu_blks= tokenize_to_rng_dict(doc, rule) #idu_token) #'IDU_BLK') 
    if idu_blks:
      rngs = idu_blks.keys()
      ntBlk_at_i= [ idu_blks[rng] for rng in rngs
                   if rng[0]<=i and i<rng[1]] 
      
      if ntBlk_at_i:
           
        ntBlk_at_i = ntBlk_at_i and ntBlk_at_i[0] or None                                   
        #rng = ntBlk_at_i.rng
        #conout('rng = ', rng, ': "%s"'%doc[ rng[0]:rng[1]])
        #conout( 'idu> Clicked inside an idu_blk:\n... ', ntBlk_at_i )
            # , '\n... doc[%s]'%(':'.join(rng)), doc[ rng[0]:rng[1]] )
        i = i-ntBlk_at_i.rng[0]  
        #conout('i= ',i)  
        ntParts = tokenize_to_rng_dict( ntBlk_at_i.txt, SCANNER_RULES['IDU_PART'] )
            # ntParts=
            # {'NUM_LIST': 
            #   [IDU_PART(rng=(5, 12), iL=1, cB=5, iLE=1, cE=12, txt='[4,5,6]')], 
            #  'BTN': 
            #   [IDU_PART(rng=(13, 18), iL=1, cB=13, iLE=1, cE=18, txt='{+|-}')]}
        #conout( 'ntParts = ', ntParts )
        
        ntPart_at_i = [ ntParts[rng] for rng in ntParts.keys() 
                      if rng[0]<=i and i<rng[1]
                   ]
        ntPart_at_i = ntPart_at_i and ntPart_at_i[0] or None                                   
                   
        #conout( 'ntPart_at_i: ', ntPart_at_i)  
        
        return (ntBlk_at_i, ntPart_at_i) 
      else:
        return None   

      
#def get_control_at_caret(ed):
  
#def get_new_num(oldnumstr, operatorstr, prec=6):
#  '''                                     
#    >>> get_new_num('3','+')
#    4
#    >>> get_new_num('3.3','-')
#    2.3
#    >>> get_new_num('3.3','*')
#    6.6
#    >>> get_new_num('2.4','/')
#    1.2
#    >>> get_new_num('3','+0.5')
#    3.5
#    >>> get_new_num('3.3','-1.1')
#    2.2
#    >>> get_new_num('3.3','*3')
#    9.9
#    >>> get_new_num('2.4','/0.5')
#    4.8
#       
#  ''' 
#  num = ( ('.' in oldnumstr) and float(oldnumstr) 
#          or int(oldnumstr)
#        )
#  op = operatorstr 
#  d = { op[0] in '*/': 2  
#      , op[0] in '+-': 1 
#      , len(op)>1: op[1:] 
#      , op[0] in '<>': 0
#      }[1]
#  d = float(d)
#  v = { '+': num + d 
#      , '-': num - d
#      , '*': num * d 
#      , '/': num / d
#      }[op[0]]
#
#  v= (('%.'+str(prec)+'f')%v).rstrip('0') 
#  if v.endswith('.'): v=v[:-1]
#     
#  return ('.' in v) and float(v) or int(v) 
                                 
  
def get_ntBtn_at_i(ed, i=-1):
  '''
    Return Token(typ='BTN_LABEL', rng=(18, 20), 
             rel_rng=(0, 18, 0, 20), txt='-3')
  ''' 
  conout('Entered get_ntBtn_at_i()')
  doc = ed.get_text_all()
  c,r = ed.get_caret_xy() 
    
  i = i==-1 and c-1 or i
                                 
  line = ed.get_text_line(-1)
  ntLbls = list( tokenize( line, "BTN_LABEL") )
  conout( 'In get_ntBtn_at_i(), init ntLbls = ', ntLbls ) 
  conout( 'In get_ntBtn_at_i(), i = ', i ) 
  #ch1, ch2 = doc[i-1:i+1]
  ntLbl = None
  if ntLbls:
    if i>=0:
      for lbl in ntLbls:
        if lbl.rng[0]<=i and i< lbl.rng[1]:
          ntLbl = lbl
          break      
    conout( 'In get_ntBtn_at_i(), returning ntLbl = ', ntLbl ) 
    return ntLbl
  
def get_ntBlk_at_i( ntBlks, i ):   
  '''      
     ntBlks: something like:
        [IDU_BLK(rng=(0, 11), iL=1, cB=0, iLE=1, cE=11, txt='/*= [2,3]*/'), 
         IDU_BLK(rng=(22, 39), iL=1, cB=22, iLE=1, cE=39, txt='/*.[ ]_autosave*/'), 
         IDU_BLK(rng=(65, 110), iL=3, cB=16, iLE=3, cE=61, 
              txt='/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/')] 
              
    i is the absolute index counting from the beg of doc.          
              
  '''     
  ntBlk_at_i =  [blk for blk in ntBlks
               if blk.rng[0]<=i and i<blk.rng[1]]
  return ntBlk_at_i and ntBlk_at_i[0] or None
        
def get_ntNum_b4_blk(line, blk): #, re_ptn=RE_NUM):
  '''          
    Return the namedtuple for any valid number that is 
    right b4 position blk on line. If not a number, return None.
                                                   
    line: string
    i : column index
    
               0123456789012345678901234 
    >>> line= 'x=5.67; /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk = list( tokenize( line, "IDU_BLK"))[-1]
    >>> ntBlk
    Token(typ='IDU_BLK', rng=(8, 32), rel_rng=(0, 8, 0, 32), txt='/*=[3.5, 4, 10] {+ -} */')
    >>> ntNum = get_ntNum_b4_blk( line, ntBlk )
    >>> ntNum    
    Token(typ='NUM', rng=(2, 6), rel_rng=(0, 2, 0, 6), txt='5.67')
    
    >>> line2= 'cube( [ 5.2 /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk2 = list( tokenize( line2, "IDU_BLK"))[-1]
    >>> ntNum2 = get_ntNum_b4_blk( line2, ntBlk2 )
    >>> ntNum2    
    Token(typ='NUM', rng=(8, 11), rel_rng=(0, 8, 0, 11), txt='5.2')
   
    >>> line3= 'cube( [ 5.2, /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk3 = list( tokenize( line3, "IDU_BLK"))[-1]
    >>> ntNum3 = get_ntNum_b4_blk( line3, ntBlk3 )
    >>> ntNum3    
    Token(typ='NUM', rng=(8, 11), rel_rng=(0, 8, 0, 11), txt='5.2')

    >>> line4= 'cube( [  /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk4 = list( tokenize( line4, "IDU_BLK"))[-1]
    >>> ntNum4 = get_ntNum_b4_blk( line4, ntBlk4 )
    >>> ntNum4    
            
  '''
  left= line[ :blk.rel_rng[1] ]  
  
  left2 = list( tokenize(left, token_name='NUM') )
  ntNum = left2 and left2[-1] or None
  return ntNum       
 
def get_num_at_i(line, i): #, re_ptn=RE_NUM):
  '''          
    Return the number that is matched at position i
    of line. If not a number, return None.
                                                   
    line: string
    i : column index
    
    Usage: in assign_list_val()
    
               0123456789012345678901234 
    >>> line= '/*=[3.5, 4, 10] {+ -} */'
    >>> get_num_at_i( line, 1) 
    >>> get_num_at_i( line, 5)
    3.5
    >>> get_num_at_i( line, 6)
    3.5
    >>> get_num_at_i( line, 10)    
    4
    >>> get_num_at_i( line, 13)   
    10
    
  '''
  left, right = line[:i], line[i:]
  left2 = re.compile(r'(,|\[| |\b)').split(left)[-1]
  right2= re.compile(r'\b').split(right)[0]
  matched = re.compile( RE_NUM ).match( left2+right2)
  n = matched and matched.group() or None
  if n: 
    if len(n.split('.'))>1: n = float(n)
    else: n=int(n)
    
  return n   
  
#def get_num_in_list(ed, i=-1):
#  '''   
#    Chk if the current caret is on a number in a number list  
#    and if yes, return the num
#  '''
#  doc = ed.get_text_all()
#  c,r = ed.get_caret_xy() 
#    
#  i = i==-1 and c-1 or i
#                                 
#  line = ed.get_text_line(-1)                               
#  nl_info = list( tokenize( line, "NUM_LIST") )
#    #| Token( typ='NUM_LIST', rng=(8, 15), rel_rng=(0, 8, 0, 15)
#    #|      , txt='[3,4,5]')
#     
#  IS_DEBUG=1
#  conout( 'In get_num_in_list(), init nl_info = ', nl_info ) 
#  conout( 'In get_num_in_list(), i = ', i ) 
  #ch1, ch2 = doc[i-1:i+1]
  #if nl_info:
  #  if i>=0: 
      
    
#      for btn in nl_info:
#        if btn.rng[0]<=i and i< btn.rng[1]:
#          nl_info = btn
#    else:
#      nl_info= nl_info[-1]
#    conout( 'In get_num_in_list(), returning nl_list = ', nl_info ) 
#    return nl_info
#  IS_DEBUG=0     

def get_prev_chars(ed, n=3):
  '''Get chars right before the caret.
  '''
  i = ed.get_caret_pos()
  if i<n: n=i
  return ed.get_text_all()[i-n:i]
 
def is_in_word(ed, word):
  ''' chk if caret is within word boundary.

    Let word = "synw" and 'I' represents the caret, 'I' is 
    in its boundary if it is : Isynw, sIynw, syInw and synIW,
    but NOT synwI.
    
    Return None or a namedtuple:
      (typ='WORD', rng=(abs_x,abs_y), 
       rel_rng=( line_beg, c_beg, line_end, c_end)
       txt= 'synw' 
      )
         
 '''
  if word: 
    doc = ed.get_text_all()
    line = ed.get_text_line(-1)
    pos = ed.get_caret_pos()
    c,r = ed.get_caret_xy()
    
    wl= len(word)
    for i in range(wl):
      wd = doc[ pos-i: pos-i+wl]
      if wd == word:  
#        ntWord= collections.namedtuple('WORD',
#                  ['typ','rng','rel_rng','txt'])( 
#                    'WORD', (pos-i,pos-i+wl), 
#                    (r,c-i,r,c-i+wl), word
#                    ) 
        ntWord = ntBlk( 'WORD', typ='WORD', rng= (pos-i,pos-i+wl), 
                        rel_rng= (r,c-i,r,c-i+wl), txt= word )
        return ntWord          
 
def is_in_words(ed, words):
  for w in words:
    ntWord = is_in_word(ed, w)
    if ntWord: return ntWord
                    
        
def get_ntBool_at_i(ed):

  ntBool = is_in_word(ed, 'true')
  ntBool = ntBool and ntBool or is_in_word(ed, 'false') 
  #print( 'in get_ntBool_at_i(), ntBool = ', ntBool)
  return ntBool               
   
               
def assign_btn_val(ed, ntBlk, ntBtn, prec=6):
  '''
     Assign new value to the value assignment code
     (like width=3;) right in before the given ntBlk,
     which is a namedtuple representing an idu ntBlk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
     btn_label_ch is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  ''' 
  conout('Enter assign_btn_val()')
  line= ed.get_text_line(-1)
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  
  if ntOldNum and ntBtn:

    num_ibeg, num_iend = ntOldNum.rng 
    
    newtxt= { 1: ntBtn.txt
            , ntBtn.txt=="+": "+1"
            , ntBtn.txt=="-": "-1"
            , ntBtn.txt=="*": "*2"
            , ntBtn.txt=="/": "/2" 
            }[1]
       
    newnum = eval( ntOldNum.txt + newtxt) 
    #print('newnum = ', newnum, ', type = ', type(newnum),
    #      ', ', round(newnum,prec))
    if type(newnum)==float: newnum=round(newnum, prec) 
    
    line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
    num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
    num_iend_abs = num_iend-num_ibeg 
    
    ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
   
    return 1    
    
  else: conout ('assign not found')  
 
             
  
IS_DEBUG=0

def set_bool_onsite(ed, dlg_menu, menu_id
                  , isAutoSave 
                  , file_save_func
                  , prec=6):
  '''      
    Called by on_click when user clicks at or right before
    a number and replace the num with a new one by calling up a 
    menu and selecting an operation (like "+", "-3", etc) 
    which will be applied the old number. 
    
    ntNum is a namedtuple representing a num
     like Token(typ='NUM', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='234.567891')

     prec is to set the decimal points after the "." of
     the new num
  ''' 
  conout('\n\nEnter set_bool_onsite()')   
  file_save = file_save_func
  ## When click inside /*=*/, bring up a menu with operations
  ##  to be executed upon the num before /*=*/. This menu will
  ##  stay on if an operation is selected. Will quit if click
  ##  on [done] or somewhere outside the menu or hit [Esc] 
  ##

  
  #ops = operators + uis 
  
  im=1        # index of menu item                
  #tSel = None # selected text 
  #oldnum = ntBool #eval(ntNum.txt) 
  
  keepRunning = 1
              
  ## Make the menu dlg keep popping up until user hits [Esc]
  ##  or select [done] or click off-menu, all return None
  ## 
  ## Note that the cursor has to be re-set according to
  ##  the len of new val (like 3.3456, comparing to 3,  
  ##  will push the caret to the far right and out of /*=*/
  ##  This is done in assign_menu_val() 
  ##
  while keepRunning : #m!=None and tSel!=ops[-1]:
    conout('\nStarting while loop' )
       
      
    ## Re-scan to get new blk 'cos the previous
    ##  run might change caret position   
    ntBool = get_ntBool_at_i(ed)
    ed.set_sel( ntBool.rng[0], len(ntBool.txt), nomove=False)
    
    newtext = (ntBool.txt=='true' and 'false' or 'true')
    
    menu_text= newtext + '\n-\n[done]'
             
    im= dlg_menu( menu_id, caption='Toggle Boolean'
                , text= menu_text )          
              
    ed.set_caret_pos( ntBool.rng[0] )
    
    if im==0:
      ed.replace( ntBool.rng[0]
                , len(ntBool.txt)
                , newtext
                ) 
      if isAutoSave: file_save()  
              
    else:
      keepRunning = 0    
      

      
           
def set_val_onsite(ed, dlg_menu, menu_id
                  , ntNum
                  , isAutoSave 
                  , file_save_func
                  , prec=6):
  '''      
    Called by on_click when user clicks at or right before
    a number and replace the num with a new one by calling up a 
    menu and selecting an operation (like "+", "-3", etc) 
    which will be applied the old number. 
    
    ntNum is a namedtuple representing a num
     like Token(typ='NUM', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='234.567891')

     prec is to set the decimal points after the "." of
     the new num
  ''' 
  conout('\n\nEnter set_val_onsite()')   
  file_save = file_save_func
  ## When click inside /*=*/, bring up a menu with operations
  ##  to be executed upon the num before /*=*/. This menu will
  ##  stay on if an operation is selected. Will quit if click
  ##  on [done] or somewhere outside the menu or hit [Esc] 
  ##
  operators= [ ' +1',r' -1','+0.3','-0.2','+10','-7'
       , r' *2',' /2']
  undo_section= ['-','[original]','[undo]']     
  undos = [ ]
  uis= ['-', '[done]']
  
  #ops = operators + uis 
  
  im=1        # index of menu item                
  tSel = None # selected text 
  oldnum = eval(ntNum.txt)
  keepRunning = 1
              
  ## Make the menu dlg keep popping up until user hits [Esc]
  ##  or select [done] or click off-menu, all return None
  ## 
  ## Note that the cursor has to be re-set according to
  ##  the len of new val (like 3.3456, comparing to 3,  
  ##  will push the caret to the far right and out of /*=*/
  ##  This is done in assign_menu_val() 
  ##
  while keepRunning : #m!=None and tSel!=ops[-1]:
    conout('\nStarting while, tSel ="%s", undos=%s'%(tSel,undos) )
       
      
    ## Re-scan to get new blk and part 'cos the previous
    ##  run might change caret position       
    ## blk_n_part = get_idu_at_i(ed, 'NUM') 
    #ed.set_caret_pos( ntNum.rng[0] )
    ntNum = get_idu_at_i(ed, SCANNER_RULES['NUM'])[0]
    ed.set_sel( ntNum.rng[0], len(ntNum.txt), nomove=False)
                                                                   
    undo_list= ([ '*old:'+','.join([str(x) for x in undos])]
               + ['[Save old as ui]'])
                        
    ops = ( operators + undo_section 
          + ( undos and undo_list or []) 
          + uis ) 
    im= dlg_menu( menu_id, caption='Select operation'
              , text= '\n'.join(ops) )
    ed.set_caret_pos( ntNum.rng[0] )
      
    tSel = type(im)==int and ops[im].strip() or None
    conout('tSel = ', tSel)
                
    if tSel == '[original]':   ## Revert to the old num
      ed.replace( ntNum.rng[0]
                , len(ntNum.txt)
                , str(oldnum)
                ) 
      if isAutoSave: file_save()  
              
    elif tSel=='[undo]':   ## Revert to the prev changed num
      if undos:
        conout('Asking for undo, undos = %s'%(undos))
        ed.replace( ntNum.rng[0]
                  , len(ntNum.txt)
                  , str(undos.pop())
                  )              
      if isAutoSave: file_save()
    
    elif tSel=='[Save old as ui]':    
    
      ed.set_caret_pos(  ntNum.rng[1] )
      ed.insert('/*=['+ ops[im-1].replace('*old:','') +']{< >}*/' ) 
      ed.set_caret_pos(  ntNum.rng[0] )
             
                            
    elif im==None or tSel == '[done]':
      keepRunning = 0 
      
    else:
       
      newnum = eval( ntNum.txt + tSel) 
      conout('newnum = ', newnum, ', type = ', type(newnum),
          ', ', round(newnum,prec))
      if type(newnum)==float: newnum=round(newnum, prec) 
      
      ed.replace( ntNum.rng[0]
                , len(ntNum.txt)
                , str(newnum)
                )    
      #ed.set_caret_pos( ntNum.rng[0] ) 
      if isAutoSave: file_save() 
      undos.append( eval(ntNum.txt) )
      keepRunning= 1     
      #conout( 'a = ', a )

      
def assign_menu_val(ed, ntBlk, sMenuItem, prec=6):
  '''    
    NOTE: this needs to retire ...
      
    Called by on_click when user clicks at "/**/" in the 
    middle of "**"
    
     Assign new value to the number right in before the 
     given ntBlk by calling up a menu and selecting an 
     operation (like "+", "-3", etc) which will be executed
     upon the old number. 
     ntBlk is a namedtuple representing an idu ntBlk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
       
     sMenuItem is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  ''' 
  conout('Enter assign_menu_val()')
  i = ed.get_caret_pos()
  c,r = ed.get_caret_xy()
  line =  ed.get_text_line(-1)[:c]
  conout( 'c=%s, r=%s, line="%s"'%(c,r,line) )
  
  #| Create a ntBlk with an attribute 'rel_rng' that can be 
  #| used by get_ntNum_b4_blk(). 
  #ntBlk = collections.namedtuple('BLK', ['rel_rng'])( (r,c) )
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  conout( 'ntOldNum = ', ntOldNum ) 
  
  if ntOldNum:

    num_ibeg, num_iend = ntOldNum.rng 
    
    newtxt= { 1: sMenuItem
            , sMenuItem=="+": "+1"
            , sMenuItem=="-": "-1"
            , sMenuItem=="*": "*2"
            , sMenuItem=="/": "/2" 
            }[1]
       
    newnum = eval( ntOldNum.txt + newtxt) 
    
    conout('newnum = ', newnum, ', type = ', type(newnum),
          ', ', round(newnum,prec))
    if type(newnum)==float: newnum=round(newnum, prec) 
    
    line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
    num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
    num_iend_abs = num_iend-num_ibeg 
    
    ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))

    ## The cursor has to be re-set according to the
    ##  len of new val (like 3.3456, comparing to 3,  
    ##  will push the caret to the far right and out of /*=*/ 
    ##    
    len_oldNum = len(ntOldNum.txt)
    len_newNum = len(str(newnum))
    newi = i+ len_newNum- len_oldNum
    #ed.set_sel(start=i-c, len=1, nomove=True)  
    ed.set_caret_pos( newi ) 
    return 1    
    
  else: conout ('assign not found')  
     
def assign_next_val(ed, ntBlk, ntBtn, prec=12):
  '''    
    When btn is either ">" or "<"
    
    This requires either a list or a range:
    
      x=3; /*=[3,4,5] {>} */
      x=3; /*=[0:3:15] {>} */
      
    
  ''' 
  conout('Enter assign_next_val()')
  line= ed.get_text_line(-1)
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  
  if ntOldNum and ntBtn:
                            
    # Chk if the ntBlk contains either range ( [i:j] or [i:j:k] )
    # or nun list ( [2,4,6,10] )
    #
    ntRngs = list(tokenize( ntBlk.txt, "RANGE" ))
    conout('ntRngs = ', ntRngs)
    rng=None 
      
    if ntRngs:
      ntRng = ntRngs[0]
      ns = [eval(x) for x in ntRng.txt[1:-1].split(':')] #[i,j,k], [i,j]
      if len(ns)==2: ns= [ns[0],1,ns[1]]
      beg,itv,end = ns
      #rng = [ x for x in range(beg,end+1,itv) ] 
      count = int((end-beg)/itv)  
      conout( 'beg=%s, itv=%s, end=%s, count=%s'%(beg,itv,end,count))
      rng=[ round(i*itv+beg, prec) for i in range(count+1)]
      
    else:
      nt_nlists = list( tokenize( ntBlk.txt, "NUM_LIST") )
      conout('nt_nlists = ', nt_nlists) 
      if nt_nlists:  
        nt_nlist = nt_nlists[0]
        rng = eval( nt_nlist.txt)
        
    if rng:        
      
      oldNum = float(ntOldNum.txt)
      rng.sort() 
                  
      def get_next_i(rng, val, direction='>'): 
        '''
            Return next index, depending on direction.
            If i out of bound, it cycles from the other end.
        '''
        i=0
        if direction=='>':
          if val>= rng[-1]: i= 0
          else:
            rng_tmp =  rng + [ rng[-1]+ 
                              ((len(rng)>1 and (rng[-1]-rng[-2])) or 1)
                             ]
            i=0
            for i,v in enumerate(rng_tmp):
              if val < v: break 
          return i
          
        else:
          if val<= rng[0]:
            i=len(rng)-1
          else:
            rng_tmp = [ rng[0] -
                        ((len(rng)>1 and (rng[0]-rng[1])) or 1)
                      ] +rng
            iis = [x for x in range(len(rng))]
            iis.reverse()
            for i in iis:
              if val> rng[i]: break  
          return i
      
      nexti = get_next_i(rng, oldNum, direction=ntBtn.txt)
      
      if nexti>=0 and nexti<len(rng):  
           
        conout( 'rng = %s, nexti = %s'%(rng,nexti))      
        newnum = rng[ nexti ]
        conout( 'rng = %s, nexti = %s, newnum = %s'%(rng,nexti,newnum))      
        
        if type(newnum)==float: newnum=round(newnum, prec) 
        num_ibeg, num_iend = ntOldNum.rng 
        
        line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
        num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
        num_iend_abs = num_iend-num_ibeg 
        
        ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
       
        return 1    
    
  else: conout ('num to-be-replaced not found')  
       
      
def assign_list_val(ed, blk, prec=6):
  '''
     Assign new value to the value assignment code
     (like width=3;) right in before the given blk,
     which is a namedtuple representing an idu blk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
     btn_label_ch is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  '''   
  conout('Entered: assign_list_val()')
  line= ed.get_text_line(-1)
  c,r = ed.get_caret_xy()
  newnum = get_num_at_i(line,c)   
  conout('newnum = ', newnum)
  if not newnum==None:
    ntOldNum = get_ntNum_b4_blk( line, blk )
    conout('ntOldNum = ', ntOldNum)  
    
    if ntOldNum:

      num_ibeg, num_iend = ntOldNum.rng 
      line_ibeg_abs = blk.rng[0]-blk.rel_rng[1]
      num_ibeg_abs = line_ibeg_abs + num_ibeg 
      num_iend_abs = num_iend-num_ibeg 
      if type(newnum)==float: round( newnum, prec )
      ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
   
      return 1 
    else: conout ("Can't find a number to be modified.")
  else: conout ("Can't find the number at i from the list")  

  
    

  
                   
#def load_idu(s, idu=None):
#  r'''  
#    To be a valid idu doc, the 1st line of scad file should look like: 
#    /*==[idu] [x]_on [ ]_autosave ==*/
#  
#    When s is a valid idu block, create an idu instance and 
#    load defaults:
#                                 
#    >>> s= '/*==[idu] [x]_on [ ]_autosave ==*/'
#    >>> idu=load_idu(s)    
#    >>> idu.options # doctest: +NORMALIZE_WHITESPACE
#    cb_on=1, cb_autosave=0  
#    >>> idu.options.keys()    
#    ['cb_on', 'cb_autosave']
#    
#    Else, return nothing
#    
#    >>> s2= ' blah blah '
#    >>> idu2=load_idu(s2)    
#    >>> idu2  # Return nothing 
#             
#  '''
#  #s = s and s or ed.get_text_line(0) 
#  dft = (idu and idu ) or InDocUi()
#  idu = parse_idu_content( s )  
#  #if not idu: 
#    #msg= ed.dlg_menu( ed.MENU_STD, 'test_menu', 'test_item')
#    #input('Valid IDU init line not found. Create one ?')
#    
#  return idu and idu or None 
# 
#
#   
#def parse_idu_content( block_content ):
#  '''               
#     Return an InDocUi instance. 
#     
#  >>> blk= '/*=[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/'
#  >>> d=parse_idu_content( blk ) # doctest: +NORMALIZE_WHITESPACE
#  >>> d.options 
#  cb_autosave=0, cb_on=1
#  
#  >>> s = '/*= blah blah blah*/'
#  >>> d2 = parse_idu_content( s )
#  >>> d2 # Return nothing
#   
#  '''
#  #conout( 'parse_idu_content()');
#  d= idu_tokenize( block_content
#                 , 'IDU_PART') #token_set=SCANNER_RULES['IDU_PART'] )
#    # d:{'CHK_BOX': [(1, 25, '[ ]_autosave'), (1, 38, '[x]_on')], 
#    #    'NUM_LIST': [(1, 4, '[ 3, -4, 5.5 ]')], 
#    #    'BTN': [(1, 18, '{+|-|>}')]}                
#                  
#  if d:               
#    idu = InDocUi()
#    for k in d.keys():
#      if k=='CHK_BOX':
#        for rng, iL,cB, iLE, cBE, text in d[k]:
#        #line,col, nline, nc, text in d[k]: # text : '[ ]_autosave'
#           parts = text.split('_')             
#           var = '_'.join(parts[1:])
#           val = ( parts[0][1]!=' ' and 1) or 0 
#           idu.options[ 'cb_'+var ] = val
#                          
#    return idu
# 
 
def toggleBlock(ed, dlg_menu, menu_id, w_bi, w_len, word_at_i
                , isAutoSave 
                , file_save_func
                ):
    '''   
      Starting from w_bi, find text to toggle between the two below:
      
          resize(...){
                ...
                ...
          }
          
      and  

          /*resize(...){*/
                ...
                ...
          /*}*/
           
      To be called by on_click when clicking at a previously
      selected word
    
    '''  
    if not word_at_i: return  
    MENU_MAX_LOOP = 50
    #print('\nEnter toggleBlock')            
    doc = ed.get_text_all()

    pdoc = doc[w_bi:] #.replace('\n','')  
    ntBlk = o_p.findblock(pdoc)
    ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
    header = pdoc[:ibeg+1]  ## = "resize(...) {"
    #ntBlks = o_p.scanBlocks(pdoc)
    #print('ntBlk = ', ntBlk )
    if ntBlk:   
      #print('blks = ', blks )
      #blk = blks[0]
      #ibeg,iend,txt = blks[0] 
      #ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
      #print( 'ibeg,iend= ', ibeg,iend )                                         
      
      #header = pdoc[:ibeg+1]  ## = "resize(...) {"
      #print(' 3 elements: "%s", "%s", "%s"'%(doc[w_bi-2:w_bi], pdoc[ibeg+1:ibeg+3], pdoc[ iend-3:iend+2] ))
        
      ismenu=1
      counter = 0 
      while ismenu==1 and counter< MENU_MAX_LOOP:
        
        #print('*** enter while ***')       

        im= dlg_menu( menu_id, caption='Toggle block'
                    , text= 'toggle "%s" block\n-\n[done]'%word_at_i )  
        #print('menu select index = ', im)            
        if im!=0:
          ed.set_sel( w_bi-1, 0, nomove=True ) 
          ed.set_caret_pos( w_bi )
          #ed.set_sel( w_bi, len(word_at_i), nomove=True )
   
          ismenu=0
        else:              
          if (doc[w_bi-2:w_bi]=='/*' and pdoc[ibeg+1:ibeg+3]=='*/'
             and pdoc[ iend-3:iend+2]=='/*}*/' ):

            ed.replace( w_bi+iend-1-2, 5, '}')
            # Turn "resize(...){" into "/*resize(...){*/" 
            ed.replace( w_bi-2, ibeg+5, header ) 
            w_bi = w_bi-2           
             
          else:
                
            ed.replace( w_bi+iend-1, 1, '/*}*/')        
            # Turn "resize(...){" into "/*resize(...){*/" 
            ed.replace( w_bi, ibeg+1, '/*%s*/'%header )
            w_bi = w_bi+2
          ed.set_sel( w_bi, len(word_at_i), nomove=True )
          if isAutoSave: file_save_func()
  
#          if (doc[w_bi-2:w_bi]=='/*' and pdoc[ibeg+1:ibeg+3]=='*/'
#             and pdoc[ iend-3:iend+2]=='/*}*/' ):
#
#            ed.replace( w_bi+iend-1-2, 5, '}')
#            # Turn "resize(...){" into "/*resize(...){*/" 
#            ed.replace( w_bi-2, ibeg+5, header ) 
#             
#          else:
#                
#            ed.replace( w_bi+iend-1, 1, '/*}*/')        
#            # Turn "resize(...){" into "/*resize(...){*/" 
#            ed.replace( w_bi, ibeg+1, '/*%s*/'%header )
                          
        
        if counter>=MENU_MAX_LOOP:
          
          raise OverflowError(
            'Number of menu calls in toggleBlock() exceeds MENU_MAX_LOOP(%s)'%counter)
        if ismenu:
          doc = ed.get_text_all()
          pdoc = doc[w_bi:]   
          #print(' in while bottom, pdoc="%s", len=%s'%(pdoc,len(pdoc)))     
          ntBlk = o_p.findblock(pdoc) 
          #print(' in while bottom, ntBlk="%s"'%str(ntBlk))     
          ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
          #print( 'ibeg,iend= ', ibeg,iend )                                         
          header = pdoc[:ibeg+1]  ## = "resize(...) {"
                
       
    
if __name__=='__main__':
    #doctest.REPORT_NDIFF=True
    def test_docstr(func): 
      conout('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                
    #doctest.run_docstring_examples(test1, globals())
    #test_docstr( zdict ) 
    test_docstr( InDocUi ) 
    test_docstr( tokenize )  
    test_docstr( tokenize_to_rng_dict )
    test_docstr( tokenize_to_typ_dict )
    #test_docstr( get_idu_blocks )
    #test_docstr( parse_idu_content )
    #test_docstr( get_new_num )
#    test_docstr( load_idu )
    test_docstr( get_num_at_i )
    test_docstr( get_ntNum_b4_blk )
#    

"""
python re.Scanner examples:

https://docs.python.org/3.2/library/re.html#writing-a-tokenizer
http://www.programcreek.com/python/example/53972/re.Scanner
http://stackoverflow.com/questions/691148/pythonic-way-to-implement-a-tokenizer?lq=1
http://stackoverflow.com/questions/19280898/re-scanner-only-searching-start-of-string
"""