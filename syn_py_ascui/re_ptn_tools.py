######################################
#
#   RegExp Tools
#
######################################

#import os,sys
import re, doctest, collections

def ntBlk(blkname, typ, rng, rel_rng, txt ):
  return collections.namedtuple( blkname,
          ['typ','rng','rel_rng','txt'])( 
            typ, rng, rel_rng, txt)

def _c(txt): return r'('+txt+r')'
def _nc(txt): return r'(?:'+txt+r')'  

def _or_items( data, itemcap=''):
  if itemcap == 'c': data=[ _c(x) for x in data ]
  elif itemcap=='nc': data=[ _nc(x) for x in data ]
  return  _c( r'|'.join(data) )
  
def _or(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _or_items( data, itemcap='')
    
def _or_c(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _or_items( data, itemcap='c')

def _or_nc(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _or_items( data, itemcap='nc')

def _nc_or_items( data, itemcap=''):
  if itemcap == 'c': data=[ _c(x) for x in data ]
  elif itemcap=='nc': data=[ _nc(x) for x in data ]
  return  _nc( r'|'.join(data) )    
  
def _nc_or(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _nc_or_items( data, itemcap='')
    
def _nc_or_c(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _nc_or_items( data, itemcap='c')

def _nc_or_nc(*args): 
  data = (type(args[0]) in (list,tuple)) and args[0] or args
  return _nc_or_items( data, itemcap='nc')
            
def test_re_or():
    '''                     
    >>> _or( ('abc','def'))
    '(abc|def)'
    >>> _or( 'abc','def')
    '(abc|def)'
    
    >>> _or_c( 'abc','def')
    '((abc)|(def))'
    >>> _or_c( ('abc','def'))
    '((abc)|(def))'
                           
    >>> _or_c( 'abc','def')
    '((abc)|(def))'
    >>> _or_nc( ('abc','def'))
    '((?:abc)|(?:def))'
       
    >>> _nc_or( ('abc','def'))
    '(?:abc|def)'
    >>> _nc_or( 'abc','def')
    '(?:abc|def)'
    >>> _nc_or_c( 'abc','def')
    '(?:(abc)|(def))'
    >>> _nc_or_c( ('abc','def'))
    '(?:(abc)|(def))'
                           
    >>> _nc_or_c( 'abc','def')
    '(?:(abc)|(def))'
    >>> _nc_or_nc( ('abc','def'))
    '(?:(?:abc)|(?:def))'
    
    '''
#---------------------------------------------------------            

def _rep( data, count=r'*', counts=None, cap=''):
  '''
    >>> _rep('abc') 
    'abc*'
    
    >>> _rep('abc', '+')
    'abc+'
    
    >>> _rep('abc', count='?', cap='c')
    '(abc)?'
    
    >>> _rep('abc', count='*', cap='nc')
    '(?:abc)*'
    
    >>> _rep('abc', counts='2,3', cap='nc')
    '(?:abc){2,3}'
     
  '''
  if counts:
    return { 'nc' : r'(?:' + data+ r'){'+counts+ r'}'
           , 'c'  : r'(' + data+ r'){'+counts+ r'}'
           , ''   : data+ r'){'+counts+ r'}'  
           }[cap]  
  else:
    return { 'nc' : r'(?:' + data+ ')'+count
           , 'c'  : r'(' + data+ ')'+count  
           , ''   : data+ count 
           }[cap]         
           
def _nc_rep( data, count=r'*', counts=None):
  return _rep( data, count=count, counts=counts, cap = 'nc')
  
def _01(data):  return _rep(data, count=r'?', cap='c' )
def _0m(data):  return _rep(data, count=r'*', cap='c' )
def _1m(data):  
  '''    
  >>> _1m('abc')
  '(abc)+'
  '''
  return _rep(data, count=r'+', cap='c' )

def _nc_01(data): return _rep(data, count=r'?', cap='nc' )
def _nc_0m(data): return _rep(data, count=r'*', cap='nc' )
def _nc_1m(data): 
  '''    
  >>> _nc_1m('abc')
  '(?:abc)+'
  
  '''
  return _rep(data, count=r'+', cap='nc' )
def _0ms(): return r'\s*?'  
#---------------------------------------------------------            
def _af(pre, data): return r'(?<=%s)%s'%(pre,data)    # after
def _naf(pre, data): return r'(?<!%s)%s'%(pre,data)
def _fb(data, post): return r'%s(?=%s)'%(data,post)    #followed by
def _nfb( data, post): return r'%s(?!%s)'%(data,post)  
def _naf_nfb(pre,data,post): return r'(?<!%s)%s(?!%s)'%(pre,data,post)

  

def re_test():
  ''' Test the re and token using doctest
  ''' 
  def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                
          
  test_docstr( test_re_or )
  test_docstr( _1m )
  test_docstr( _nc_1m )
  test_docstr( _rep )
    
if __name__=='__main__':
    re_test()