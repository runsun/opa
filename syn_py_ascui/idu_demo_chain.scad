      
$fn= 16; /*={+ - +10 -7} */
                   
module Axes(){
  rLine=0.1;
  color("red",0.3) hull() {
    sphere(rLine); translate([10,0,0]) sphere(rLine);}
  color("green",0.3) hull() {
    sphere(rLine); translate([0,10,0]) sphere(rLine);}
  color("blue",0.3) hull() {
    sphere(rLine); translate([0,0,10]) sphere(rLine);}
}   

module Chain( nodes=[ [1, [0,0,0] ], [1, [3,3,3] ]] ) 
{ 
   for (i= [0:len(nodes)-2] ) 
      hull(){ 
        translate( nodes[i][1] ) sphere( nodes[i][0] );              
        translate( nodes[i+1][1] ) sphere( nodes[i+1][0] );              
      }
}                     

Chain([ 
       [ 2 , [-3.6 , 0 , 0 ] ]
     , [ 1.4 , [3 , 1.3 , 0 ] ]
     , [ 1.1 , [5 , 5 , 0 ] ]
     , [ 0.8 , [0 , 10 , 0 ] ]
     , [ 0.0375 , [-7 , 3.3 , 3 ] ]
     ] );

