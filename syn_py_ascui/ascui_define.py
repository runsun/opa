
'''
Design an AscUI in-doc control for OpenSCAD:
 
'''
#from sw import *
import re, pprint, os, sys, doctest
this_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, this_dir)

#print('test')
from re_ptn_tools import *
##########################################
# Somehow "import re_ptn_tools" works 
#  but not "from re_ptn_tools import *" 
# So we have to do this the hard ugly way:
import re_ptn_tools 
unitnames = [k for k in dir(sys.modules['re_ptn_tools']) ]
for k in unitnames:
  if not k.endswith('__'):
    globals()[k]= getattr(re_ptn_tools,k)  
#########################################3

#
#  RE_??? : python re pattern
#  IDU_??? : py re pattern for AscUI
#

# Define common RE
# 
RADIO_BOX = (r'\(',r'\)')
RADIO_MARK= r'\*'
CHK_BOX = (r'\[',r'\]')
CHK_MARK= 'x'
BTN   = (r'{',r'}')


#================================================
IDU_BEG_CHAR = (r'\.', r'\=', r'\^')
#IDU_BEG = r'/\*(?:(?:\[(\w+)*\])+)*(?:'+ '|'.join(IDU_BEG_CHAR)+')'
#IDU_BEG = r'/\*(?:(?:\[(\w+)*\])+)*' + _or_nc(IDU_BEG_CHAR)
#IDU_BEG = r'/\*'+ _0m_nc('(?:\[(\w+)*\])+') + _or_nc(IDU_BEG_CHAR)
#IDU_BEG = r'/\*'+ _0m_nc(_01_nc('\[(\w+)*\]')) + _or_nc(IDU_BEG_CHAR)

IDU_BEG = r'/\*'+ _nc_01('\[(\w+)*\]') + _nc_or(IDU_BEG_CHAR)
         #   match:           .group()      .groups()            
         # "/*."              '/*.'         (None,)
         # "/*="              '/*='         (None,)
         # "/*^"              '/*^'         (None,)
         # "/*[abc]."         '/*[abc].     ('abc',) 
         # "/*[abc]="         '/*[abc]=     ('abc',) 
         # "/*[abc]^"         '/*[abc]^     ('abc',) 
def test_IDU_BEG():
  r'''
     
     >>> p = re.compile(IDU_BEG)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('/*.', (None,))
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('/*=', (None,))
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('/*^', (None,))
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('/*[label1].', ('label1',))
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('/*[abc]=', ('abc',))
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('/*[ttt]^', ('ttt',))
     
  ''' 
#
          
IDU_END = r'\*/'
def test_IDU_END():
  r'''
          
     >>> p = re.compile(IDU_END)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('*/', ())
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('*/', ())
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('*/', ())
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('*/', ())
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('*/', ())
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('*/', ())
     
  '''   
#  

IDU_BLK0 = r'(?s)'+IDU_BEG + r'.*?' + IDU_END  
#IDU_BLK  = r'(?:/\*\*/|' +IDU_BLK0 + r')'       ## Add "/**/"
IDU_BLK = _nc_or( '/\*\*/', IDU_BLK0)         ## Add "/**/"
 
def test_IDU_BLK():
  r'''
     
     >>> p = re.compile(IDU_BLK)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('/*. blah blah .*/', (None,))
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('/*= blah blah .*/', (None,))
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('/*^ blah blah .*/', (None,))
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('/*[label1]. blah blah .*/', ('label1',))
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('/*[abc]= blah blah .*/', ('abc',))
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('/*[ttt]^ blah blah .*/', ('ttt',))
     
     --------------------------------
     >>> f( r' b=3; /**/' ) 
     ('/**/', (None,))
     
     
  '''  
#

#================================================
# Identifier in OpenSCAD: two cases: starts with $ or not
# For OpenSCAD, these are valid identifiers: 
#   $123, 123ab_
# These are not :
#   123, 123$a, abc$def 
#
#RE_ID = ( r'(?:'              # Not capture this group (already captured below)
#        +     r'(?:'            # The case that starts with $ 
#        +         r'(?<=\W)'    # Must be preceded by non-word to avoid 4$a, b$b
#        +         r'\$\w+'      # $ and word ( =[A-Za-z_0-9] ) 
#        +     r')' 
#        +     r'|'           # or 
#        +     r'(?:[0-9]*[A-Za-z_]+\w*\b)'  # The case not starts with $
#        + r')'
#        + r'(?!\$)' # Negative lookahead = Should not be followed by $
#        )           
RE_ID = _nfb( _nc_or( # Not capture 'or' (already captured below)
                  
                    _af( r'\W', r'\$\w+' ) ## That starts with $ 
                                    ## $ and followed by word ( =[A-Za-z_0-9] )
                                    ## after non-word to avoid 4$a, b$b
                        
                   , r'[0-9]*[A-Za-z_]+\w*\b'  # That not starts with $  
                   
                   ) 
            ,r'\$'  # not followed by $
            )              
        
RE_ID_C = _c(RE_ID)
   
def test_RE_ID():
  r'''
     
     >>> p = re.compile( RE_ID)
     >>> f = lambda s: ( p.search(s) and 
     ...                 p.search(s).group()) or None  
     >>> f2 = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
          
     --------------------------------
     >>> f( r' b=3; d_=4; 23c=5; $4=6;' ) 
     'b'
     >>> f( r' b$=3; d_=4; 23c=5; $4=6;' ) 
     'd_'
     >>> f( r' 23c=5; $4=6;' ) 
     '23c'
     >>> f( r' 23_=5; $4=6;' ) 
     '23_'
     >>> f( r' _23_=5; $4=6;' ) 
     '_23_'
     >>> f( r' $4=6;' ) 
     '$4'
     >>> f( r' $__=6;' ) 
     '$__'
     >>> f( r' 4$4=6;' ) 
     >>> f( r' ab$4=6;' ) 
     >>> f( r' 4b$4=6;' ) 
  '''           
#
      
#================================================
RE_STR= r'(?s)("(?:\\"|.)*?")' # (?s): Dot matches newline characters
                               # (?: ): non capturing group. We capture
                               #         "xxx", but not xxx 
                               # \" needs to be considered 
RE_STR= r'("(?:\\"|.)*?")'  # Consider multiline
RE_STR = _c('"'+ _nc('\\\\"|.')+'*?"')           
  
def test_RE_STR():
  r'''       
     >>> p = re.compile( RE_STR)
     >>> f = lambda s: ( p.search(s) and 
     ...                 p.search(s).group()) or None  
     
     --------------------------------
     >>> f( 'blah blah "abc" ...' ) 
     '"abc"'
     >>> f( r'blah blah "ab\"c" ...' ) 
     '"ab\\"c"'
  '''     
      

#================================================
RE_SYM= r'[\{\},\<\>;\#\(\)\[\]=\.\*\+\|\$-\?/&@]'
#================================================
#   [ n0, n1, n2 ...]
# 
RE_NUM= r'-?\d*\.?\d+'
#RE_NUM_C = r'('+RE_NUM+')'  # non-capturing number
#RE_NUM_NC = r'(?:-?\d*\.?\d+)'  # non-capturing number
RE_NUM_C = _c(RE_NUM)  # non-capturing number
RE_NUM_NC= _nc(RE_NUM)  # non-capturing number

#RE_NUM_LIST = r'\[\s*?'+RE_NUM_NC+r'(\s*?,\s*?'+RE_NUM_NC+r')*\s*?\]'  
RE_NUM_LIST= ( r'\['+ _0ms() + RE_NUM_NC 
                   + _nc_0m(  _0ms()+','+_0ms() + RE_NUM_NC )
                   + _0ms() 
             + r'\]')   
             
RE_ASSIGN= r'('+RE_ID+r')\s*?\=\s*?('+RE_NUM+')\s*?'  # match "width= 34.5;"
RE_ASSIGN= RE_ID_C+r'\s*?\=\s*?('+RE_NUM+')\s*?'  # match "width= 34.5;"
def test_RE_ASSIGN():
  r'''                 
     This re will be used in the following situations:
     
      width=4;        /*=[5,6,7] {+|-} =*/
      sphere( r=3 )   /*=[5,6,7] {+|-} =*/
     
     >>> p = re.compile( RE_ASSIGN)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah abc  = 3; def=5; ...' ) 
     ('abc  = 3', ('abc', '3'))
   
     >>> f( 'func(d = 3 , def=5; ...' ) 
     ('d = 3', ('d', '3'))
     
          
  ''' 
#

RE_RANGE= r'\['+RE_NUM_NC + r'(?::'+RE_NUM_NC+r'){1,2}\]'
RE_RANGE= ( r'\['+ _0ms() + RE_NUM_NC 
                 + _nc_rep(  _0ms()+':'+_0ms() + _nc(RE_NUM), counts='1,2' )
                 + _0ms() 
          + r'\]') 

def test_RE_RANGE():
  r'''
    >>> p = re.compile( RE_RANGE )
    >>> f = lambda s: ( p.search(s) and 
    ...                 ( p.search(s).group(), p.search(s).groups() )
    ...               ) or None  

    --------------------------------
    >>> f(' blah [3: 4.5 ] blah ')       
    ('[3: 4.5 ]', ())
    
    >>> f(' blah [3:4.5:9] blah ')       
    ('[3:4.5:9]', ())
    
    >>> f(' blah [ -10:-5] blah ')       
    ('[ -10:-5]', ())
    
    >>> f(' blah [6:-2: -4] blah ')       
    ('[6:-2: -4]', ())
  '''

#=================================================================
#  IDU controls:
#
#  Radiobox:  ( )_label1  or (*)_label2  
IDU_RADIO_BOX= RADIO_BOX[0]+r'( |'+RADIO_MARK+r')'+RADIO_BOX[1]+'_'+RE_ID_C
def test_IDU_RADIO_BOX():
  r'''     
     >>> p = re.compile( IDU_RADIO_BOX)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah ( )_abc ...' ) 
     ('( )_abc', (' ', 'abc'))
     
     >>> f( 'blah blah ( )_border_width ...' ) 
     ('( )_border_width', (' ', 'border_width'))
     
     >>> f( 'blah blah (*)_123b ...' ) 
     ('(*)_123b', ('*', '123b'))
     
     >>> f( 'blah blah ( )_123 ...' )  # 123 is not a valid OpenSCAD identifier

  ''' 

#  Radio data: ( )_width=3;height=3;
IDU_RADIO_DATA= ( RADIO_BOX[0]+r'( |'+RADIO_MARK+r')'+RADIO_BOX[1]+'_('
                  + RE_ASSIGN +')+' 
                )        
                
# check box: [ ]_label or [x]_label               
IDU_CHK_BOX= CHK_BOX[0]+r'( |'+CHK_MARK+r')'+CHK_BOX[1]+'_'+RE_ID_C
def test_IDU_CHK_BOX():
  r'''       
     >>> p = re.compile( IDU_CHK_BOX)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah [ ]_abc ...' ) 
     ('[ ]_abc', (' ', 'abc'))
    
     >>> f( 'blah blah [x]_border_width ...' ) 
     ('[x]_border_width', ('x', 'border_width'))
    
     >>> f( 'blah blah [x]_123b ...' ) 
     ('[x]_123b', ('x', '123b'))
         
     >>> f( 'blah blah [ ]_123 ...' )  # 123 is not a valid OpenSCAD identifier

  ''' 
#

IDU_BTN_LABEL_PARTS = (r'\+',r'\-',r'\*',r'\/',r'\>',r'\<')

# IDU_BTN_LABEL1: For one btn label, like '+', '+2.5','>'
#IDU_BTN_LABEL1= ( r'(?:'+ '|'.join(IDU_BTN_LABEL_PARTS)
#                + r')(?:' +RE_NUM+')?'
#                ) 
#
#def test_IDU_BTN_LABEL1():
#  r'''       
#     >>> p = re.compile( IDU_BTN_LABEL1 )
#     >>> f = lambda s: ( p.search(s) and 
#     ...                 ( p.search(s).group(), p.search(s).groups() )
#     ...               ) or None  
#     
#     --------------------------------
#     >>> f( 'blah / + ...' ) 
#     ('/', ())
#          
#     >>> f( 'blah -2.3 + ...' ) 
#     ('-2.3', ())
#     
#     >>> f( 'blah >2 + ...' ) 
#     ('>2', ())
#         
#  ''' 
##
#
#IDU_BTN_LABEL1_C= ( r'('+ '|'.join(IDU_BTN_LABEL_PARTS)
#                + r')(' +RE_NUM+')?'
#                ) 

#def test_IDU_BTN_LABEL1_C():
#  r'''       
#     >>> p = re.compile( IDU_BTN_LABEL1_C )
#     >>> f = lambda s: ( p.search(s) and 
#     ...                 ( p.search(s).group(), p.search(s).groups() )
#     ...               ) or None  
#     
#     --------------------------------
#     >>> f( 'blah / + ...' ) 
#     ('/', ('/', None))
#          
#     >>> f( 'blah -2.3 + ...' ) 
#     ('-2.3', ('-', '2.3'))
#     
#     >>> f( 'blah >2 + ...' ) 
#     ('>2', ('>', '2'))
#         
#  ''' 
#        
  
## 
                     
#IDU_BTN_LABEL_C = r'('+IDU_BTN_LABEL1+')'                                     

RE_BTN_LABEL= r'(?:(?:(?:\+|\-|(?<!\/)\*(?!\/)|(?<!\*)\/(?!\*))'+RE_NUM_C+r'*)|(?:<|>))'
  # NOTE: (?<!\/)\*(?!\/) : * not preceded or followed by a / 
  #       (?<!\*)\/(?!\*) : / not preceded or followed by a *  
#print(RE_BTN_LABEL)  
SYM_MUL = _naf_nfb( '/','\*','/' )
SYM_DIV = _naf_nfb( '\*','/','\*' )

IDU_BTN_LABEL= _nc_or(
                   _nc_or('\+','\-',SYM_MUL,SYM_DIV)+_nc_0m(RE_NUM_NC)
                 , _nc_or('<','>')
              )
#print(RE_BTN_LABEL)  


def test_IDU_BTN_LABEL():
  '''             
    match: "+", "-", "/", "*" and their numbered versions:
           "+1.5","-0.8" ... 
    Also match:
          ">", "<" 
    >>> p = re.compile( IDU_BTN_LABEL)
    >>> f = lambda s: ( p.search(s) and 
    ...                 ( p.search(s).group(), p.search(s).groups() )
    ...               ) or None  

    --------------------------------
    >>> f('blah blah {+}')
    ('+', ())
    >>> f('blah blah {+2.3}')
    ('+2.3', ())
    >>> f('blah blah {>}')
    ('>', ())
    >>> f('blah blah {/3}')
    ('/3', ())
    >>> f('blah blah {/*}')   # / and * should not be together
    >>> f('blah blah {*/}')   # / and * should not be together
  '''                           
  
IDU_BTN_LABEL_ALL= ( IDU_BTN_LABEL
                   + r'(?:.(?:'+ IDU_BTN_LABEL 
                   + r'))*'
                   ) 
IDU_BTN_LABEL_ALL = ( IDU_BTN_LABEL
                    + _nc_0m( '.'+_nc(IDU_BTN_LABEL))
                    )                           

def test_IDU_BTN_LABEL_ALL():
  r'''       
     >>> p = re.compile( IDU_BTN_LABEL_ALL )
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah {+2 -5 +6 +4 /3 ...' ) 
     ('+2 -5 +6 +4 /3', ())
     
     >>> f( 'blah > +3 ...' ) 
     ('> +3', ())
     
     >>> f( 'blah >,+3,-,< ...' ) 
     ('>,+3,-,<', ())
           
  ''' 
#

#IDU_BTN_LABEL_ALL_GROUPS= ( IDU_BTN_LABEL
#                   + r'(.'+ IDU_BTN_LABEL 
#                   + r')*'
#                   )    
#def test_IDU_BTN_LABEL_ALL_GROUPS():
#  r'''       
#     >>> p = re.compile( IDU_BTN_LABEL_ALL_GROUPS )
#     >>> f = lambda s: ( p.search(s) and 
#     ...                 ( p.search(s).group(), p.search(s).groups() )
#     ...               ) or None  
#     
#     --------------------------------
#     >>> f( 'blah >2 +3 ...' ) 
#     ('>2 +3', ('>', '2', '+', '3'))
#     
#     >>> f( 'blah >2,+3,-,< ...' ) 
#     ('>2,+3,-,<', ('>', '2', '+', '3', '-', '', '<', ''))
#           
#  ''' 
                                          
# IDU_BTN_LABEL_C = r'('+IDU_BTN_LABEL1+')'                                     

IDU_BTN= (BTN[0]+ r' *?('+IDU_BTN_LABEL_ALL
         + r') *?'+ BTN[1]
         )             
         # {+}
         # {+ -3 > }
          
IDU_BTN= BTN[0]+ _0ms() + IDU_BTN_LABEL_ALL + _0ms() + BTN[1] 
                  
def test_IDU_BTN():
  r'''       
     >>> p = re.compile( IDU_BTN )
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah {+} ...' ) 
     ('{+}', ())
          
     >>> f( 'blah blah {+ -} ...' ) 
     ('{+ -}', ())
     
     >>> f( 'blah blah {*2 / < > +1.5 -0.1} ...' ) 
     ('{*2 / < > +1.5 -0.1}', ())
        
        
  ''' 
#


SCANNER_RULES={

  "IDU_LIST":[ ("["     ,"\["),
             ('ID',        RE_ID),  
             ('NUMBER',    RE_NUM),
             ('STRING',    RE_STR),   
             ("]",         "\]")
             ]
  ,"IDU_CTRL":[
              ("RADIO_BOX", IDU_RADIO_BOX),
              ("CHK_BOX", IDU_CHK_BOX),
              ("BTN", IDU_BTN),
              ('SKIP',    r'([ \t\n]|'+IDU_BEG+r'|'+IDU_END+'|' +RE_ID+'|'+RE_SYM+ r')' ),
             ]
  ,"IDU_BLK": [ ('IDU_BLK',   IDU_BLK),   # IDU_BLK will match \n, too. 
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"ASSIGN": [ ('ASSIGN',   RE_ASSIGN),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"BTN_LABEL": [ ('BTN_LABEL',   RE_BTN_LABEL),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]    
  ,"NUM_LIST": [ ('NUM_LIST',   RE_NUM_LIST),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"RANGE": [ ('RANGE',   RE_RANGE),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]    
  ,"NUM": [ ('NUM',   RE_NUM),   # IDU_BLK will match \n, too. 
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]  
  ,"IDU_PART":[
      ('NUM_LIST',  RE_NUM_LIST),
      ('RADIO_DATA',IDU_RADIO_DATA),
      ('RADIO_BOX', IDU_RADIO_BOX),
      ('CHK_BOX',   IDU_CHK_BOX),
      ('BTN',       IDU_BTN),
      ('SKIP',    r'([ \t\n]|'+IDU_BEG+r'|'+IDU_END+'|' +RE_ID+'|'+RE_SYM+ r')' ),
      ('IDU_BEG', IDU_BEG),
      ('IDU_END', IDU_END),
      ]         
  ,"IDU_LINE": [
      ('SETVAR',    RE_ASSIGN),
      ('ID',        RE_ID),  
      ('IDU_BEG',   IDU_BEG),
      ('IDU_END',   IDU_END),
      ('BTN',       IDU_BTN ),
      ('RADIO_BOX', IDU_RADIO_BOX), 
      ('CHK_BOX',   IDU_CHK_BOX), 
      ('NUM_LIST',  RE_NUM_LIST), # Integer or decimal number
      ('NUMBER',    RE_NUM), # Integer or decimal number
      ('SYMBOL', RE_SYM), #
      ('NEWLINE', r'\n'),           #Line endings
      ('SKIP',    r'[ \t\n]'),        #Skip over spaces and tabs
    ]  
}  
  
  
def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                

def re_test():
  ''' Test the re and token using doctest
  ''' 
  #test_docstr( _or )
  test_docstr( _rep )
  test_docstr( test_re_or )
  #test_docstr( _1m_nc )
  test_docstr( test_IDU_BEG )
  test_docstr( test_IDU_END )
  test_docstr( test_IDU_BLK )
  test_docstr( test_RE_ID ) 
  test_docstr( test_RE_STR )
  test_docstr( test_RE_ASSIGN ) 
  test_docstr( test_IDU_RADIO_BOX )  
  test_docstr( test_IDU_CHK_BOX ) 
  test_docstr( test_IDU_BTN_LABEL )
  test_docstr( test_IDU_BTN_LABEL_ALL )
  test_docstr( test_IDU_BTN ) 
  test_docstr( test_RE_RANGE )

    
if __name__=='__main__':
    #doctest.REPORT_NDIFF=True
    #doctest.run_docstring_examples(test1, globals()) 
    ##doctest.run_docstring_examples(AscGrid.addpxdata, globals())
    #test_autoscale()  
    #test_docstr( re_test ) 
    re_test()
"""   

http://www.programcreek.com/python/example/53972/re.Scanner
def _tokenize(self):
        '''
        #Lex the input text into tokens and yield them in sequence.
        '''
        # scanner callbacks
        def bool_(scanner, t): return bool_token(t)
        def identifier(scanner, t): return ident_token(t)
        def integer(scanner, t): return int_token(t)
        def eq(scanner, t): return eq_op_token()
        def neq(scanner, t): return neq_op_token()
        def or_(scanner, t): return or_op_token()
        def and_(scanner, t): return and_op_token()
        def lparen(scanner, t): return lparen_token()
        def rparen(scanner, t): return rparen_token()
        def string_(scanner, t): return string_token(t)
        def not_(scanner, t): return not_op_token()

        scanner = re.Scanner([
            (r"true|false", bool_),
            (r"[a-zA-Z_]\w*", identifier),
            (r"[0-9]+", integer),
            (r'("[^"]*")|(\'[^\']*\')', string_),
            (r"==", eq),
            (r"!=", neq),
            (r"\|\|", or_),
            (r"!", not_),
            (r"&&", and_),
            (r"\(", lparen),
            (r"\)", rparen),
            (r"\s+", None), # skip whitespace
            ])
        tokens, remainder = scanner.scan(self.text)
        for t in tokens:
            yield t
        yield end_token()
        
"""        

"""
python re.Scanner examples:

https://docs.python.org/3.2/library/re.html#writing-a-tokenizer
http://www.programcreek.com/python/example/53972/re.Scanner
http://stackoverflow.com/questions/691148/pythonic-way-to-implement-a-tokenizer?lq=1
http://stackoverflow.com/questions/19280898/re-scanner-only-searching-start-of-string
"""