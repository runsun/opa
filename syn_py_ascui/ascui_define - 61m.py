
'''
Design an AscUI in-doc control for OpenSCAD:
(to be used in on_caret_move() event handler in SynWrite)

An ascui block:  //.> ... //<

  { }.Label   // a checkBox
  ( ).Label   // a radioBox
  [_Label_]   // a button
  [_x|y|z_]   // collection of buttons
  [ ]         // a textbox
        
//.>               
 
 { } Save_Upon_Change
 ( )


===================
I. Block controls:
===================
 
1) Init line 
--------------
     
/*==[idu] [ ]_on [ ]_reverse [ ]_autosave ==*/
   
  [ ]_on: checked: all idu blocks are handled
          unchecked: all idu blocks are not handled, means
            mouse click will have no effect. 

One setting per line:
           
/*==[idu] [ ]_edit
  [ ]_on
  [ ]_autosave
  [ ]_render
  var1=( )on( )off
  
==*/

b=3; /*==[3,4,5] {+|-|+3|-2|>|<} ==*/

/*==[idu]
  [ ]_on
  [ ]_autosave
  [ ]_render
  var1=( )on ( )off ( )default
    
  .b= [3,4,5] {+|-|+3|-2|>|<} .autosave[ ] 
  .d= [4,5,6] {+|-|+3|-2|>|<}  
  .c= ( )h=3;w=2;
      ( )h=3;w=2;
  .codes=
    [  (
         ( )width=3; height=3;
         ( )width=5; height=1;
       )   
    
    ]
==*/        

/*==[idu:global]
  [ ]_on           // comment 
  [ ]_autosave     // comment  
  [ ]_render       // 
  [ ]_cycling          
  a_var = ( )_val1 ( )_val2 ( )_val3 
  another= ( )_val4 ( )_val5
==*/

  * creates idu.options.on= False
            idu.options.autosave= False
            idu.options.render= False
            idu.options.cycling= False
  * creates idu.options.other_var= 'val1' | 'val2' | 'val3'
          
2) Data Block
--------------
  
# Place this at the very end of doc.
# Each item should contains valid OpenSCAD code (except starting with ( )_)

/*==[idu:data]
  ( )_width=3;height=3;
  ( )_width=4;height=5;    
  [ ]_autosave 
  {<|>} 
==*/
 < data picked above will be shown here as OpenSCAD variable settings>
//==[idu:data_end]

===================
II. Line controls:
===================

  width= 3; /*==[3,4,5] 
            {<|>|+|-|+3|-2} 
             [ ]_cycling
             [ ]_autosave
            ==*/
  
  <var>=<val>; /*( <val_list>{<btn>}[ ]chkbox_label )*/
  
  <var>: id
  <val>: number
  <val_list>: [n1,n2 ...] , range: [1:2:20]
  
  {<btn>}:  {<|>}
            {+|-}
            {+3|-2|>|<}                   
  [ ]_on [ ]_save ( )_cycle ( )_u_turn            
                                   
  width=3; /*( [4,5,6]{<|>} )*/
  
'''
#from sw import *
import re, pprint, doctest
#IS_DEBUG = 0

#
#  RE_??? : python re pattern
#  IDU_??? : py re pattern for AscUI
#

# Define common RE
# 
RADIO_BOX = (r'\(',r'\)')
RADIO_MARK= r'\*'
CHK_BOX = (r'\[',r'\]')
CHK_MARK= 'x'
BTN   = (r'{',r'}')


#================================================
IDU_BEG_CHAR = (r'\.', r'\=', r'\^')
IDU_BEG = r'/\*(?:(?:\[(\w+)*\])+)*(?:'+ '|'.join(IDU_BEG_CHAR)+')'
         #   match:           .group()      .groups()            
         # "/*."              '/*.'         (None,)
         # "/*="              '/*='         (None,)
         # "/*^"              '/*^'         (None,)
         # "/*[abc]."         '/*[abc].     ('abc',) 
         # "/*[abc]="         '/*[abc]=     ('abc',) 
         # "/*[abc]^"         '/*[abc]^     ('abc',) 
          
def test_IDU_BEG():
  r'''
     
     >>> p = re.compile(IDU_BEG)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('/*.', (None,))
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('/*=', (None,))
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('/*^', (None,))
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('/*[label1].', ('label1',))
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('/*[abc]=', ('abc',))
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('/*[ttt]^', ('ttt',))
     
  '''         
IDU_END = r'\*/'


def test_IDU_END():
  r'''
          
     >>> p = re.compile(IDU_END)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('*/', ())
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('*/', ())
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('*/', ())
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('*/', ())
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('*/', ())
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('*/', ())
     
  '''   
  

IDU_BLK0 = r'(?s)'+IDU_BEG + r'.*?' + IDU_END  
IDU_BLK  = r'(?:/\*\*/|' +IDU_BLK0 + r')'       ## Add "/**/" 

#IDU_BLK  = '/\\*\\*/'
#print('IDU_BLK = ', IDU_BLK)
def test_IDU_BLK():
  r'''
     
     >>> p = re.compile(IDU_BLK)
     >>> f = lambda s: ( p.search(s) and 
     ...     (p.search(s).group(), p.search(s).groups())) or None
     
     --------------------------------
     >>> f( r' b=3; /*. blah blah .*/' ) 
     ('/*. blah blah .*/', (None,))
     
     >>> f( r' b=3; /*= blah blah .*/' )     
     ('/*= blah blah .*/', (None,))
     
     >>> f( r' b=3; /*^ blah blah .*/' )
     ('/*^ blah blah .*/', (None,))
     
     --------------------------------
     >>> f( r' b=3; /*[label1]. blah blah .*/' ) 
     ('/*[label1]. blah blah .*/', ('label1',))
     
     >>> f( r' b=3; /*[abc]= blah blah .*/' )     
     ('/*[abc]= blah blah .*/', ('abc',))
     
     >>> f( r' b=3; /*[ttt]^ blah blah .*/' )
     ('/*[ttt]^ blah blah .*/', ('ttt',))
     
     --------------------------------
     >>> f( r' b=3; /**/' ) 
     ('/**/', (None,))
     
     
  '''  

IDU_INIT_LINE = '/*.[idu] [x]_on [ ]_autosave [ ]_uturn .*/'
#================================================
# For OpenSCAD, these are valid identifiers: 
#   $123, 123ab_
# These are not :
#   123, 123$a, abc$def 

# Identifier in OpenSCAD: two cases: starts with $ or not

RE_ID = ( r'(?:'              # Not capture this group (already captured below)
        +     r'(?:'            # The case that starts with $ 
        +         r'(?<=\W)'    # Must be preceded by non-word to avoid 4$a, b$b
        +         r'\$\w+'      # $ and word ( =[A-Za-z_0-9] ) 
        +     r')' 
        +     r'|'           # or 
        +     r'(?:[0-9]*[A-Za-z_]+\w*\b)'  # The case not starts with $
        + r')'
        + r'(?!\$)' # Negative lookahead = Should not be followed by $
        ) 
RE_ID_C = r'('+RE_ID+r')'
   
def test_RE_ID():
  r'''
     
     >>> p = re.compile( RE_ID)
     >>> f = lambda s: ( p.search(s) and 
     ...                 p.search(s).group()) or None  
     >>> f2 = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
          
     --------------------------------
     >>> f( r' b=3; d_=4; 23c=5; $4=6;' ) 
     'b'
     >>> f( r' b$=3; d_=4; 23c=5; $4=6;' ) 
     'd_'
     >>> f( r' 23c=5; $4=6;' ) 
     '23c'
     >>> f( r' 23_=5; $4=6;' ) 
     '23_'
     >>> f( r' _23_=5; $4=6;' ) 
     '_23_'
     >>> f( r' $4=6;' ) 
     '$4'
     >>> f( r' $__=6;' ) 
     '$__'
     >>> f( r' 4$4=6;' ) 
     >>> f( r' ab$4=6;' ) 
     >>> f( r' 4b$4=6;' ) 
  '''           
      
#IDU_END = r'\.\.\*/'
#IDU_BLK = r'(?s)'+IDU_BEG + r'.*?' + IDU_END 
IDU_INIT_LINE = '/*.[idu] [x]_on [ ]_autosave [ ]_uturn .*/'
#================================================
RE_STR= r'(?s)("(?:\\"|.)*?")' # (?s): Dot matches newline characters
                               # (?: ): non capturing group. We capture
                               #         "xxx", but not xxx 
                               # \" needs to be considered 
     
def test_RE_STR():
  r'''       
     >>> p = re.compile( RE_STR)
     >>> f = lambda s: ( p.search(s) and 
     ...                 p.search(s).group()) or None  
     
     --------------------------------
     >>> f( 'blah blah "abc" ...' ) 
     '"abc"'
     >>> f( r'blah blah "ab\"c" ...' ) 
     '"ab\\"c"'
  '''     
      
#IDU_END = r'\.\.\*/'
#IDU_BLK = r'(?s)'+IDU_BEG + r'.*?' + IDU_END 
IDU_INIT_LINE = '/*.[idu] [x]_on [ ]_autosave [ ]_uturn .*/'

#================================================
RE_SYM= r'[\{\},\<\>;\#\(\)\[\]=\.\*\+\|\$-\?/&@]'
#================================================
#   [ n0, n1, n2 ...]
# 
RE_NUM= r'-?\d*\.?\d+'
RE_NUM_C = r'('+RE_NUM+')'  # non-capturing number
RE_NUM_NC = r'(?:-?\d*\.?\d+)'  # non-capturing number
RE_NUM_LIST = r'\[\s*?'+RE_NUM_NC+r'(\s*?,\s*?'+RE_NUM_NC+r')*\s*?\]'    
RE_ASSIGN= r'('+RE_ID+r')\s*?\=\s*?('+RE_NUM+')\s*?'  # match "width= 34.5;"
RE_ASSIGN= RE_ID_C+r'\s*?\=\s*?('+RE_NUM+')\s*?'  # match "width= 34.5;"
RE_RANGE= r'\['+RE_NUM_NC + r'(?::'+RE_NUM_NC+r'){1,2}\]'
def test_RE_RANGE():
  r'''
    >>> p = re.compile( RE_RANGE )
    >>> f = lambda s: ( p.search(s) and 
    ...                 ( p.search(s).group(), p.search(s).groups() )
    ...               ) or None  

    --------------------------------
    >>> f(' blah [3:4.5] blah ')       
    ('[3:4.5]', ())
    
    >>> f(' blah [3:4.5:9] blah ')       
    ('[3:4.5:9]', ())
    
    >>> f(' blah [-10:-5] blah ')       
    ('[-10:-5]', ())
    
    >>> f(' blah [6:-2:-4] blah ')       
    ('[6:-2:-4]', ())
      
  '''

def test_RE_ASSIGN():
  r'''                 
     This re will be used in the following situations:
     
      width=4;        /*=[5,6,7] {+|-} =*/
      sphere( r=3 )   /*=[5,6,7] {+|-} =*/
     
     >>> p = re.compile( RE_ASSIGN)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah abc  = 3; def=5; ...' ) 
     ('abc  = 3', ('abc', '3'))
   
     >>> f( 'func(d = 3 , def=5; ...' ) 
     ('d = 3', ('d', '3'))
     
  ''' 

RE_BTN_LABEL= r'(?:(?:(?:\+|\-|(?<!\/)\*(?!\/)|(?<!\*)\/(?!\*))'+RE_NUM_C+r'*)|(?:<|>))'
  # NOTE: (?<!\/)\*(?!\/) : * not preceded or followed by a / 
  #       (?<!\*)\/(?!\*) : / not preceded or followed by a *

def test_RE_BTN_LABEL():
  '''             
    match: "+", "-", "/", "*" and their numbered versions:
           "+1.5","-0.8" ... 
    Also match:
          ">", "<" 
    >>> p = re.compile( RE_BTN_LABEL)
    >>> f = lambda s: ( p.search(s) and 
    ...                 ( p.search(s).group(), p.search(s).groups() )
    ...               ) or None  

    --------------------------------
    >>> f('blah blah {+}')
    ('+', (None,))
    >>> f('blah blah {+2.3}')
    ('+2.3', ('2.3',))
    >>> f('blah blah {>}')
    ('>', (None,))
    >>> f('blah blah {/3}')
    ('/3', ('3',))
    >>> f('blah blah {/*}')   # / and * should not be together
    >>> f('blah blah {*/}')   # / and * should not be together
    
    
    
  '''          
#=================================================================
#  IDU controls:
#
#  Radiobox:  ( )_label1  or (*)_label2  
IDU_RADIO_BOX= RADIO_BOX[0]+r'( |'+RADIO_MARK+r')'+RADIO_BOX[1]+'_'+RE_ID_C
def test_IDU_RADIO_BOX():
  r'''     
     >>> p = re.compile( IDU_RADIO_BOX)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah ( )_abc ...' ) 
     ('( )_abc', (' ', 'abc'))
     
     >>> f( 'blah blah ( )_border_width ...' ) 
     ('( )_border_width', (' ', 'border_width'))
     
     >>> f( 'blah blah (*)_123b ...' ) 
     ('(*)_123b', ('*', '123b'))
     
     >>> f( 'blah blah ( )_123 ...' )  # 123 is not a valid OpenSCAD identifier

  ''' 

#  Radio data: ( )_width=3;height=3;
IDU_RADIO_DATA= ( RADIO_BOX[0]+r'( |'+RADIO_MARK+r')'+RADIO_BOX[1]+'_('
                  + RE_ASSIGN +')+' 
                )        
                
# check box: [ ]_label or [x]_label               
IDU_CHK_BOX= CHK_BOX[0]+r'( |'+CHK_MARK+r')'+CHK_BOX[1]+'_'+RE_ID_C
def test_IDU_CHK_BOX():
  r'''       
     >>> p = re.compile( IDU_CHK_BOX)
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah [ ]_abc ...' ) 
     ('[ ]_abc', (' ', 'abc'))
    
     >>> f( 'blah blah [x]_border_width ...' ) 
     ('[x]_border_width', ('x', 'border_width'))
    
     >>> f( 'blah blah [x]_123b ...' ) 
     ('[x]_123b', ('x', '123b'))
         
     >>> f( 'blah blah [ ]_123 ...' )  # 123 is not a valid OpenSCAD identifier

  ''' 

IDU_BTN_LABEL_PARTS = (r'\+',r'\-',r'\*',r'\/',r'\>',r'\<')

# IDU_BTN_LABEL1: For one btn label, like '+', '+2.5','>'
IDU_BTN_LABEL1= ( r'(?:'+ '|'.join(IDU_BTN_LABEL_PARTS)
                + r')(?:' +RE_NUM+')?'
                ) 

def test_IDU_BTN_LABEL1():
  r'''       
     >>> p = re.compile( IDU_BTN_LABEL1 )
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah / + ...' ) 
     ('/', ())
          
     >>> f( 'blah -2.3 + ...' ) 
     ('-2.3', ())
     
     >>> f( 'blah >2 + ...' ) 
     ('>2', ())
         
  ''' 

IDU_BTN_LABEL1_C= ( r'('+ '|'.join(IDU_BTN_LABEL_PARTS)
                + r')(' +RE_NUM+')?'
                ) 

#def test_IDU_BTN_LABEL1_C():
#  r'''       
#     >>> p = re.compile( IDU_BTN_LABEL1_C )
#     >>> f = lambda s: ( p.search(s) and 
#     ...                 ( p.search(s).group(), p.search(s).groups() )
#     ...               ) or None  
#     
#     --------------------------------
#     >>> f( 'blah / + ...' ) 
#     ('/', ('/', None))
#          
#     >>> f( 'blah -2.3 + ...' ) 
#     ('-2.3', ('-', '2.3'))
#     
#     >>> f( 'blah >2 + ...' ) 
#     ('>2', ('>', '2'))
#         
#  ''' 
#        
  
## 
                     
#IDU_BTN_LABEL_C = r'('+IDU_BTN_LABEL1+')'                                     

IDU_BTN_LABEL_ALL= ( IDU_BTN_LABEL1
                   + r'(?:.(?:'+ IDU_BTN_LABEL1 
                   + r'))*'
                   )         

def test_IDU_BTN_LABEL_ALL():
  r'''       
     >>> p = re.compile( IDU_BTN_LABEL_ALL )
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah >2 +3 ...' ) 
     ('>2 +3', ())
     
     >>> f( 'blah >2,+3,-,< ...' ) 
     ('>2,+3,-,<', ())
           
  ''' 

IDU_BTN_LABEL_ALL_GROUPS= ( IDU_BTN_LABEL1_C
                   + r'(.'+ IDU_BTN_LABEL1_C 
                   + r')*'
                   )    
#def test_IDU_BTN_LABEL_ALL_GROUPS():
#  r'''       
#     >>> p = re.compile( IDU_BTN_LABEL_ALL_GROUPS )
#     >>> f = lambda s: ( p.search(s) and 
#     ...                 ( p.search(s).group(), p.search(s).groups() )
#     ...               ) or None  
#     
#     --------------------------------
#     >>> f( 'blah >2 +3 ...' ) 
#     ('>2 +3', ('>', '2', '+', '3'))
#     
#     >>> f( 'blah >2,+3,-,< ...' ) 
#     ('>2,+3,-,<', ('>', '2', '+', '3', '-', '', '<', ''))
#           
#  ''' 
                                          
# IDU_BTN_LABEL_C = r'('+IDU_BTN_LABEL1+')'                                     

IDU_BTN= (BTN[0]+ r' *?('+IDU_BTN_LABEL_ALL
         + r') *?'+ BTN[1]
         )             
         # {+}
         # {+ -3 > }
                   
def test_IDU_BTN():
  r'''       
     >>> p = re.compile( IDU_BTN )
     >>> f = lambda s: ( p.search(s) and 
     ...                 ( p.search(s).group(), p.search(s).groups() )
     ...               ) or None  
     
     --------------------------------
     >>> f( 'blah blah {+} ...' ) 
     ('{+}', ('+',))
          
     >>> f( 'blah blah {+ -} ...' ) 
     ('{+ -}', ('+ -',))
     
     >>> f( 'blah blah {*2 / < > +1.5 -0.1} ...' ) 
     ('{*2 / < > +1.5 -0.1}', ('*2 / < > +1.5 -0.1',))
        
        
  ''' 


#RE_LIST= r'\[.*\]'
#RE_NUM_LIST= r'\[' + RE_NUM + 

#IDU_LINE = RE_ID
#RE_VAL_LIST = '(?<=//=)(\[.+\])'
#RE_LIST_BTN_COMBO = '(?<=//=)(\[.+\])<(.*)>'
  # "b= 34; //=[3,4,5]<+|-|5>"  ==> "[3,4,5]", "+|-|5"
  

IDU_TOKENS={

  "IDU_LIST":[ ("["     ,"\["),
             ('ID',        RE_ID),  
           ('NUMBER',    RE_NUM),
           ('STRING',    RE_STR),   
           ("]",         "\]")
           ]
  ,"IDU_CTRL":[
              ("RADIO_BOX", IDU_RADIO_BOX),
              ("CHK_BOX", IDU_CHK_BOX),
              ("BTN", IDU_BTN),
              ('SKIP',    r'([ \t\n]|'+IDU_BEG+r'|'+IDU_END+'|' +RE_ID+'|'+RE_SYM+ r')' ),
             ]
  ,"IDU_BLK": [ ('IDU_BLK',   IDU_BLK),   # IDU_BLK will match \n, too. 
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"ASSIGN": [ ('ASSIGN',   RE_ASSIGN),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"BTN_LABEL": [ ('BTN_LABEL',   RE_BTN_LABEL),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]

  ,"NUM_LIST": [ ('NUM_LIST',   RE_NUM_LIST),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]
  ,"RANGE": [ ('RANGE',   RE_RANGE),  
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]

  ,"NUM": [ ('NUM',   RE_NUM),   # IDU_BLK will match \n, too. 
              ('SKIP',    r'([ \t\n]|'+RE_ID+r'|'+RE_NUM+'|' +RE_STR+'|'+RE_SYM+ r')' ),
              ]

#           ('ID',        RE_ID),  
#           ('NUMBER',    RE_NUM),
#           ('STRING',    RE_STR),
#           ('SYMBOL',    RE_SYM),
#           ('NEWLINE', r'\n'),           #Line endings
#           ('SKIP',    r'[ \t\n]'),        #Skip over spaces and tabs

         
#  ,"IDU": [ ('ID',        RE_ID),  
#           ('IDU_BEG',   IDU_BEG),
#           ('IDU_END',   IDU_END),
#           ('SYMBOL',    RE_SYM),
#           ('SKIP',    r'[ \t\n]'),        #Skip over spaces and tabs
#           #('ELSE',    r'.*')
#         ]
  ,"IDU_PART":[
      ('NUM_LIST', RE_NUM_LIST),
      ('RADIO_DATA', IDU_RADIO_DATA),
      ('RADIO_BOX', IDU_RADIO_BOX),
      ('CHK_BOX', IDU_CHK_BOX),
      ('BTN', IDU_BTN),
      ('SKIP',    r'([ \t\n]|'+IDU_BEG+r'|'+IDU_END+'|' +RE_ID+'|'+RE_SYM+ r')' ),
      ('IDU_BEG', IDU_BEG),
      ('IDU_END', IDU_END),
     # ('ID', RE_ID),
     # ('SYMBOL', RE_SYM),        
      
      ]         
  ,"IDU_LINE": [
      ('SETVAR',    RE_ASSIGN),
      ('ID',        RE_ID),  
      ('IDU_BEG',   IDU_BEG),
      ('IDU_END',   IDU_END),
      ('BTN',       IDU_BTN ),
      ('RADIO_BOX', IDU_RADIO_BOX), 
      ('CHK_BOX',   IDU_CHK_BOX), 
      ('NUM_LIST',  RE_NUM_LIST), # Integer or decimal number
      ('NUMBER',    RE_NUM), # Integer or decimal number
      #('ASCUI_BEG', r'//='),        # begin of ascui control
     # ('BTN', IDU_BTN ), #BTN[0]+r'.*?'+BTN[1]),
#      ('ASSIGN',  r'='),          # Assignment operator
      ('SYMBOL', RE_SYM), #
#      ('END',     r';'),            Statement terminator
#      ('OP',      r'[+*\/\-]'),    # Arithmetic operators
      ('NEWLINE', r'\n'),           #Line endings
      ('SKIP',    r'[ \t\n]'),        #Skip over spaces and tabs
      
    ]  
}  
  
  
def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                

def re_test():
  ''' Test the re and token using doctest
  '''           
  test_docstr( test_IDU_BEG )
  test_docstr( test_IDU_END )
  test_docstr( test_IDU_BLK )
  test_docstr( test_RE_ID ) 
  test_docstr( test_RE_STR )
  test_docstr( test_RE_ASSIGN ) 
  test_docstr( test_RE_BTN_LABEL )
  test_docstr( test_IDU_RADIO_BOX )  
  test_docstr( test_IDU_CHK_BOX ) 
  test_docstr( test_IDU_BTN_LABEL1 )
  #test_docstr( test_IDU_BTN_LABEL1_C )
  test_docstr( test_IDU_BTN_LABEL_ALL )
  #test_docstr( test_IDU_BTN_LABEL_ALL_GROUPS )
  test_docstr( test_IDU_BTN ) 
  test_docstr( test_RE_RANGE )
    
if __name__=='__main__':
    #doctest.REPORT_NDIFF=True
    #doctest.run_docstring_examples(test1, globals()) 
    ##doctest.run_docstring_examples(AscGrid.addpxdata, globals())
    #test_autoscale()  
    #test_docstr( re_test ) 
    re_test()
"""   

http://www.programcreek.com/python/example/53972/re.Scanner
def _tokenize(self):
        '''
        #Lex the input text into tokens and yield them in sequence.
        '''
        # scanner callbacks
        def bool_(scanner, t): return bool_token(t)
        def identifier(scanner, t): return ident_token(t)
        def integer(scanner, t): return int_token(t)
        def eq(scanner, t): return eq_op_token()
        def neq(scanner, t): return neq_op_token()
        def or_(scanner, t): return or_op_token()
        def and_(scanner, t): return and_op_token()
        def lparen(scanner, t): return lparen_token()
        def rparen(scanner, t): return rparen_token()
        def string_(scanner, t): return string_token(t)
        def not_(scanner, t): return not_op_token()

        scanner = re.Scanner([
            (r"true|false", bool_),
            (r"[a-zA-Z_]\w*", identifier),
            (r"[0-9]+", integer),
            (r'("[^"]*")|(\'[^\']*\')', string_),
            (r"==", eq),
            (r"!=", neq),
            (r"\|\|", or_),
            (r"!", not_),
            (r"&&", and_),
            (r"\(", lparen),
            (r"\)", rparen),
            (r"\s+", None), # skip whitespace
            ])
        tokens, remainder = scanner.scan(self.text)
        for t in tokens:
            yield t
        yield end_token()
        
"""        

"""
python re.Scanner examples:

https://docs.python.org/3.2/library/re.html#writing-a-tokenizer
http://www.programcreek.com/python/example/53972/re.Scanner
http://stackoverflow.com/questions/691148/pythonic-way-to-implement-a-tokenizer?lq=1
http://stackoverflow.com/questions/19280898/re-scanner-only-searching-start-of-string
"""