from sw import *
import os, sys, pprint, time
this_dir = os.path.dirname(os.path.abspath(__file__))
#folder_Settings = sw.app_ini_dir()  # => "...\SynWrite\Setting"
sys.path.insert(0, this_dir)
from ascui_define import *
from ascui import *
import openscad_parser as o_p


isToggling = False 
selected = None
idu = InDocUi()

IS_IDU_ON = 1
IS_AUTOSAVE = 0 
#DOUBLE_CLICK_SEC = 2  # msec to recognize a double click
#clicktime = time.clock()  ## init the clock for use in double-click 
prev_sel_rng=(None,None)
    
prev_word = (None,None,None)

def opa_msg_box(s, msgid= MSG_CONFIRM_Q ):
  s = ('Opa>\n'
      +'----------------------------------------------------\n\n'
      + s
      )
  return msg_box( msgid, s )
  
class Command:

  def on_open(self, ed_self): 
    ''' Initiate idu when opening file'''
    global idu
    idu.options['cb_on'] = IS_IDU_ON
    idu.options['cb_autosave'] = IS_AUTOSAVE 
    print( 'idu initiated. idu.options: ', idu.options )
    print( '===> hit ctrl+shift+/ to change them')

  def on_key0(self, ed_self, key, state):	#Called when user presses a key. key is int key code. state is few chars 
    if state=='c':
      print(ed_self.get_sel())          
  
  def on_key(self, ed_self, key, state):	#Called when user presses a key. key is int key code. state is few chars 
    ''' 
      Note: 
      if starting a line with "abc", and hit a "d", then in on_key():
      
      * The arg 'key' will indicate 'd'
      * The x from ed.get_caret_xy() will be 3
      * ed.get_text_line(-1) = "abc"  (without "d")
      
    '''
    ### Uncomment next line to check key/state value:
    # print( 'state= "%s", key=%s'%(state, key) )
    #   ctrl+shift+? => "sc", 191
    #   187 = "="
    
    
    if (key== 187 and  get_prev_chars(ed,2)=='/*'):
      ## When user hits /*=, bring up a list of operator strings
      ##  for user to choose from
      
      MENU_CTRL_LIST = (
          ('=', '')
         ,('=*/', 'Bring up operator menu when clicked')
         ,('={+ -} */', 'Add Buttons')
         ,('={+ - +10 -7 +3 -2 +0.3 -0.2} */', 'Add more Buttons')
         ,('=[1,2,3] {< >} */', 'Add list')
         ,('=[0:2.5:10] {< >} */', 'Add range')
         ,('=[1,2,3] {+ - +10 -7 +3 -2 +0.3 -0.2 } */', 'List+Buttons')
      )    
      ctrl_list = '\n'.join(
                    ('\t'.join(x) for x in MENU_CTRL_LIST)
                    ) 
      m= dlg_menu( MENU_STD
                  , 'Select idu controls'
                  , ctrl_list
                  )        
      if (not str(m)=='None') and m >=0:
        newtxt = MENU_CTRL_LIST[m][0] #+ (m>0 and '*/' or '')
        conout('Inserting text: "%s"'%newtxt)
        ed_self.insert( newtxt )   
                 
      conout( 'm = '+ str(m))  
           
    elif state=='sc' and key== 191: 
      ''' Hit ctrl+shift+? to bring up the idu options. '''
       
      df_vars = ('on','autosave')
      
      ## Add * in front of var name if it is 1 
      ## See doc of dlg_checklist at
      ## http://sourceforge.net/p/synwrite/wiki/py%20functions%20dialog/
      ##
      df_vars_ch = [ (idu.options['cb_'+k] and '*' or '')+k
                     for k in df_vars ] 
                     
      conout('df_vars_ch = ', df_vars_ch )   
                 
      ## cl is [true, false ... ]    
      ##
      cl = dlg_checklist(caption='Set idu default'
           , columns=' \t0' 
           , items= '\n'.join( df_vars_ch) 
           , size_x= 230
           , size_y= 160
           )        
      conout('Current idu.options= ', idu.options)
      
      if cl:     
        for i,k in enumerate(df_vars):
          idu.options['cb_'+k]= cl[i] and 1 or 0 
                          
      conout('cl = ', cl)
      print('New idu.options= ', idu.options)
  
#  def on_func_hint(self, ed_self): 
#    print('Enter on_func_hint()')
#    return 'test1,test2'         
    
  def on_select(self, ed_self):
    pass
#    c,r = ed_self.get_sel() 
#    doc = ed_self.get_text_all()
#    selected = doc[c:r+c]
#    print('on_select: %s,%s, "%s", len(doc)=%s'%(c,r,selected, len(doc)))
  
  def on_double_click(self, ed_self):
    print('double-clicked!!')      
    
  def on_click(self, ed_self, state):

    ed = ed_self
    doc = ed.get_text_all()
    c,r = ed.get_caret_xy()
    i   = ed.get_caret_pos()
             
    ## Selection before click:
    prev_selc,prev_sellen = ed_self.get_sel()
    prev_selected = ed.get_text_all()[prev_selc:prev_selc+prev_sellen]
    print('\non_click, selected: "%s"'%(prev_selected )) #ed.get_sel()) )
    
    ## Word at moust click
    w_bi, w_len, word_at_i = ed.get_word(*ed.get_caret_xy())  # (i, len, word)
    print( 'word at i= "%s" '%( doc[ w_bi:w_bi+w_len ] ))
    isClickOnPrevSel = w_len and (w_bi,w_len)==(prev_selc,prev_sellen)
    print( 'Current word == prev selection ? ', isClickOnPrevSel )
    
    
    if idu.options['cb_on']:
      
      isAutoSave = idu.options['cb_autosave']      
      
      if isClickOnPrevSel:  
         
         toggleBlock( ed, dlg_menu, MENU_STD
                    , w_bi, w_len, word_at_i
                    , isAutoSave= isAutoSave 
                    , file_save_func= file_save
                    )
      
      elif get_ntBool_at_i(ed):
       
        set_bool_onsite(ed, dlg_menu=dlg_menu, menu_id=MENU_STD
                  , isAutoSave= isAutoSave 
                  , file_save_func= file_save
                  , prec=6)
       
      else:     
        #doc = ed.get_text_all() 
        #i = ed.get_caret_pos()
        #c,r = ed.get_caret_xy()
        #print("SCANNER_RULES['NUM'] = ",SCANNER_RULES['NUM'])
        blk_n_part = get_idu_at_i(ed, SCANNER_RULES['NUM'])      
        ntBlk = ntPart = None
        if blk_n_part: 
          ntBlk,ntPart = blk_n_part
          print( 'idu> Clicked!! ntBlk=%s, ntPart=%s'%blk_n_part) # = ', blk_n_part )
         
        if ntBlk and ntBlk.typ == 'NUM': 
              
          set_val_onsite(ed, dlg_menu, menu_id= MENU_STD
                        , ntNum=ntBlk, isAutoSave=isAutoSave
                        , file_save_func = file_save
                        , prec=6)         

        else:
          
          blk_n_part = get_idu_at_i(ed, SCANNER_RULES['IDU_BLK'])
        
          ntBlk = ntPart = None
          if blk_n_part: 
            ntBlk,ntPart = blk_n_part
            conout( 'idu> Clicked!! ntBlk=%s, ntPart=%s'%blk_n_part) #
            
            if ntPart:
               
              if ntPart.typ=='BTN':
                ##
                ## ntPart.txt is something like '+', or '-', '+3', etc
                ##
                ntBtn = get_ntBtn_at_i(ed) 
                if ntBtn:     
                  if ntBtn.txt in '<>': 
                    a = assign_next_val(ed, ntBlk, ntBtn)
                    if a and isAutoSave: 
                       file_save()                 
                  else:    
                    a= assign_btn_val(ed, ntBlk, ntBtn)
                        
                    if a and isAutoSave: 
                       file_save() 
                      
              elif ntPart.typ == 'NUM_LIST': 
                ##
                ## ntPart.txt is something like [2,3,5] or [2:2:10] or [0:5]
                ##
                a= assign_list_val(ed, ntBlk) 
                if a and isAutoSave: 
                  file_save()        
                 
              elif ntPart.typ=='CHK_BOX':
                 toggle_box(ed, callback=None,
                           callback_scope={}, idu=idu)
                 if isAutoSave: 
                    file_save()        
        
    else:
      
      conout('idu> Clicked. idu is OFF. ctrl-click to turn it ON')
      
        
  def on_caret_move(self, ed_self):
    '''
      Use on_caret_move to simulate an on_click event 'cos SynWrite
      doesn't seem to have that. 
      
      Issues:
       
      1) If the event handler causes the caret to move, for example,
        inserting or deleting text (in fact even replacing text with
        ed.replace causes caret move), then this on_caret_move will
        be triggered a second time with just one click. 
        
        To overcome this, use a global isToggling flag
        
      2) Sometimes continuous clicking on the same spot is desired
         For example, in our case we want to toggle a text between
        "[ ]" and "[x]" to see its effect on OpenSCAD code. So users
        could click on the same spots back and forth.
        
        But clicking on the same spot the 2nd time doesn't trigger
        on_caret_move 'cos it is not moving. So we add a statement
        in the end of each click:
        
          ed.set_sel(i+3,0)
          
        to move the caret away from the spot after the click. So
        clicking on the same spot the 2nd time will work.
        
        The set caret function,
        
          ed.set_caret_pos(i+3)
          
        should have been working, but it will select from the spot 
        of clicking to the spot it sets, resulting in a highlighting
        that we don't need.       
          
    '''       
    global isToggling
    #conout( 'on_caret_move()')
  #entered = "Entered: state= %s, key= %s, chr= %s"%(state, key, chr(key) )
  #app_log( LOG_ADD, entered) 
  #if state=="s" and key==57:  # when "(" is entered
#      xy = ed.get_caret_xy()
#      word_data = ed.get_word(xy[0], xy[1] )#=>(offset, len, word)
#      offset = word_data[0]
#      word = word_data[2]
#      #app_log( LOG_ADD, '"%s" at %s'%(word, offset) )
#      curline = ed_self.get_text_line(-1)
    #if curline:
    def conoutCurLine():
      conout( ed.get_text_line(-1) ) 
    conout( 'idu = %s'%idu)   
    #app_log( LOG_ADD, curline)
    if isToggling:
      isToggling = False
    else:
      #i = ed_self.get_caret_pos()
      #toggle_box(ed_self, i)
      toggle_box( ed_self, callback=conoutCurLine )
      #ed_self.set_sel(i+3,0)
      isToggling = True


'''
# Py api categories
  http://sourceforge.net/p/synwrite/wiki/python%20API/      
  
# py api funcs home page
  http://sourceforge.net/p/synwrite/wiki/python%20API%20functions/ 
   
# py func dialog
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20dialog/ 
   
# py func misc
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20misc/ 
   
# Install python plugin:
  http://sourceforge.net/p/synwrite/wiki/Plugins%20manual%20installation/ 
  
# Py event handler:
  http://sourceforge.net/p/synwrite/wiki/py%20event%20names/  
  
# app_log(id,..) : id constant
  http://sourceforge.net/p/synwrite/wiki/py%20log%20id/    
  
# msg_box(id) constant
  http://sourceforge.net/p/synwrite/wiki/py%20msgbox%20id/ 
  
# Py Editor class
  http://sourceforge.net/p/synwrite/wiki/py%20Editor%20class/  
  
# SynWrite addon repos
  http://sourceforge.net/projects/synwrite-addons/files/PyPlugins/  
'''  

#      cklist = dlg_checklist(caption='The Chklist', 
#              columns='c1\t0\nc2\t0\nc3\t0', 
#              items= ('abc0\tThis is abc\tr0c3'
#               +'\ndef1\tThis is def\tr0c3'
#               +'\n?ghi2\tThis is ghi\tr0c3'
#               +'\n?ghi3\tThis is ghi1\tr0c3'
#               +'\n!def4\tThis is def\tr0c3'
#               +'\n!def5\tThis is def2\tr0c3'
#               ), size_x=500, size_y=400)
#                                      
#      conout( 'cklist= %s'%cklist )        
#
#      pass
#      on = (1,0)[ on ]
#      idu.on( on ) 
#      #conout( 'idu> Set idu.on to: %s'%on  )
#      conout( 'idu> ctrl-clicked:\n... idu is turned %s. To turn it %s: ctrl-click'%(
#              ('OFF (All idu controls are inactive)', 'ON')[on]
#              ,('ON','OFF')[on])
#           )
#      #d= ed.get_text_all()
#      #pconout.pconout( get_idu_blocks(d) )  
#        
#      #d = dlg_menu(id, caption, text)
#      m1= dlg_menu(MENU_simple,    'The Caption', ('abc\tThis is abc'
#                                 +'\ndef\tThis is def'
#                                 +'\n-'
#                                 +'\n?ghi\tThis is ghi'
#                                 +'\n?ghi1\tThis is ghi1'
#                                 +'\n!def\tThis is def'
#                                 +'\n!def2\tThis is def2'
#                                 )     
#    conout( 'm1 = ', m1)                         
#        
##      m2= dlg_menu(MENU_STD,    'Caption', ('abc\tThis is abc'
##                                 +'\ndef\tThis is def'
##                                 +'\n-'
##                                 +'\n?ghi\tThis is ghi'
##                                 +'\n?ghi1\tThis is ghi1'
##                                 +'\n!def\tThis is def'
##                                 +'\n!def2\tThis is def2'
##                                 )
##                  ) 
##      conout( 'm2 = ', m2)   
#      
#      m3= dlg_menu(MENU_DOUBLE,    'Caption', ('abc\tThis is abc'
#                                 +'\ndef\tThis is def'
#                                 +'\n-'
#                                 +'\n?ghi\tThis is ghi'
#                                 +'\n?ghi1\tThis is ghi1'
#                                 +'\n!def\tThis is def'
#                                 +'\n!def2\tThis is def2'
#                                 )
#                  ) 
#      conout( 'm3 = ', m3)                         
#      # dlg_snippet(name, alias, lexers, text)	
#      # Shows dialog to edit snippet properties. Returns 4-tuple of string, or None if dialog cancelled.
#      # dlg = dlg_snippet(name, alias, 'OpenSCAD', "test")      
  