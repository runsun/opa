
'''


'''
import re, pprint, doctest, collections

import os,sys
this_dir = os.path.dirname(os.path.abspath(__file__))
#folder_Settings = sw.app_ini_dir()  # => "...\SynWrite\Setting"
sys.path.insert(0, this_dir)
#from ascui_define import *
#from openscad_parser import *
import re_utils as ru
import openscad as opd
import openscad_api_tools as opapi
#import tkinter as tk

tokenize = ru.tokenize 

IS_DEBUG= 0

KEY_SHIFT = 's16' # hit SHIFT only
KEY_CTRL  = 'c17'
KEY_ALT   = 'a18'
KEY_ENTER = '13'
KEY_TAB   = '9'
KEY_S_TAB = 's9'
KEY_C_TAB = 'c17'
KEY_C_V   = 'c86'
KEY_C_Z   = 'c90'
KEY_CAP   = '20'
KEY_EQUAL = '187'  # =
KEY_APOS  = '222'  # '  (apostrophe)
KEY_QUOTE = 's222' # "
KEY_BRACE_L = 's219' # {
KEY_BRACE_R = 's221' # }
KEY_BRACKET_L= '219'  # [
KEY_BRACKET_R= '221'  # ]
KEY_PAREN_L  = 's57'  # (  (parenthesis)
KEY_PAREN_R  = 's48'  # )
KEY_QUESTION= 's191'

INDENT_SIZE = 2 # Used in OpenscadCommandMenu.on_exit to set the indent

def conout(*args):
  global IS_DEBUG
  if IS_DEBUG: 
    args = ' '.join( [ str(x) for x in args] )
    print(args) 

class LastUpdatedOrderedDict(collections.OrderedDict):
    '''Store items in the order the keys were last added
       https://docs.python.org/2/library/collections.html
    '''                      
    def __setitem__(self, key, value):
        if key in self:
            del self[key]
        OrderedDict.__setitem__(self, key, value)
        
class Options(collections.OrderedDict):
    '''  
    
    '''                      
    def __setitem__(self,k,v): 
      words=('cb_','rb_','bt_')
      if not k[:3] in words:
        raise KeyError(
              ('Option name in InDocUi.options must starts with '+
              'one of %s.'%( ','.join('"%s"'%x for x in words)) + 
              ' You gave: "%s"'%k)
           )
      else:
        #self.__dict__[k]=v  
        collections.OrderedDict.__setitem__(self, k,v)
           
    def __getitem__(self,k): 
      words=('cb_','rb_','bt_')
      if not k[:3] in words:
        raise KeyError(
              ('Option name in InDocUi.options must starts with '+
              'one of %s.'%( ','.join('"%s"'%x for x in words)) + 
              ' You gave: "%s"'%k)
           )
      else:
        return collections.OrderedDict.__getitem__(self, k)                          
    
    def __repr__(self):
      '''
        Return something like:
        'cb_on=1, cb_autosave=1, cb_uturn=0'
      '''  
      
      s = ', '.join(  '%s=%s'%(x,y) for x,y in self.items() )
      
    #      s =(collections.OrderedDict.__repr__(self)
    #          .replace("Options([('",'')
    #          .replace("', ", ",")
    #          .replace("', ", ",")
    #          .replace(')])','')
    #         ) 
      return s    
                                    
      
class InDocUi(object):
  def __init__(self, idu_str='', ed=None, **kargs):
    #d= self.__dict__
    #setattr(d, 'options', collections.OrderedDict() )
    self.options = Options() #collections.OrderedDict() 
    self.data = collections.OrderedDict() 
    #d.data= LastUpdatedOrderedDict() 
  def on(self, val=1):
      self.options['cb_on']= val
      
    
  #def load(self, idu_str):
        

    
def _toggle_box(ed, callback=None, callback_scope={}, idu=None): 
  '''  
    To be called by on_click() to check if the caret falls within
    a checkbox or radiobox. If yes, toggle the checkmark or radiomark
    
  '''             
  #conout( 'Entered toggle_box(), idu=', idu )
  doc = ed.get_text_all() 
  
  if doc :
    i= ed.get_caret_pos() -1     
    #nC,nL =  ed.get_caret_xy()   
    #nC= nC-1
       # The offset index obtained by get_caret_pos() points
      # to the char after the caret, so to obtain the one
      # before the caret, needs to -1 
    #conout ('doc[i]="%s"'%doc[i])              
    def toggle(i, chkmark):
      #conout ('Entered toggle()')
      #txt = doc[i] 
      #conout ('\ni = %s, doc[i] = txt= "%s"'%(i,txt))  
      if i>=0:
        newval = doc[i]==' ' and chkmark[-1] or ' '
        ed.replace( i,1, newval)
          # If chkmark is a RADIO_MARK which is set to r'\*', we need
          # to get the last char, so chkmark[-1]  
        if chkmark == CHK_MARK:   
          #nC, nL = ed.get_caret_xy()
          #varname = ed.get_text_line(-1)[ nC+2: ].split(' ')[0]
          varname = doc[ i+3: ].split(' ')[0] 
          conout( 'varname="%s", newval = "%s"'%(varname,newval) )
          idu.options['cb_'+varname] = (newval!=' ' and 1) or 0  
        if callable(callback):
          call(callback, callback_scope)
      ed.set_sel(i+3,0)
                   
    case1= ('\\'+doc[i], '\\'+doc[i+2]) # ( _)
    case2= ('\\'+doc[i-1],'\\'+doc[i+1]) # (_ )
    #conout( 'case1,2 = "%s", "%s"'%(case1, case2))
    #conout( 'doc[i-1:i+3] = "%s"'%doc[i-1:i+3] )
    if case1==CHK_BOX: toggle(i+1,CHK_MARK)          
    elif case2==CHK_BOX: toggle(i,CHK_MARK)          
    elif case1==RADIO_BOX: toggle(i+1,RADIO_MARK)          
    elif case2==RADIO_BOX: toggle(i,RADIO_MARK)          
      

  



import collections

    
                
       
def _get_idu_at_i(ed, rule): #idu_token='IDU_BLK'):
    '''return (ntBlk_at_i, ntPart_at_i) '''
    
    doc = ed.get_text_all()
    i = ed.get_caret_pos()  
    #conout ( 'i = ', i )
    idu_blks= tokenize_to_rng_dict(doc, rule) #idu_token) #'IDU_BLK') 
    if idu_blks:
      rngs = idu_blks.keys()
      ntBlk_at_i= [ idu_blks[rng] for rng in rngs
                   if rng[0]<=i and i<rng[1]] 
      
      if ntBlk_at_i:
           
        ntBlk_at_i = ntBlk_at_i and ntBlk_at_i[0] or None                                   
        #rng = ntBlk_at_i.rng
        #conout('rng = ', rng, ': "%s"'%doc[ rng[0]:rng[1]])
        #conout( 'idu> Clicked inside an idu_blk:\n... ', ntBlk_at_i )
            # , '\n... doc[%s]'%(':'.join(rng)), doc[ rng[0]:rng[1]] )
        i = i-ntBlk_at_i.rng[0]  
        #conout('i= ',i)  
        ntParts = tokenize_to_rng_dict( ntBlk_at_i.txt, SCANNER_RULES['IDU_PART'] )
            # ntParts=
            # {'NUM_LIST': 
            #   [IDU_PART(rng=(5, 12), iL=1, cB=5, iLE=1, cE=12, txt='[4,5,6]')], 
            #  'BTN': 
            #   [IDU_PART(rng=(13, 18), iL=1, cB=13, iLE=1, cE=18, txt='{+|-}')]}
        #conout( 'ntParts = ', ntParts )
        
        ntPart_at_i = [ ntParts[rng] for rng in ntParts.keys() 
                      if rng[0]<=i and i<rng[1]
                   ]
        ntPart_at_i = ntPart_at_i and ntPart_at_i[0] or None                                   
                   
        #conout( 'ntPart_at_i: ', ntPart_at_i)  
        
        return (ntBlk_at_i, ntPart_at_i) 
      else:
        return None   

                 
  
def _get_ntBtn_at_i(ed, i=-1):
  '''
    Return Token(typ='BTN_LABEL', rng=(18, 20), 
             rel_rng=(0, 18, 0, 20), txt='-3')
  ''' 
  conout('Entered get_ntBtn_at_i()')
  doc = ed.get_text_all()
  c,r = ed.get_caret_xy() 
    
  i = i==-1 and c-1 or i
                                 
  line = ed.get_text_line(-1)
  ntLbls = list( tokenize( line, "BTN_LABEL") )
  conout( 'In get_ntBtn_at_i(), init ntLbls = ', ntLbls ) 
  conout( 'In get_ntBtn_at_i(), i = ', i ) 
  #ch1, ch2 = doc[i-1:i+1]
  ntLbl = None
  if ntLbls:
    if i>=0:
      for lbl in ntLbls:
        if lbl.rng[0]<=i and i< lbl.rng[1]:
          ntLbl = lbl
          break      
    conout( 'In get_ntBtn_at_i(), returning ntLbl = ', ntLbl ) 
    return ntLbl
  
def _get_ntBlk_at_i( ntBlks, i ):   
  '''      
     ntBlks: something like:
        [IDU_BLK(rng=(0, 11), iL=1, cB=0, iLE=1, cE=11, txt='/*= [2,3]*/'), 
         IDU_BLK(rng=(22, 39), iL=1, cB=22, iLE=1, cE=39, txt='/*.[ ]_autosave*/'), 
         IDU_BLK(rng=(65, 110), iL=3, cB=16, iLE=3, cE=61, 
              txt='/*^[ 3, -4, 5.5 ]{+|-|>}[ ]_autosave [x]_on*/')] 
              
    i is the absolute index counting from the beg of doc.          
              
  '''     
  ntBlk_at_i =  [blk for blk in ntBlks
               if blk.rng[0]<=i and i<blk.rng[1]]
  return ntBlk_at_i and ntBlk_at_i[0] or None
        
def _get_ntNum_b4_blk(line, blk): #, re_ptn=RE_NUM):
  '''          
    Return the namedtuple for any valid number that is 
    right b4 position blk on line. If not a number, return None.
                                                   
    line: string
    i : column index
    
               0123456789012345678901234 
    >>> line= 'x=5.67; /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk = list( tokenize( line, "IDU_BLK"))[-1]
    >>> ntBlk
    Token(typ='IDU_BLK', rng=(8, 32), rel_rng=(0, 8, 0, 32), txt='/*=[3.5, 4, 10] {+ -} */')
    >>> ntNum = get_ntNum_b4_blk( line, ntBlk )
    >>> ntNum    
    Token(typ='NUM', rng=(2, 6), rel_rng=(0, 2, 0, 6), txt='5.67')
    
    >>> line2= 'cube( [ 5.2 /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk2 = list( tokenize( line2, "IDU_BLK"))[-1]
    >>> ntNum2 = get_ntNum_b4_blk( line2, ntBlk2 )
    >>> ntNum2    
    Token(typ='NUM', rng=(8, 11), rel_rng=(0, 8, 0, 11), txt='5.2')
   
    >>> line3= 'cube( [ 5.2, /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk3 = list( tokenize( line3, "IDU_BLK"))[-1]
    >>> ntNum3 = get_ntNum_b4_blk( line3, ntBlk3 )
    >>> ntNum3    
    Token(typ='NUM', rng=(8, 11), rel_rng=(0, 8, 0, 11), txt='5.2')

    >>> line4= 'cube( [  /*=[3.5, 4, 10] {+ -} */'
    >>> ntBlk4 = list( tokenize( line4, "IDU_BLK"))[-1]
    >>> ntNum4 = get_ntNum_b4_blk( line4, ntBlk4 )
    >>> ntNum4    
            
  '''
  left= line[ :blk.rel_rng[1] ]  
  
  left2 = list( tokenize(left, token_name='NUM') )
  ntNum = left2 and left2[-1] or None
  return ntNum       
 
def _get_num_at_i(line, i): #, re_ptn=RE_NUM):
  '''          
    Return the number that is matched at position i
    of line. If not a number, return None.
                                                   
    line: string
    i : column index
    
    Usage: in assign_list_val()
    
               0123456789012345678901234 
    >>> line= '/*=[3.5, 4, 10] {+ -} */'
    >>> get_num_at_i( line, 1) 
    >>> get_num_at_i( line, 5)
    3.5
    >>> get_num_at_i( line, 6)
    3.5
    >>> get_num_at_i( line, 10)    
    4
    >>> get_num_at_i( line, 13)   
    10
    
  '''
  left, right = line[:i], line[i:]
  left2 = re.compile(r'(,|\[| |\b)').split(left)[-1]
  right2= re.compile(r'\b').split(right)[0]
  matched = re.compile( ru.RE_NUM ).match( left2+right2)
  n = matched and matched.group() or None
  if n: 
    if len(n.split('.'))>1: n = float(n)
    else: n=int(n)
    
  return n   
  
#line= '/*=[3.5, 4, 10] {+ -} */'
#print('_get_num_at_i= ' , _get_num_at_i( line, 1))
  
def _get_prev_chars(ed, n=3):
  '''Get chars right before the caret.
  '''
  i = ed.get_caret_pos()
  if i<n: n=i
  return ed.get_text_all()[i-n:i]
 
def _is_in_word(ed, word):
  ''' chk if caret is within word boundary.

    Let word = "synw" and 'I' represents the caret, 'I' is 
    in its boundary if it is : Isynw, sIynw, syInw and synIW,
    but NOT synwI.
    
    Return None or a namedtuple:
      (typ='WORD', rng=(abs_x,abs_y), 
       rel_rng=( line_beg, c_beg, line_end, c_end)
       txt= 'synw' 
      )
         
 '''
  if word: 
    doc = ed.get_text_all()
    line = ed.get_text_line(-1)
    pos = ed.get_caret_pos()
    c,r = ed.get_caret_xy()
    
    wl= len(word)
    for i in range(wl):
      wd = doc[ pos-i: pos-i+wl]
      if wd == word:  
    #        ntWord= collections.namedtuple('WORD',
    #                  ['typ','rng','rel_rng','txt'])( 
    #                    'WORD', (pos-i,pos-i+wl), 
    #                    (r,c-i,r,c-i+wl), word
    #                    ) 
        ntWord = ntBlk( 'WORD', typ='WORD', rng= (pos-i,pos-i+wl), 
                        rel_rng= (r,c-i,r,c-i+wl), txt= word )
        return ntWord          
 
def _is_in_words(ed, words):
  for w in words:
    ntWord = is_in_word(ed, w)
    if ntWord: return ntWord
                    
        
def _get_ntBool_at_i(ed):

  ntBool = is_in_word(ed, 'true')
  ntBool = ntBool and ntBool or is_in_word(ed, 'false') 
  #print( 'in get_ntBool_at_i(), ntBool = ', ntBool)
  return ntBool               
   
               
def _assign_btn_val(ed, ntBlk, ntBtn, prec=6):
  '''
     Assign new value to the value assignment code
     (like width=3;) right in before the given ntBlk,
     which is a namedtuple representing an idu ntBlk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
     btn_label_ch is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  ''' 
  conout('Enter assign_btn_val()')
  line= ed.get_text_line(-1)
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  
  if ntOldNum and ntBtn:

    num_ibeg, num_iend = ntOldNum.rng 
    
    newtxt= { 1: ntBtn.txt
            , ntBtn.txt=="+": "+1"
            , ntBtn.txt=="-": "-1"
            , ntBtn.txt=="*": "*2"
            , ntBtn.txt=="/": "/2" 
            }[1]
       
    newnum = eval( ntOldNum.txt + newtxt) 
    #print('newnum = ', newnum, ', type = ', type(newnum),
    #      ', ', round(newnum,prec))
    if type(newnum)==float: newnum=round(newnum, prec) 
    
    line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
    num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
    num_iend_abs = num_iend-num_ibeg 
    
    ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
   
    return 1    
    
  else: conout ('assign not found')  
 
             
  
IS_DEBUG=0

def _set_bool_onsite(ed, dlg_menu, menu_id
                  , isAutoSave 
                  , file_save_func
                  , prec=6):
  '''      
    Called by on_click when user clicks at or right before
    a number and replace the num with a new one by calling up a 
    menu and selecting an operation (like "+", "-3", etc) 
    which will be applied the old number. 
    
    ntNum is a namedtuple representing a num
     like Token(typ='NUM', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='234.567891')

     prec is to set the decimal points after the "." of
     the new num
  ''' 
  conout('\n\nEnter set_bool_onsite()')   
  file_save = file_save_func
  ## When click inside /*=*/, bring up a menu with operations
  ##  to be executed upon the num before /*=*/. This menu will
  ##  stay on if an operation is selected. Will quit if click
  ##  on [done] or somewhere outside the menu or hit [Esc] 
  ##

  
  #ops = operators + uis 
  
  im=1        # index of menu item                
  #tSel = None # selected text 
  #oldnum = ntBool #eval(ntNum.txt) 
  
  keepRunning = 1
              
  ## Make the menu dlg keep popping up until user hits [Esc]
  ##  or select [done] or click off-menu, all return None
  ## 
  ## Note that the cursor has to be re-set according to
  ##  the len of new val (like 3.3456, comparing to 3,  
  ##  will push the caret to the far right and out of /*=*/
  ##  This is done in assign_menu_val() 
  ##
  while keepRunning : #m!=None and tSel!=ops[-1]:
    conout('\nStarting while loop' )
       
      
    ## Re-scan to get new blk 'cos the previous
    ##  run might change caret position   
    ntBool = get_ntBool_at_i(ed)
    ed.set_sel( ntBool.rng[0], len(ntBool.txt), nomove=False)
    
    newtext = (ntBool.txt=='true' and 'false' or 'true')
    
    menu_text= newtext + '\n-\n[done]'
             
    im= dlg_menu( menu_id, caption='Toggle Boolean'
                , text= menu_text )          
              
    ed.set_caret_pos( ntBool.rng[0] )
    
    if im==0:
      ed.replace( ntBool.rng[0]
                , len(ntBool.txt)
                , newtext
                ) 
      if isAutoSave: file_save()  
              
    else:
      keepRunning = 0    
      

      
           
def _set_val_onsite(ed, dlg_menu, menu_id
                  , ntNum
                  , isAutoSave 
                  , file_save_func
                  , prec=6):
  '''      
    Called by on_click when user clicks at or right before
    a number and replace the num with a new one by calling up a 
    menu and selecting an operation (like "+", "-3", etc) 
    which will be applied the old number. 
    
    ntNum is a namedtuple representing a num
     like Token(typ='NUM', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='234.567891')

     prec is to set the decimal points after the "." of
     the new num
  ''' 
  conout('\n\nEnter set_val_onsite()')   
  file_save = file_save_func
  ## When click inside /*=*/, bring up a menu with operations
  ##  to be executed upon the num before /*=*/. This menu will
  ##  stay on if an operation is selected. Will quit if click
  ##  on [done] or somewhere outside the menu or hit [Esc] 
  ##
  operators= [ ' +1',r' -1','+0.3','-0.2','+10','-7'
       , r' *2',' /2']
  undo_section= ['-','[original]','[undo]']     
  undos = [ ]
  uis= ['-', '[done]']
  
  #ops = operators + uis 
  
  im=1        # index of menu item                
  tSel = None # selected text 
  oldnum = eval(ntNum.txt)
  keepRunning = 1
              
  ## Make the menu dlg keep popping up until user hits [Esc]
  ##  or select [done] or click off-menu, all return None
  ## 
  ## Note that the cursor has to be re-set according to
  ##  the len of new val (like 3.3456, comparing to 3,  
  ##  will push the caret to the far right and out of /*=*/
  ##  This is done in assign_menu_val() 
  ##
  while keepRunning : #m!=None and tSel!=ops[-1]:
    conout('\nStarting while, tSel ="%s", undos=%s'%(tSel,undos) )
       
      
    ## Re-scan to get new blk and part 'cos the previous
    ##  run might change caret position       
    ## blk_n_part = get_idu_at_i(ed, 'NUM') 
    #ed.set_caret_pos( ntNum.rng[0] )
    ntNum = get_idu_at_i(ed, SCANNER_RULES['NUM'])[0]
    ed.set_sel( ntNum.rng[0], len(ntNum.txt), nomove=False)
                                                                   
    undo_list= ([ '*old:'+','.join([str(x) for x in undos])]
               + ['[Save old as ui]'])
                        
    ops = ( operators + undo_section 
          + ( undos and undo_list or []) 
          + uis ) 
    im= dlg_menu( menu_id, caption='Select operation'
              , text= '\n'.join(ops) )
    ed.set_caret_pos( ntNum.rng[0] )
      
    tSel = type(im)==int and ops[im].strip() or None
    conout('tSel = ', tSel)
                
    if tSel == '[original]':   ## Revert to the old num
      ed.replace( ntNum.rng[0]
                , len(ntNum.txt)
                , str(oldnum)
                ) 
      if isAutoSave: file_save()  
              
    elif tSel=='[undo]':   ## Revert to the prev changed num
      if undos:
        conout('Asking for undo, undos = %s'%(undos))
        ed.replace( ntNum.rng[0]
                  , len(ntNum.txt)
                  , str(undos.pop())
                  )              
      if isAutoSave: file_save()
    
    elif tSel=='[Save old as ui]':    
    
      ed.set_caret_pos(  ntNum.rng[1] )
      ed.insert('/*=['+ ops[im-1].replace('*old:','') +']{< >}*/' ) 
      ed.set_caret_pos(  ntNum.rng[0] )
             
                            
    elif im==None or tSel == '[done]':
      keepRunning = 0 
      
    else:
       
      newnum = eval( ntNum.txt + tSel) 
      conout('newnum = ', newnum, ', type = ', type(newnum),
          ', ', round(newnum,prec))
      if type(newnum)==float: newnum=round(newnum, prec) 
      
      ed.replace( ntNum.rng[0]
                , len(ntNum.txt)
                , str(newnum)
                )    
      #ed.set_caret_pos( ntNum.rng[0] ) 
      if isAutoSave: file_save() 
      undos.append( eval(ntNum.txt) )
      keepRunning= 1     
      #conout( 'a = ', a )

      
def _assign_menu_val(ed, ntBlk, sMenuItem, prec=6):
  '''    
    NOTE: this needs to retire ...
      
    Called by on_click when user clicks at "/**/" in the 
    middle of "**"
    
     Assign new value to the number right in before the 
     given ntBlk by calling up a menu and selecting an 
     operation (like "+", "-3", etc) which will be executed
     upon the old number. 
     ntBlk is a namedtuple representing an idu ntBlk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
       
     sMenuItem is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  ''' 
  conout('Enter assign_menu_val()')
  i = ed.get_caret_pos()
  c,r = ed.get_caret_xy()
  line =  ed.get_text_line(-1)[:c]
  conout( 'c=%s, r=%s, line="%s"'%(c,r,line) )
  
  #| Create a ntBlk with an attribute 'rel_rng' that can be 
  #| used by get_ntNum_b4_blk(). 
  #ntBlk = collections.namedtuple('BLK', ['rel_rng'])( (r,c) )
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  conout( 'ntOldNum = ', ntOldNum ) 
  
  if ntOldNum:

    num_ibeg, num_iend = ntOldNum.rng 
    
    newtxt= { 1: sMenuItem
            , sMenuItem=="+": "+1"
            , sMenuItem=="-": "-1"
            , sMenuItem=="*": "*2"
            , sMenuItem=="/": "/2" 
            }[1]
       
    newnum = eval( ntOldNum.txt + newtxt) 
    
    conout('newnum = ', newnum, ', type = ', type(newnum),
          ', ', round(newnum,prec))
    if type(newnum)==float: newnum=round(newnum, prec) 
    
    line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
    num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
    num_iend_abs = num_iend-num_ibeg 
    
    ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))

    ## The cursor has to be re-set according to the
    ##  len of new val (like 3.3456, comparing to 3,  
    ##  will push the caret to the far right and out of /*=*/ 
    ##    
    len_oldNum = len(ntOldNum.txt)
    len_newNum = len(str(newnum))
    newi = i+ len_newNum- len_oldNum
    #ed.set_sel(start=i-c, len=1, nomove=True)  
    ed.set_caret_pos( newi ) 
    return 1    
    
  else: conout ('assign not found')  
     
def _assign_next_val(ed, ntBlk, ntBtn, prec=12):
  '''    
    When btn is either ">" or "<"
    
    This requires either a list or a range:
    
      x=3; /*=[3,4,5] {>} */
      x=3; /*=[0:3:15] {>} */
      
    
  ''' 
  conout('Enter assign_next_val()')
  line= ed.get_text_line(-1)
  
  ntOldNum = get_ntNum_b4_blk( line, ntBlk )
  
  if ntOldNum and ntBtn:
                            
    # Chk if the ntBlk contains either range ( [i:j] or [i:j:k] )
    # or nun list ( [2,4,6,10] )
    #
    ntRngs = list(tokenize( ntBlk.txt, "RANGE" ))
    conout('ntRngs = ', ntRngs)
    rng=None 
      
    if ntRngs:
      ntRng = ntRngs[0]
      ns = [eval(x) for x in ntRng.txt[1:-1].split(':')] #[i,j,k], [i,j]
      if len(ns)==2: ns= [ns[0],1,ns[1]]
      beg,itv,end = ns
      #rng = [ x for x in range(beg,end+1,itv) ] 
      count = int((end-beg)/itv)  
      conout( 'beg=%s, itv=%s, end=%s, count=%s'%(beg,itv,end,count))
      rng=[ round(i*itv+beg, prec) for i in range(count+1)]
      
    else:
      nt_nlists = list( tokenize( ntBlk.txt, "NUM_LIST") )
      conout('nt_nlists = ', nt_nlists) 
      if nt_nlists:  
        nt_nlist = nt_nlists[0]
        rng = eval( nt_nlist.txt)
        
    if rng:        
      
      oldNum = float(ntOldNum.txt)
      rng.sort() 
                  
      def get_next_i(rng, val, direction='>'): 
        '''
            Return next index, depending on direction.
            If i out of bound, it cycles from the other end.
        '''
        i=0
        if direction=='>':
          if val>= rng[-1]: i= 0
          else:
            rng_tmp =  rng + [ rng[-1]+ 
                              ((len(rng)>1 and (rng[-1]-rng[-2])) or 1)
                             ]
            i=0
            for i,v in enumerate(rng_tmp):
              if val < v: break 
          return i
          
        else:
          if val<= rng[0]:
            i=len(rng)-1
          else:
            rng_tmp = [ rng[0] -
                        ((len(rng)>1 and (rng[0]-rng[1])) or 1)
                      ] +rng
            iis = [x for x in range(len(rng))]
            iis.reverse()
            for i in iis:
              if val> rng[i]: break  
          return i
      
      nexti = get_next_i(rng, oldNum, direction=ntBtn.txt)
      
      if nexti>=0 and nexti<len(rng):  
           
        conout( 'rng = %s, nexti = %s'%(rng,nexti))      
        newnum = rng[ nexti ]
        conout( 'rng = %s, nexti = %s, newnum = %s'%(rng,nexti,newnum))      
        
        if type(newnum)==float: newnum=round(newnum, prec) 
        num_ibeg, num_iend = ntOldNum.rng 
        
        line_ibeg_abs = ntBlk.rng[0]-ntBlk.rel_rng[1]
        num_ibeg_abs = line_ibeg_abs + ntOldNum.rng[0]   
        num_iend_abs = num_iend-num_ibeg 
        
        ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
       
        return 1    
    
  else: conout ('num to-be-replaced not found')  
       
      
def _assign_list_val(ed, blk, prec=6):
  '''
     Assign new value to the value assignment code
     (like width=3;) right in before the given blk,
     which is a namedtuple representing an idu blk
     like Token(typ='IDU_BLK', rng=(404, 414), 
       rel_rng=(18, 9, 18, 19), txt='/*={+|-}*/')
     btn_label_ch is something like "+", "-", etc. 
     prec is to set the decimal points after the "."
  '''   
  conout('Entered: assign_list_val()')
  line= ed.get_text_line(-1)
  c,r = ed.get_caret_xy()
  newnum = get_num_at_i(line,c)   
  conout('newnum = ', newnum)
  if not newnum==None:
    ntOldNum = get_ntNum_b4_blk( line, blk )
    conout('ntOldNum = ', ntOldNum)  
    
    if ntOldNum:

      num_ibeg, num_iend = ntOldNum.rng 
      line_ibeg_abs = blk.rng[0]-blk.rel_rng[1]
      num_ibeg_abs = line_ibeg_abs + num_ibeg 
      num_iend_abs = num_iend-num_ibeg 
      if type(newnum)==float: round( newnum, prec )
      ed.replace( num_ibeg_abs, num_iend_abs, str(newnum))
   
      return 1 
    else: conout ("Can't find a number to be modified.")
  else: conout ("Can't find the number at i from the list")  

  
  
def toggleBlock(ed, dlg_menu, menu_id, w_bi, w_len, word_at_i
                ):
    '''   
      Starting counting from index w_bi, find text to toggle between 
      the two below:
      
          resize(...){
                ...
                ...
          }
          
      and  

          /*resize(...){*/
                ...
                ...
          /*}*/
           
      To be called by on_click when clicking at a previously
      selected word
    
    '''  
    if not word_at_i: return  
    MENU_MAX_LOOP = 50
    #print('\nEnter toggleBlock')            
    doc = ed.get_text_all()

    pdoc = doc[w_bi:] #.replace('\n','')  
    #ntBlk = o_p.findblock(pdoc)
    ntBlk = ru.findAllBlks( pdoc
            , name='{}', ends=("\{","\}"))
    if ntBlk:
      ntBlk=ntBlk[0]
      ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
      header = pdoc[:ibeg+1]  ## = "resize(...) {"
    #ntBlks = o_p.scanBlocks(pdoc)
    #print('ntBlk = ', ntBlk )
    #if ntBlk:   
      #print('blks = ', blks )
      #blk = blks[0]
      #ibeg,iend,txt = blks[0] 
      #ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
      #print( 'ibeg,iend= ', ibeg,iend )                                         
      
      #header = pdoc[:ibeg+1]  ## = "resize(...) {"
      #print(' 3 elements: "%s", "%s", "%s"'%(doc[w_bi-2:w_bi], pdoc[ibeg+1:ibeg+3], pdoc[ iend-3:iend+2] ))
        
      ismenu=1
      counter = 0 
      while ismenu==1 and counter< MENU_MAX_LOOP:
        
        #print('*** enter while ***')       

        im= dlg_menu( menu_id, caption='Toggle block'
                    , text= 'toggle "%s" block\n-\n[done]'%word_at_i )  
        #print('menu select index = ', im)            
        if im!=0:
          ed.set_sel( w_bi-1, 0, nomove=True ) 
          ed.set_caret_pos( w_bi )
          #ed.set_sel( w_bi, len(word_at_i), nomove=True )
   
          ismenu=0
        else:              
          if (doc[w_bi-2:w_bi]=='/*' and pdoc[ibeg+1:ibeg+3]=='*/'
             and pdoc[ iend-3:iend+2]=='/*}*/' ):

            ed.replace( w_bi+iend-1-2, 5, '}')
            # Turn "resize(...){" into "/*resize(...){*/" 
            ed.replace( w_bi-2, ibeg+5, header ) 
            w_bi = w_bi-2           
             
          else:
                
            ed.replace( w_bi+iend-1, 1, '/*}*/')        
            # Turn "resize(...){" into "/*resize(...){*/" 
            ed.replace( w_bi, ibeg+1, '/*%s*/'%header )
            w_bi = w_bi+2
          ed.set_sel( w_bi, len(word_at_i), nomove=True )

        if counter>=MENU_MAX_LOOP:
          
          raise OverflowError(
            'Number of menu calls in toggleBlock() exceeds MENU_MAX_LOOP(%s)'%counter)
        if ismenu:
          doc = ed.get_text_all()
          pdoc = doc[w_bi:]   
          #print(' in while bottom, pdoc="%s", len=%s'%(pdoc,len(pdoc)))     
          #ntBlk = o_p.findblock(pdoc) 
          ntBlk = ru.findAllBlks( pdoc, name='{}', ends=( "\{","\}"))[0]
          
          #print(' in while bottom, ntBlk="%s"'%str(ntBlk))     
          ibeg,iend = ntBlk.rng  # Index INSIDE pdoc
          #print( 'ibeg,iend= ', ibeg,iend )                                         
          header = pdoc[:ibeg+1]  ## = "resize(...) {"
                

def opa_test():
  ''' Test the re and token using doctest
  ''' 
  def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                
  
  funcs = ( 
            InDocUi  
          #, tokenize   
          #, tokenize_to_rng_dict 
          #, tokenize_to_typ_dict 
          #, get_idu_blocks 
          #, parse_idu_content 
          #, get_new_num 
          #, load_idu 
          , _get_num_at_i 
          , _get_ntNum_b4_blk 

          )
  def dumb():pass
  for f in funcs: 
    if type(f)== type(dumb):
      test_docstr( f ) 
    else:
      print('====== %s ======'%f)
      
if __name__=='__main__':
    opa_test()
    