#. Intro
#
# sw_menu.py
#
# A menu plugin for SynWrite.
#
# Written in Python 2.7 by
# Runsun Pan ( runsun at gmail dot com )
# 2016.2
#
# --- See Usage or Menu_test for examples
# --- Run this file for doctest
#. Usage:
'''

I.    menu = Xmenu() #sw_menu.Menu(dlg_menu)
      menu.pages[0].ctrl_sec.items[0].isShown = 1
      menu.add_page('Page2')
      menu.pages[0].add_radiobox(name='color'
                         , item_names=('blue','green'))        
      menu.add_radiobox( name='align'
                         , item_names=('left','center','right')
                         , page_id= 1)        
      menu.add_checkbox(name='default', item_names=('on','autosave')) 
      
      menu.show()
      
      print( menu.get_data() )
      ==> {'align': 'center', 'color': 'green', 
           'default': {'autosave': 0, 'on': 1}}

      print( menu.get_data_by_page() )
      ==> {'Page2': {'align': 'center'}, 
           'P0': {'color': 'green', 'default': {'autosave': 0, 'on': 1}}}

  
II. Event Handler (simple):

    def on_menu_click(menu):
        blah blah
    
    menu = sw_menu.Menu( dlg_menu )  
    ( blah blah ...) 
    menu.on_sel = lambda: on_menu_click(menu)
    
  
III. Event Handler (complex):
  
    If you want to encaptulate the event handler (don't want it to
    be on the global scope):
    
    ### Subclass Menu

    class Xmenu(sw_menu.Menu):
      def __init__(menu):
        sw_menu.Menu.__init__(menu, dlg_menu)
        
      def on_sel(menu):
        
        print('Xmenu clicked! selected= %s/%s/%s'%( 
              menu.page and menu.page.name or None 
              , menu.sec and menu.sec.name or None
              , menu.item and menu.item.name or None))
              
        print( 'menu.sel_item.keepAlive = %s'%menu.item.keepAlive)       
        
      def on_exit(menu):
          print('menu.get_data():\n%s'%menu.get_data())
          print('menu.get_data_by_page():\n%s'%menu.get_data_by_page())
   

    ### Use it in class Command on_sel:
    
    class Command:

      def on_sel(self, ed_self, state):
      
        menu = Xmenu() #sw_menu.Menu(dlg_menu)
        (blah blah ...)
        menu.show()
   
More Example:

class OpenscadCommandMenu(sw_menu.Menu):

  def __init__(self):
    sw_menu.Menu.__init__(self, dlg_menu)
    self.post_init()
    
  def post_init(self):  
    self.pages[0].name = 'Object'
    self.add_radiobox('obj', ops.BUILTIN_OBJ_W_HOTKEY, isShowHeader=0)    
    self.add_page('Action').add_radiobox('action', ops.BUILTIN_ACTION_W_HOTKEY).isShowHeader=0
    self.add_page('Function').add_radiobox('func', ops.BUILTIN_CORE_W_HOTKEY).isShowHeader=0
  
  def on_exit(self):
    print( self.get_data() )
    print( 'Selected: %s'%self.get_data()['obj'])   
 
class Command:

  def on_sel(self, ed_self, state):
  
    menu = OpenscadCommandMenu()
    menu.show()                
'''
            
 
#. Init
import doctest

MENU_STD = 2 ## needed to show standard menu for sw.dlg_menu
EXIT_LABEL = 'Done' 
    
def get_args(_locals, newdata=None, excludes=['self']):
  ''' 
      Re-populate newdata (a dict) to locals so we can 
      use them directly (like x=a but not x=newdata['a'])
      
      To be called with locals() as the FIRST statement 
      inside a function to get args:
      
      def f( ...):
        args = get_args( locals(),{'parent':self} )
        item = MenuItem( **args )
      
      def add_radiobox(self, sec_name, ... **kargs):
  
        d = get_args( locals(), kargs)
        page = page_id==None and self.page or self.get_page(page_id)
        del d['page_id']
        del d['kargs']
        page.add_radiobox( **d )
        return self 
      
      Or in class __init__:
      
        args= get_args( locals() ) 
        for k,v in args.items(): setattr(self, k, v )  
       
        or 
        
        args = get_args(locals())
        for k,v in args.items(): self.__dict__[k]= v
         
  '''      
  d= { k:v for k,v in _locals.items() 
      if not k in excludes }
  if newdata: d.update(newdata)
  return d
      
def get_repr(cls): #, clstype='', parentname=''):
    '''
      Could be called in a class's __repr__ to return a str
      representing the attribute values of the class.
      That class must have a self.argnames containing all 
      keys to be reported.
      
      Return a str: "<selftype>(k1=v1, k2="v2", ...)
      
        MenuItem(parent.name="Ctrl_Sec", name="Done", selected=0
        , enabled=1, keepAlive=0, isShown=1, isSecHead=0
        , linkObj=None, linkDict=None)        
      
    '''

    #print( 'in get_repr, clstype="%s"'%clstype )
    #parentname = parentname and parentname or cls.parent.name
    kargs=[]
    for k in cls.argnames:
      v = getattr( cls, k)
      if k in ('parent','menu','page','sec'): 
        v = '.name="%s"'%eval('cls.'+k+'.name') 
      
      else:
        v = ( type(v)==str and '="%s"' or '=%s')%str(v) 
      kargs.append( (k,v) )
    
    s = ( cls.__class__.__name__+'('  
         + ', '.join( [ k + v for k,v in kargs ] ) 
         +')' )
    return s

def menu_line(parent):
  return MenuItem(parent    ## MenuSec class
                , name='-'      
                )       
 
      
#. MenuItem    
class MenuItem( object ):

  '''
  *  item.sel() is the same as item.selected=1, both 
      1. sets _selected=1 and 
      2. triggers on_before_sel() and on_after_sel()
  
  *  item.click():
      1. could sel or unsel the item
      2. triggers on_before_click and on_after_click
      
  *  Use ._selected=1 directly if don't want to trigger events
  
  '''
  def __init__( item   
      , sec     ## MenuSec class
      , name       ## 
      , selected=0 
      , enabled=1  ## if 0: disable and will not be 
                   ## counted into the indices refered  
                   ## to by the menu return, m = dlg_menu(...)   
      , keepAlive=1 ## menu stays alive after click this
      , isShown = 1 ## show name on menu
      , isSecHead=0 ## Is this a section header ?      
      ):

    ## argnames is needed for get_repr()
    item.argnames= locals().keys()
    item.argnames.remove('item')
    item.argnames.insert( item.argnames.index('sec'), 'page')
        
    ## Usually only 'item' (in cases of page, sec: "page" or "sec")
    ## is excluded in arg distribution. But in MenuItem, setting
    ## item.selected will trigger item.__setattr__(), which 
    ## then sets item._selected. So we exclude it as well.  
    args= get_args(locals(), excludes=['item','selected'])  
    for k,v in args.items(): 
      setattr(item, k, args[k] )

    item.parent = sec
    item.sec = sec
    item.page= sec.parent
    item.menu= item.page.parent
    item._selected = selected 
    
  def __setattr__(item, attr, val):
    if attr=='selected': 
      if val: item.sel()
      item._selected= val
    else:       
      item.__dict__[attr]= val
    
  def __getattr__(item, attr):
    if attr=='selected': return item._selected
    else: return item.__dict__[attr] 
           
  def get_label(item):
    '''
      Return processed .name as label
      If item is selected and 
       -- in a checkbox section: prefix with '!'
       -- in a radiobox section: prefix with '?'
        
    '''
    pt = item.sec.typ
    prefix = ( (item._selected and pt=='checkbox') and '!'
              or  (item._selected and pt=='radiobox') and '?'
              or ''
             )
    deco = '______ %s ______'
    label = item.isSecHead and deco%item.name or item.name
    enabled = (not item.enabled or item.isSecHead) and "*" or ""      
    
    label= enabled + prefix + label
    return label   
 

  def sel(item):
  
    item.on_before_sel()
    
    if item.sec.typ== 'radiobox': item.sec.unsel_all()    
    item._selected = 1

    ## Difference between last_sel and item:
    ##   item: limited only to items in user_secs  
    ##   last_sel: could be item in user_secs, or 
    ##   something in tab_sec or ctrl_sec
    item.sec.last_sel = item
    item.page.last_sel = item
    item.menu.last_sel = item
        
    #if not item.sec.name in ('Tab_Sec', 'Ctrl_Sec'):
    if not (isinstance(item.sec, MenuTabSec) 
            or isinstance(item.sec, MenuCtrlSec) ):
      item.sec.item = item
      item.page.item = item 
      item.menu.item = item
    
    item.on_after_sel()
        
  def click(item):
  
    item.on_before_click()
    item.selected = item.selected==0 and 1 or 0
    item.on_after_click()
    
  def on_before_sel(item): pass
  def on_after_sel(item): pass
  def on_before_click(item): pass
  def on_after_click(item): pass
      
  def __repr__(item):
    return get_repr(item) #, 'MenuItem')
    
#. MenuSec

class MenuSecHeader(MenuItem):
  def __init__(secHeader, sec, name, isShown=1):
    MenuItem.__init__( secHeader
              , sec = sec     
              , name = name     
              , selected=0 
              , enabled=0     
              , keepAlive=0 
              , isShown = isShown 
              , isSecHead=1 
              )

    ## argnames is needed for get_repr()
    ##secHeader.argnames= locals().keys()
    ##secHeader.argnames.remove('secHeader')
  def __repr__(secHeader): return get_repr(secHeader) #, secHeader.name)
               
              
class MenuSec(object):
  '''
    Menu section class
  
    - MenuSec
      - .header (MenuSecHeader<-MenuItem) 
      - .items
         MenuItem
         MenuItem
         ...   
    
  
    Each menu could have many pages (class MenuPage),
    and each page has:
    
        -- 1 tab_section
        -- many user_sections
        -- 1 ctrl_section
   
    To add item, either one of the following:
    
      1) .add_item()
      2) .items.append( a_MenuItem_instance )
      3) .items = .items + [ a_MenuItem_instance ]
      
    To add multiple items:
    
      .add_items( ... )
      
      with same args as .add_item(), but instead of a
      single value, they could also be a tuple. For example,
      
        .add_item( 'undo', keepAlive=1 )
        .add_item( 'done', keepAlive=0 )
        
      is the same as:
      
        .add_items( ('undo','done'), keepAlives=(1,0) )
      
      or   
          
        .add_items( ('undo','done'), keepAlives=0 )
         ==> means items[0] is keepAlive, but not items[1]
      
      This is useful when the section is a radiobox:
      
        .add_items( (<many items>, selected= 2 )
        ==> means only items[2] is selected (.selected=1),
            the rest are .selected=0
  '''  

  def __init__(sec, name
              , typ= 'checkbox' # 'radiobox' | 'checkbox' | 'tabs' / 'ctrl' / ''
                          # 'one-item' is a new type of sec that
                          # contains only one-item and, when
                          # get_data(), the sec.name will be
                          # var name and item.name will be val
                          # When add_item, it will always replace
                          # the item[1].
              , page = None
              , items = []
              , isShown= 1
              , isShowHeader= 0
              , hasTopLine= 0
              , hasBotLine= 0
              ):
    #print('>>> sec.init, name = ', name )
 
    ## argnames is needed for get_repr()
    sec.argnames= locals().keys()
    sec.argnames.remove('sec')
           
    args = get_args(locals(), excludes=['sec', 'isShowHeader'])
    for k,v in args.items(): sec.__dict__[k]= v
        
    sec.parent = page
    sec.page = page
    sec.menu = sec.page.parent
    sec.last_sel = None
    
    ## Add header
    sec.header=  MenuSecHeader( sec=sec
                          , name = sec.name 
                          , isShown = isShowHeader 
                          ) 
                         
    #print('>>> Leaving sec.init, self.items = ', sec.items )

  def __setattr__(sec, attr, val):
    if attr=='isShowHeader': sec.header.isShown= val
    else: sec.__dict__[attr]= val
    
  def __getattr__(sec, attr):
    if attr=='isShowHeader': return sec.header.isShown
    else: return sec.__dict__[attr]                            
   
  def get_all(sec, showhidden=0, showdisabled=0, includeheader=1
              , hasline=0):
    '''
      Return a list of all lines including [header] and items, depending
      on the following chart:
      
      -------------------------------------------          
             showhidden   1  1  0  0   
           showdisabled   1  0  1  0
      .isShown .enabled
      -------------------------------------------          
          1       1       1  1  1  1 
          1       0       1  0  1  0
          0       1       1  1  0  0
          0       0       1  0  0  0 
      -------------------------------------------          
    '''
    def isget(x):
      rtn= ( (x.isShown and x.enabled) or (showhidden and showdisabled)
             or (x.isShown and showdisabled) or (x.enabled and showhidden))
      
      return rtn
             
    #print('>>> in sec.get_all, sec.header,sec.enabled = ', isget(sec.header))
    #print('>>> in sec.get_all, isget(sec.header) = ', isget(sec.header))
    
            
    header = (includeheader and sec.header.isShown) and [sec.header] or [] 
    items = [ x for x in sec.items if isget(x) ]
    topline = (hasline and sec.hasTopLine) and [menu_line(sec)] or []
    botline = (hasline and sec.hasBotLine) and [menu_line(sec)] or []
     
    exitbtn= ((isinstance(sec, MenuCtrlSec) and isget(sec.exit_btn)) 
              and [sec.exit_btn] or [])
    return topline+ header + items + exitbtn + botline

  def __get_shown_items(sec):
    return [ x for x in sec.items if x.isShown ]         
                 
  def get_labels(sec, hasline=0):
    '''
      Return 'processed' labels of this sec. 'processed' means proper prefix 
      could be added for display on the menu. For example, '?' or '!' for 
      'checkmark' or 'usual mark'.
    
      To get the 'pure' label, use the .name attribute of sec's content:
                  
      header: <sec>.header.name
      items : [x.name for x in sec.items]
      
    '''   
    #all= sec.get_shown(hasline=hasline, showhidden=showhidden)
    #return [item.get_label() for item in all]
    
    recs = sec.get_all(hasline=hasline)
    labels = [item.get_label() for item in recs]
    return labels 
  
        
  def __get_shown(sec, hasline=0, showhidden=0):
    '''
      .get_shown_items(): the items that are visible
      .get_shown(): include header 
      .get_labels(): labels of get_all()  
    '''  
    h_n_items = sec.get_all(showhidden=0, showdisabled=0)

    exit_btn = ( sec.name=='Ctrl_Sec' and 
                 (showhidden or sec.exit_btn.isShown)
               ) and [sec.exit_btn] or []

      
    items = showhidden and sec.items or sec.get_shown_items()  
    lines = header+ items + exit_btn
    
    if hasline: 
      if sec.hasTopLine:
        lines = [menu_line(sec)]+ lines
      if sec.hasBotLine:
        lines.append( menu_line(sec))

    return lines 
        
    
  def add_item(sec, name       
              , selected=0
              , enabled=1
              , keepAlive=1
              , isShown = 1
              , isSecHead=0
              ):
      d= get_args(locals(),{'sec':sec})
      item = MenuItem( **d )
      sec.items.append( item )

      
  def add_items(sec, names      ## 
              , selected= 0 #10000 #[]
              , enabled= 1 #[]
              , keepAlive= 1 #[]
              , isShown = 1 #[]
              ):
    '''
        add_items( names= ('x1','x2','x3', ...)
                 , selected= 2  ## index: select items [2] = 'x3'
                           = (0,2)  ## tuple: set items [0], [2] to 1 
    '''  
    def get_v( v, i ):
      '''
        given value, v, which is an argument to set all items
        added, and is a list/tuple, containing the either the 
        values of each item (thus the the same length as names):
        
          add_items( names= ('a','b','c'), selected=(1,0,1) )
         
        or names of items that have value=1:
        
          add_items( names= ('a','b','c'), selected=('a','c') )
        
        or any single value: see all items to that value    
      '''
      if type(v)==tuple or type(v)==list: 
        #print( 'In sw_menu sec.add_items/name= %s, get_v: v=%s'%(name, v))
        if type(v[0]) == str:
          return names[i] in v and 1 or 0 #v.index(names[i])
        else:
          return v[i]
      else:
        return v           
                 
    for i,name in enumerate(names):
      #print( 'existing item names= %s, new name="%s"'%(
      #      [x.name for x in sec.items], name ) )   
      sec.add_item( name = name   
                  , selected = get_v( selected,i )
                  , enabled  = get_v( enabled,i )
                  , keepAlive= get_v( keepAlive,i ) 
                  , isShown  = get_v( isShown,i ) 
                  , isSecHead= 0
                   )
    if sec.typ=='radiobox':
      sels = [ x for x in sec.items if x.selected ]
      if len(sels)==0:
        sec.items[-1].selected=1
      elif len(sels)>1:
        #sec.unsel_all()
        sels[-1].selected=1 ## this will unsel all first in radiobox
                        
  def get_item(sec, item):
      #print('>>> in sec.get_item, sec.items = ', sec.items)
      if type(item)==int: return sec.items[item]
      elif type(item)==str: 
        for x in sec.items: 
          if x.name == item: return x
      
  def get_sel_items(sec):
      sels = [ x for x in sec.items if x.selected ]
      if sec.typ=='radiobox' and len(sels)==0: 
          sec.sel_item(0)
          sels = [ sec.items[0] ]
        #elif len(sels)>1: sec.sel_item( sels[0].name )
      return sels
                
  def __get_sel_names(sec):
      return [x.name for x in sec.get_sel_items()]
            
  def sel_item(sec, item_id):
      #print('in <sec>.sel_item, item_id = ', 
      #      type(item_id)==str and item_id or item_id.name)
      #if sec.typ=='radiobox': 
      #  sec.unsel_all()
      
      item = isinstance(item_id, MenuItem) and item_id or sec.get_item(item_id)
      item.sel()
        
      #if item:
      #  print('in <sec>.sel_item, page/sec/item = %s/%s/%s'%(
      #        sec.parent.name, sec.name, item.name))
        
        #item.selected = 1  ## this will set sec.last_sel to item, too
      #if not isinstance(item, MenuTabSec):
      #  sec.item = item
      #  sec.last_sel = item        
          
  def __toggle_item(sec, item_id):  # probably not much use
      x = sec.get_item( item_id )
      x.selected = x.selected==0 and 1 or 0
      print('in sec.toggle_item, item_id=', item_id, ', new .selected = ', x.selected)
      
  def unsel_all(sec): 
      for x in sec.items: x._selected = 0
          
  def click( sec, item_id ):
      sec.on_before_click()
      print('-'*30+'\nin sec.click, item_id=', item_id)
      if sec.typ == 'radiobox':
        sec.unsel_all()
        sec.sel_item( item_id )
      sec.on_after_click()
      
  def get_data(sec):
    #all= sec.get_all( includeheader=0 )
    #items = [x for x in sec.items if x.isShown]
    if sec.typ == "radiobox":
      return sec.item.name.replace('&','')
      #return [ x for x in all 
      #         if x.selected ][0].name.replace('&','') 
    else:
      return { x.name.replace('&',''): x.selected and 1 or 0
              for x in sec.items if x.isShown }
              
      #return { x.name.replace('&',''): x.selected 
      #         for i,x in enumerate( all )
      #         if i!=0 }     

  def get_loadable_data(sec): ## 2016.4.1
    '''
    loadable_data= {
      'P0': 
          ( ('my_sec', (('a',1), ('c',0), ('b',1) ) ), ) # checkbox 
      ,'P1': 
          ( ('color', ( 'green', ('blue','green') ))   # radiobox
              , ('Sec0', ( ('abc',1), ('def',1)) ) 
              , ('Set_var', (('width',1), ('height', 0)) )
              )
        }
    '''
    all= sec.get_all( includeheader=0 )
    shown_items = all #sec.get_shown_items()
    show_item_names = [ x.name.replace('&','') for x in shown_items ]#[1:]
      
    if sec.typ == "radiobox":
    
      sel = [ x.name for x in shown_items if x.selected ][0]
      
      return [sel, show_item_names]
    
    else:
    
      return [ (x.name.replace('&',''), x.selected) 
              for x in shown_items ] #[1:]
      

  def on_before_click(sec):pass
  def on_after_click(sec):pass
  
  def __repr__(sec): return get_repr(sec) #, sec.name)
                    
                
  
        
#. MenuPage 

class MenuTabSec(MenuSec):
  '''
    
  ''' 
  def __init__(tabsec , page
                    , name='Tab_Sec'
                    , items = []
                    ):
    #print( '>>> in MenuTabSec.__init__()')
    MenuSec.__init__(tabsec, 
         name = name
        , typ= 'radiobox'
        , page= page
        , items = items
        , isShown= len(page.parent.pages)>1 and 1 or 0
        , isShowHeader= 0
        , hasTopLine= 0
        , hasBotLine= 1
        )     

class MenuCtrlSec(MenuSec):
  '''
    
  ''' 
  def __init__(ctrlsec, page
                    , name='Ctrl_Sec'
                    ):
    #print( '>>> in MenuTabSec.__init__()')
    MenuSec.__init__(ctrlsec, 
         name = name
        , typ= 'radiobox'
        , page= page
        , items = []
        , isShown= 1
        , isShowHeader= 0
        , hasTopLine= 1
        , hasBotLine= 0
        ) 
    
    ctrlsec.exit_btn = MenuItem(name=EXIT_LABEL
                    , sec=ctrlsec
                    , selected= 0
                    , keepAlive=0  
                    ) 
                     
                           
class MenuPage(object):

  def __init__(page, dlg_menu, menu, name='P0', isShown=1):
  
    page.parent = menu
    page.menu = menu
    page.name = name
    page.sec = None
    page.item= None    
    page.undos = []
    page.isShown = isShown
    page.dlg_menu= dlg_menu    
    page._page_setup()
   
    ## argnames is needed for get_repr()
    page.argnames= locals().keys()
    page.argnames.remove('page')
    
  def _page_setup(page):  
    #print( '>>> in MenuPage._page_setup(), .name="%s"'%page.name, ', len(pages)=', len(page.menu.pages))
    
    tab_sec = MenuTabSec( page=page )
    page.tab_sec = tab_sec 
    page.user_secs= []    
    page.ctrl_sec= MenuCtrlSec( page=page )
    #page.isShown = len(page.menu.pages)>1 and 1 or 0
    
    #pages = page.menu.pages
    #tab_sec.items=[]  ## Somehow we need to reset this to []. This shouldn't
                      ## have happened. But if we don't, there will be 
                      ## duplicate items  
    #print('>>> in MenuPage, tab_sec.items: ', len(tab_sec.items), [x.name for x in tab_sec.items])
    
#    if len(pages)==0:
#        page.tab_sec.add_item(name=page.name, selected=1) 
#    else:
#    #if page:
#      for p in pages:
#        ## Add each existing page name as items to the current page
#        page.tab_sec.add_item(name=p.name, selected=0) 
#        #if p.name!=page.name:
#        ## Add current page to tab_sec of existing pages
#        p.tab_sec.add_item(name=page.name, selected=0) 
#    
    #pages.append(page)    
                             
    
    #print( '>>> Leaving MenuPage._page_setup(), .name="%s"'%page.name, ', len(pages)=', len(page.menu.pages))    
                            
  def __getattr__(page, attr):
    if attr=='secs':
      return page.get_all_secs()
    elif attr=='items':
      return page.get_all_items()  
    elif attr=='shown_items':
      return page.get_all(includeheader=0) #shown_items()  
    elif attr in dir(page):
      return getattr(page, attr)
    else:
      raise AttributeError('Errpr in MenuPage "%s": attr "%s" not found'%(page.name, attr))
         
          
  #  def __setattr__(page, attr,v):
  #    if attr=='secs':
  #       raise KeyError( 
  #          'secs in MenuPage (name:"%s") is read=only "%s"'%(
  #           page.name))
  #    else:
  #      setattr(page, attr, v)           
                              
  def add_section(page, name=None, typ='checkbox'
                  #, parent=None
                  , items=None ## NOTE: for unknown reason, setting this to []
                               ## will bring over the items of previous section,
                               ## which shouldn't have happened. Has to set it
                               ## to None   
                  , isShowHeader=1
                  , **kargs):
    '''Add user section''' 
    #print('In Page.add_section, items=%s'%(items))             
    if name==None: name = 'Section_%s'%len(page.user_secs)
    sec = MenuSec(name=name, typ=typ, page=page
                  , items= items!=None and items or []
                  , isShowHeader=isShowHeader, **kargs) 
                  
#      def __init__(sec, name
#              , typ= 'checkbox' # 'radiobox' | 'checkbox' | 'tabs' / 'ctrl' / ''
#                          # 'one-item' is a new type of sec that
#                          # contains only one-item and, when
#                          # get_data(), the sec.name will be
#                          # var name and item.name will be val
#                          # When add_item, it will always replace
#                          # the item[1].
#              , page = None
#              , items = []
#              , isShown= 1
#              , isShowHeader= 0
#              , hasTopLine= 0
#              , hasBotLine= 0
#              ):              
                  
                          
    page.user_secs.append( sec )
    return sec    
  
  def add_radiobox(page, sec_name, item_names, isShowHeader=1, **kargs):
      sec= page.add_section(typ='radiobox'
                   , name=sec_name, isShowHeader=isShowHeader
                   )
      sec.add_items( item_names, selected=0, **kargs ) 
      return sec
      
  def add_checkbox(page, sec_name, item_names, isShowHeader=1, **kargs):
      sec= page.add_section(typ='checkbox', name=sec_name, isShowHeader=isShowHeader)
      #sec.add_items( item_names, selected=0, **kargs ) 
      sec.add_items( item_names, **kargs ) 
      return sec
      
  def get_sec(page, sec_id): ## sec_id = index or name
    return ( type(sec_id)==int and page.secs[sec_id]
             or [ sec for sec in page.secs
                if sec.name==sec_id ][0]
           )
                
  #def get_all_secs(page):
  #  return [page.page_sec] + page.user_secs + [page.ctrl_sec]

  def get_all_secs(page):
    return [page.tab_sec] + page.user_secs + [page.ctrl_sec]
      
  #def get_shown_user_secs(page):
  #  return  [ sec for sec in page.user_secs if sec.isShown ]
  
  #def __get_all_shown_secs(page):
  #  return  [ sec for sec in page.get_all_secs() if sec.isShown ]
    
    #print( 'in MenuPage.get_all_shown_secs, page.page_sec.isShown=%s'%page.page_sec.isShown)
    #    return  ((page.page_sec.isShown and [page.page_sec] or [] )
    #            + page.get_shown_user_secs() 
    #            +(page.ctrl_sec.isShown and [page.ctrl_sec] or [] )
    #            )
  
  def get_all(page, showhidden=0, showdisabled=0, includeheader=1
              , hasline=0):
    allsecs = page.get_all_secs()
    #print( '>>>in page.get_all, allsecs=', allsecs)
    all = [ item for sec in allsecs  
             for item in sec.get_all(showhidden=showhidden
                               , showdisabled=showdisabled
                               , includeheader=includeheader
                               , hasline=hasline)
             if sec.isShown or showhidden                  
          ]
          
    return all
       
  def __get_shown_items(page):
  
    #    page_sec = page.page_sec.get_shown_items()
    #    user_sec = [ item 
    #                 for sec in page.get_shown_user_secs() 
    #                 for item in sec.get_shown_items() ]
    #    ctrl_sec = page.ctrl_sec.get_shown_items()
    #    return ( page_sec 
    #           + (page_sec and MenuItem(page,  or [])
    #           + user_sec + ['-'] + ctrl_sec
    #           )
    return [ x for sec in page.get_all_shown_secs()
               for x in sec.get_shown_items() ]
  
  def __get_all_items(page):
    return [ x for section in page.get_all_secs()
               for x in section.items ]

  def __get_shown(page, hasline=0, showhidden=0):
    return [ x for sec in page.get_all_secs() 
             for x in sec.get_shown(
                hasline=hasline, showhidden=showhidden)
             if showhidden or sec.isShown ]
                
  def get_labels(page, hasline=1):
    #return [ item.get_label() for item in page.get_shown_items(hasline=1)]
    all= page.get_all(hasline=hasline)
    #print( '>>> in page.get_labels, all = ', [x.name for x in all])
    #print( '>>> in page.get_labels, all = ', [x.name for x in page.secs])
    #return 
    
    labels = [ x.get_label() for x in all ]
    return labels 
    
    return [ label for sec in page.secs 
             for label in sec.get_labels(
                showhidden=0, hasline=hasline)
             if showhidden or sec.isShown ]
    # [item for sublist in l for item in sublist]
    
       
  def show(page): ## MenuPage.show()
           
    page.menu.page = page
             
    #print('\n>>> MenuPage.show(), page/sec/item="%s"/"%s"/"%s"'%(
    #      page.name, page.sec.name, page.item.name))
    
    #    print('... page.sec.name="%s", page.item.name="%s"'%(
    #            page.sec and page.sec.name or 'None'
    #            ,page.item and page.item.name or 'None'
    #         ))
        
    ## This section handles cases when user switchs to a new page,
    ## then click [Done] to exit the menu before clicking any item.
    ## In that case, we need to set a default sel item
    ## This usually happens when user spots the selection is already
    ## what is wanted. 
    ## Note that this could still return sec=None and item=None, if 
    ## the only user_sec is a checkbox and none is checked.
    ##
    if not page.item or page.item.parent.name == 'Tab_Sec':
      user_items= [ item for sec in page.user_secs for item in sec.items 
                    if item.isShown and item.selected ]
      page.item = user_items and user_items[0] or None
      page.parent.item = page.item
      
    if not page.sec and page.item:
      page.sec = page.item.parent
      page.parent.sec = page.sec  

    #    print('... page.sec="%s", page.item="%s"'%(
    #            page.sec and page.sec.name or 'None'
    #            ,page.item and page.item.name or 'None'
    #         ))
         
    print('\n___>>> MenuPage.show(), page/sec/item= %s/%s/%s'%(
          page.name, page.sec.name, page.item.name))
          
    ## Need to make sure that in the tab_sec, the tab of this page's name
    ## is the one that's selected, but don't want to trigger item's 
    ## event handlers (we are already in the page where the tab indicated
    ## so no need to trigger it again )
    page.tab_sec.get_item(page.name)._selected=1
    
    ## For locating the clicked item
    all_shown= page.get_shown(hasline=1)
    for x in all_shown:
      x.name = x.name.replace('&','').replace('!','').replace('?','')
               
    ## For display all labels on menu              
    labels = page.get_labels(hasline=1) 

    ## Show menu and get return. m = int for the index of selected
    m= page.dlg_menu( MENU_STD, caption=page.name
                    , text= '\n'.join( labels ) )    
    
    #print('... m=%s, page.get_labels(hasline=1) = %s, all_shown[m].name="%s"'%(m, page.get_labels(hasline=1)[:6], all_shown[m].name))
      
    if type(m)==int:  ## Hey, we got a click !!
    
      item = all_shown[m]
      print('___ In MenuPage.show(), "%s" is clicked'%(item.name))
      if item.name == EXIT_LABEL:
        page.parent.on_exit()
        print('\n___<<< MenuPage.show():, sec/item= %s/%s'%(
          page.sec.name, page.item.name))
      
      else:
        
        item.selected=1
        sec = item.parent
        
        ## Does user select a tab? If yes, nextpage is a new page; 
        ## otherwise, nextpage is still the current one
        ##
        if sec.name=='Tab_Sec': 
          nextpage = page.parent.get_page( item.name )
          for i,p in enumerate(page.parent.pages):
            p.isShown = p==nextpage and 1 or 0
            
        else:
        
          nextpage = page    
       
        page.sec = sec
        page.item = item
     
        page.parent.page = nextpage     
        page.parent.item = item
        page.parent.sec = sec
        
        if sec!= page.tab_sec and (not item.keepAlive):
          nextpage=None
         
        page.item.click()
        page.parent.on_sel(item)      
        #page.sec.click( item.name )      
        page.on_sel()
        #if item.func: 
        #  item.func(ed, page.item)
      
        #print('\nLeaving MenuPage.show():, nextpage.name=%s, sec.name=%s, item.name=%s, m=%s\n'%(
        #      nextpage.name,sec.name,item.name,m)) 
        
        print('\n___<<< MenuPage.show():, nextpage/sec/item= %s/%s/%s'%(
          nextpage.name, page.sec.name, page.item.name))
          
        return (nextpage, sec, item) 
                      
  def on_sel(page):
      pass

      
  def get_data(page):
    
    return { sec.name: sec.get_data()   
                for sec in  page.user_secs
                if sec.isShown
            }
        
  def get_loadable_data(page): ## 4/1/2016
    
    return [ (sec.name, sec.get_loadable_data() )   
                for sec in  page.user_secs #get_shown_user_secs()
                if sec.isShown
            ]    
            
  def __repr__(page): return get_repr(page, page.name)
                    
                        
#. Menu                
class Menu(object):
  '''
  
    menu = sw_menu.Menu(dlg_menu)
    (menu.add_radiobox(name='color', item_names=('blue','green'))     
         .add_radiobox(name='align', item_names=('left','center','right'))
         .add_checkbox(name='default', item_names=('on','autosave'))
    )             
    menu.show()
  
    menu.get_data():
    {'color': 'green', 'align': 'center', 'default': {'autosave': 1, 'on': 1}}

  '''
  def __init__(menu, dlg_menu=None, name='menu'):

    menu.name = name
    menu.pages= []
    menu.labels=[]
    menu.dlg_menu = dlg_menu
    menu._menu_setup()
    
  def _menu_setup(menu): ## Needs the __init__ to be compled
    #print('>>> Menu._menu_setup, adding page:')
    menu.page = menu.add_page(name='P0',isShown=1)
    #menu.pages = [menu.page]  ## NOTE: menu.add_page adds 
    menu.sec  = None 
    menu.item = None 

    ## argnames is needed for get_repr()
    #menu.argnames= locals().keys()
    #menu.argnames.remove('menu')
    
    
  def sel_page(menu, page_id):
    ''' 
      <Menu>.sel_page(0) or <Menu>.sel_page('page_name') 
    '''
    for p in menu.parent.pages: p.isShown=0
    page.isShown=1
    
    if type(page_id)==int: page= menu.pages[page_id]
    else:
      names = [ p.name for p in menu.pages ]
      menu.page= menu.pages[ names.index(page_id) ]
      page= menu.page 
    
    return page

      #print('in <sec>.sel_item, item_id = ', item_id)
  
      
  def add_page(menu, name='', isShown=1):
    name = name=='' and 'P%s'%len(menu.pages) or name
    p= MenuPage(menu.dlg_menu, menu=menu
               , name=name, isShown=isShown) 
    pages = menu.pages             
    pages.append(p)
    
    isShown = len(pages)>1 and 1 or 0
    
    pnames = [p.name for p in pages]
    for p in pages:
      p.tab_sec.items=[]           
      p.tab_sec.add_items( pnames )
      p.tab_sec.sel_item(p.name)
      p.tab_sec.isShown = isShown
      
    return p
     
    
  def get_page(menu, page_id):
    ''' 
      <Menu>.get_page(0) or <Menu>.get_page('page_name') 
    '''
    if type(page_id)==int: p= menu.pages[page_id]
    elif type(page_id)==str:
      names = [ p.name for p in menu.pages ]
      p= menu.pages[ names.index(page_id) ]
    elif isinstance(page_id)==ManuPage:
      p=page_id  
    #print( 'In menu.get_page, len(p.ctrl_sec.items)= ', len(p.ctrl_sec.items))
    return p
      
    
    
  def add_radiobox(menu, sec_name, item_names, page_id=None, isShowHeader=1, **kargs):
  
    d = get_args( locals(), kargs)
    #print('in Menu.add_radiobox, d=', d)
    #page = page_id==None and menu.pages[0] or menu.get_page(page_id)
    page = page_id==None and menu.page or menu.get_page(page_id)
    del d['page_id']
    del d['kargs']
    del d['menu']
    page.add_radiobox( **d )
    return menu                     



  def add_checkbox(menu, sec_name, item_names, page_id=None,isShowHeader=1,   **kargs):
    d = get_args( locals(), kargs)
    #page = page_id==None and menu.pages[0] or menu.get_page(page_id)
    page = page_id==None and menu.page or menu.get_page(page_id)
    del d['page_id']
    del d['kargs']
    del d['menu']
    page.add_checkbox( **d )
    menu                     
          
  def show(menu):  ## Menu.show()
    print('\n>>>>>> menu show(), menu.page.name="%s"'%(menu.page.name))
    
    menu.labels=[]
    sel = menu.page
    nextpage = menu.page
    pre_rtn = None # rtn in previous cycle. When an item is clicked,
                   # menu.page, .sec and .name will be set to rtn that
                   # represents the item. But if the item is the exit
                   # button (has EXIT_LABEL), then rtn will be reverted
                   # back to pre_rtn
    rtn= None
    while nextpage:
      
      rtn= nextpage.show() # rtn = (nextpage, sec, item)
                           # In a page.show(), menu.page, .sec and .item
                           # will be re-set to new values
      #print('... In menu.show, return from page.show, menu.page.name="%s"'%(menu.page.name), '\n...   next.page.name="%s"'%( rtn and rtn[0].name or None)) 
      if rtn==None: nextpage= None
      
      elif rtn[2]==menu.page.ctrl_sec.exit_btn: #get_exit_item():
        if not pre_rtn: pre_rtn = rtn
        nextpage, sec, item = pre_rtn 
        menu.page = nextpage
        menu.sec = sec
        menu.item = item       
        nextpage=None
        
      else: #rtn[2]!=menu.get_exit_item():
        nextpage, sec, item = rtn 
        menu.page = nextpage
        menu.sec = sec
        menu.item = item       
      
      if nextpage:
        pre_rtn = rtn
        menu.sel_page= nextpage
      
    #print('Exiting ... rtn= %s'%str(rtn))
        
    #if menu.item and menu.item.name == EXIT_LABEL:
    if rtn and rtn[2].name == EXIT_LABEL:
      menu.on_exit()
        
    print('\n<<<<<< menu show() leaving, menu.page.name="%s"'%(menu.page.name))
    return rtn
             

  def get_data(menu):
    rtn={}
    pages= [ rtn.update( page.get_data() ) 
                for page in  menu.pages 
           ]
    return rtn
    
  #  def get_exit_item(menu):
  #    
  #    rtn= [x for x in menu.page.ctrl_sec.items if x.name == EXIT_LABEL][0]
  #    #print('in Menu.get_exit_item, exit item= %s'%str(rtn))
  #    return rtn
               
  def get_data_by_page(menu):
    return { page.name: page.get_data() 
                for page in  menu.pages 
           }
  
  def get_loadable_data(menu):  ## 2016.4.1
    '''
      Return data in the loadable format so it can be loaded to 
      other menu using menu.load(data):
      
          data= {'P0': ( ('my_sec', (('a',1), ('c',0), ('b',1) ) ), ) # checkbox 
        ,'P1': ( ('color', ( 'green', ('blue','green') ))   # radiobox
              , ('Sec0', ( ('abc',1), ('def',1)) ) 
              , ('Set_var', (('width',1), ('height', 0)) )
              )
        }
    
    
    Copy the following to the command.on_open in __init__ to test:
    
        # test sw_menu.load
    data= (
          ('P0',( ('my_sec', (('a',1), ('c',0), ('b',1) ) ), ) # checkbox
          ) 
        , ('P1', ( ('color', ( 'green', ('red', 'blue','green') ))   # radiobox
              , ('Sec0', ( ('abc',1), ('def',1)) ) 
              , ('Set_var', (('width',1), ('height', 0)) )
              )
          )    
        )   
        menu= sw_menu.Menu( dlg_menu )
        menu.load( data )
        recovered_data = menu.get_loadable_data()
        recovered_data = [ (k+'_x',v) for k,v in recovered_data ]
        print('recovered_data = ', recovered_data )
        menu.load( recovered_data )  
        menu.show() 
        
    '''
    return [ (page.name, page.get_loadable_data()) 
            for page in menu.pages 
           ]
    
  def load(menu, data): ##(4/1/2016) 
    '''
    
    data format:
    
    [('P0', 
      [('my_sec', [('x0', 0), ('y0', 1), ('z0', 0), ('a', 0), 
                   ('b', 0), ('c', 0), ('m', 0), ('n', 0)]), 
       ('radio_sec', ['C', ['A', 'C']])]), 
    ('P1', 
      [('color', ['green', ['blue', 'green']]), 
       ('Set_var', [('width', 0), ('height', 0)])])]
 
    
      load data by page:
        
        (
          ('P0',( ('my_sec', (('a',1), ('c',1), ('b',1) ) ), ) # checkbox
          ) 
        , ('P1', ( ('color', ( 'blue', ('blue','green') ))   # radiobox
              , ('Sec0', ( ('abc',1), ('def',1)) ) 
              , ('Set_var', (('width',1), ('height', 0)) )
              )
          )    
        )      
             
       following:
         
       ( ('page_name', ( 'user_sec_name', dataitem ) )
       
       where dataitem are tuples of tuples. 
       
        if 1st item is not a tuple: sec is a checkbox
        if 1st item IS     a tuple: sec is a radiobox        
    '''
    
    for pname, secs in data:
      if not pname in [ p.name for p in menu.pages]:
        menu.add_page( pname )
      for secname, items in secs:
        #print('>>> in load(), secname=%s, items=%s'%(secname,items))
        if type(items[0]) in (tuple, list): # checkbox
          itemnames = [ x[0] for x in items ]
          itemvals = [ x[1] for x in items ]
          menu.add_checkbox( secname, itemnames, page_id=pname
                              , selected=itemvals )
        else: ## radiobox, items = ( value, possible_values )
          itemnames = items[1]
          sec= menu.add_radiobox( secname, itemnames, page_id=pname )
          #sec.sel_item( items[0] )
          menu.get_page(pname).get_sec(secname).sel_item( items[0] )
    

  #def __repr__(menu): return get_repr(menu, menu.name)
                                             
  def on_sel(menu, item):
    pass
 
  def on_exit(menu):
    '''
      Called when users click on the exit button.
      Note that the menu can also be made disappeared
      by clicking anywhere outside the menu. In that
      case, this function will NOT be called. 
      
      Difference bewteen on_sel and on_exit: 
        on_sel: menu.page, .sec and .item represent
                  exactly the item that's clicked or selected
        on_exit : The exit item is clicked or selected. 
                  menu.page, .sec and .item represent
                  the item that's PREVIOUSLY clicked or selected
                  before clicking exit item            
    '''
    pass
    
    
def Menu_test():
  '''
    ---------------------------------------------
    
    dlg_menu is part of SynWrite and is required in menu.show().
    But we are doing test w/o menu.show(), so it's ok to set it None.
    
    >>> menu= Menu(dlg_menu=None)
    
    A menu has .pages (menu.pages), and a page has sections
    (page.secs). Each section has items (sec.items). 
    
    ---------------------------------------------

    Every menu comes with a default page:
    
    >>> [page.name for page in menu.pages]
    ['P0']
    

    >>> P0 = menu.pages[0]
    >>> len(menu.pages), len(P0.tab_sec.items)
    (1, 1)
    
    Every page comes with 3 parts:
  
      page.tab_sec    <MenuTabSec>
      page.user_secs --- a empty list of secs  <MenuCSec>
      page.ctrl_sec   <MenuCtrlSec>
  
    P0.secs = all sections
    
    >>> [sec.name for sec in P0.secs ]
    ['Tab_Sec', 'Ctrl_Sec']

    >>> P0.user_secs
    []
    
    Users can add user secs later on :

      page.tab_sec 
      page.user_secs = [<sec>, <sec>...]
      page.ctrl_sec
          
    
    ---------------------------------------------

    Each section has a .header. The ctrl section has an extra .exit_btn:
    
      <sec>.header
      <sec>.items 
      <ctrl_sec>.exit_btn
    
    And if <sec>.hasTopLine and <sec>.hasBotLine = 1:
    
        =================  -- MenuItem() instance
        <sec>.header       -- MenuItem() instance
        <sec>.items        -- list of MenuItem() instances
        <ctrl_sec>.exit_btn  -- MenuItem() instance
        =================  -- MenuItem() instance        
  
    ---------------------------------------------

    <sec>.header is not clickable (this is set by prefixing it with a '*'
    in .get_label() ), and its .isShown decides its visibility.
    
    ctrl_sec.exit_btn is always visible (isShown=1), and keepAlive=0, means 
    a click closes the menu. 
    
    ---------------------------------------------
    
    If a <sec> is of typ 'radiobox', then .items are considered 'options'
    to pick from for variable named <sec>.name
    
    ---------------------------------------------
    
    All levels have .get_labels() ( .get_label() from MenuItem ). When 
    menu.show(), it's the return of menu.get_labels() that's actually 
    placed on the menu.    
    
    ---------------------------------------------
    
    Lets start with user secs:
    
    
    ##### User_secs #####
    
    
    Each of user sections (page.user_secs) is a MenuSec containing:    
        
        ==========  -- MenuItem()  ( when <sec>.hasTopLine )
        .header     -- MenuItem() 
        .items      -- [ MenuItem(), MenuItem(), ...]
        ==========  -- MenuItem() ( when <sec>.hasBotLine )
        
    Every new section will be added to page.user_secs: 
    
    >>> [sec for sec in P0.user_secs ]
    []
    >>> mysec= P0.add_section('my_sec')
    >>> mysec          # doctest: +NORMALIZE_WHITESPACE
    MenuSec(name="my_sec", isShown=1, hasTopLine=0, hasBotLine=0, 
    isShowHeader=1, items=[], typ="checkbox", page.name="P0")
                  
    >>> [sec.name for sec in P0.user_secs ]
    ['my_sec']
    
    Note the difference between name and label:
    
    >>> mysec.name
    'my_sec'
    
    >>> mysec.get_labels()
    ['*______ my_sec ______']
               
    
    New section has no item:
    
    >>> [item for item in mysec.items]     
    []
   
    But has a header that is set to be visible:
    
    >>> mysec.header      # doctest: +NORMALIZE_WHITESPACE
    MenuSecHeader(name="my_sec", isShown=1, selected=0, enabled=0, isSecHead=1, page.name="P0", sec.name="my_sec", keepAlive=0)
        
    >>> mysec.get_labels()
    ['*______ my_sec ______']
     
    ---------------------------------------------
    
    <sec>.typ: 'checkbox' (default) | 'radiobox' | ''
    <sec>.header
    <sec>.items: all items (NOT include .header or .exit_btn)    
    <sec>.last_sel: last selected item
    <sec>.hasTopLine
    <sec>.hasBotLine
    
    <sec>.get_shown_items(): visible items (isShown=1)    
    <sec>.get_labels( showhidden=0, hasline=0 ): 
          return properly handled labels that are item names (for example, 
          prefixed with '?' to show checkmark) including header, items
          and exit_btn (in case of ctrl_sec). 
    <sec>.add_item( name, ...) 
    <sec>.add_items( names, ... )
    <sec>.get_item( item )  # get_item('name'), get_item(2)
    <sec>.get_sel_items()
    <sec>.sel_item( item )
    
    
           
    ---------------------------------------------
    
    ## Add new item:
    
    >>> mysec.add_item('x0')
    >>> [item.name for item in mysec.items]     
    ['x0']
    
    <sec>.add_item(menu, name       
              , selected=0
              , enabled=1
              , keepAlive=1
              , isShown = 1
              , isSecHead=0
              #, func= None     # reserved
              #, linkObj = None # reserved
              #, linkDict= None # reserved
              )
              
    >>> x0= mysec.items[0]
    >>> type(x0)
    <class '__main__.MenuItem'>
    >>> isinstance(x0, MenuItem)
    True
    
    >>> x0         # doctest: +NORMALIZE_WHITESPACE
    MenuItem(name="x0", isShown=1, selected=0, enabled=1, isSecHead=0, page.name="P0", sec.name="my_sec", keepAlive=1)
        
    ---------------------------------------------

    The <item>.selected is 1 for the picked item in a sec of typ='radiobox'.
    If the sec is of typ='checkbox', there could be 0 or more items with
    .selected=1
    
    >>> mysec.typ
    'checkbox'
    
    >>> [x.selected for x in mysec.items]
    [0]
           
    ---------------------------------------------
  
    Add multiple items with add_items( names, ... )
    
    All args other than names are the same as add_item,
    except that they can be assigned a tuple/list, too.
    
    Take 'selected' as example:
    
      selected=1: all items are selected. 
      selected=0: all items are unselected. 
      selected= [ 1,0,1...]  same len as the names, setting individual value
      selected= ['a','c'] only item names = 'a' or 'c' have selected=1      
    
    >>> [item.name for item in mysec.items]     
    ['x0']
    
    >>> mysec.add_items( ('y0','z0'), keepAlive=1 )
    
    >>> [item.name for item in mysec.items]     
    ['x0', 'y0', 'z0']
    
    <item>.keepAlive means when <item> clicked, the menu doesn't not close.
    
    >>> [item.keepAlive for item in mysec.items]     
    [1, 1, 1]
    
    Items 0, 2 ('a','c' below) are kept alive:
        
    >>> mysec.add_items( ('a','b','c'), keepAlive=('a','c') )
    >>> [item.keepAlive for item in mysec.items]     
    [1, 1, 1, 1, 0, 1]
               
    >>> mysec.add_items( ('m','n'), keepAlive=0 )
    >>> [item.keepAlive for item in mysec.items]     
    [1, 1, 1, 1, 0, 1, 0, 0]
         
    >>> [x.name for x in mysec.items ]
    ['x0', 'y0', 'z0', 'a', 'b', 'c', 'm', 'n']
        
    >>> mysec.get_labels()  # doctest: +NORMALIZE_WHITESPACE
    ['*______ my_sec ______', 'x0', 'y0', 'z0', 'a', 'b', 'c', 
     'm', 'n']
     
    >>> mysec.get_item('y0').selected=1
    >>> [ x.selected for x in mysec.items ]
    [0, 1, 0, 0, 0, 0, 0, 0]
    
    Note that 'y0' is prefixed with a '!': 
    
    >>> mysec.get_labels()  # doctest: +NORMALIZE_WHITESPACE
    ['*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n']    
         
    >>> mysec.header.isShown=0
    >>> mysec.get_labels()  # doctest: +NORMALIZE_WHITESPACE
    ['x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n']
      
    >>> mysec.hasTopLine=1
    >>> mysec.hasBotLine=1
    >>> mysec.get_labels(hasline=1)
    ['-', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n', '-']
       
    >>> mysec.header.isShown=1
    >>> mysec.get_labels(hasline=1)
    ['-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n', '-']
    
    >>> radio= P0.add_section('radio_sec', typ='radiobox')
    >>> [ sec.name for sec in P0.secs]
    ['Tab_Sec', 'my_sec', 'radio_sec', 'Ctrl_Sec']
    
    >>> radio.items
    []
    
    >>> radio.get_labels()
    ['*______ radio_sec ______']
        
    >>> radio.add_items( ('A','B','C'), isShown=(1,0,1), keepAlive=('A',) )
    
    Note: the sec is a radiobox, so one and only one item should be selected.
    Since it wasn't set when add_items above, the last one added is autoset:
    
    >>> radio.items     # doctest: +NORMALIZE_WHITESPACE
    [MenuItem(name="A", isShown=1, selected=0, enabled=1, isSecHead=0,
     page.name="P0", sec.name="radio_sec", keepAlive=1), 
     MenuItem(name="B", isShown=0, selected=0, enabled=1, isSecHead=0,
      page.name="P0", sec.name="radio_sec", keepAlive=0), 
     MenuItem(name="C", isShown=1, selected=1, enabled=1, isSecHead=0,
      page.name="P0", sec.name="radio_sec", keepAlive=0)]    

    >>> [x.isShown for x in radio.items]
    [1, 0, 1]
     
    >>> radio.get_labels()
    ['*______ radio_sec ______', 'A', '?C']

      
    Sel item by index:
    
    >>> radio.sel_item(0)
    >>> radio.get_labels()
    ['*______ radio_sec ______', '?A', 'C']

    Sel item by item name:
    
    >>> radio.sel_item('C')
    >>> radio.get_labels()
    ['*______ radio_sec ______', 'A', '?C']
    >>> radio.last_sel.name 
    'C'
    
    Sel item by item:
    
    >>> radio.sel_item( radio.items[-1] )
    >>> radio.get_labels()
    ['*______ radio_sec ______', 'A', '?C']
    >>> radio.last_sel.name 
    'C'
    
    Sel item by assigning .selected:
    
    >>> radio.get_item('B').selected=1
    >>> radio.get_labels()
    ['*______ radio_sec ______', 'A', 'C']
    >>> radio.last_sel.name 
    'B'
     
    Note: 'B' is hidden, but it can still be selected. Users have to take 
    care of this.

    Page P0.get_labels() w/hasline=1 give a list of labels that are to be
    fed directly to menu.  
    
    >>> radio.sel_item('C')
      
    >>> P0.get_labels(hasline=1)       # doctest: +NORMALIZE_WHITESPACE
    ['-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n',
     '-', '*______ radio_sec ______', 'A', '?C', '-', 'Done']  
       
    ---------------------------------------------
    
    
    ## Tab_sec
    
    Tab_sec is a tab holder, holding names of pages for users to switch 
    between pages. If there's only one page (as in startup), Tab_Sec is 
    invisible.
    
      page.tab_sec = [ <item as P0>, <item as P1> ... ]
      page.user_secs = [<sec>, <sec>...]
      page.ctrl_sec    
    
    At startup, the Tab_Sec contains only the header which is Tab_Sec's 
    header.
    
      tab_sec.header     -- MenuItem() instance
           
    >>> P0.tab_sec.isShown
    0
        
    ---------------------------------------------

    If new page(s) are added, names of pages (including the 1st page) will be 
    added as tab_sec items upon .show(), with a separator line ( that is 
    returned by menu_line()) in the sec bottom :
    
      tab_sec.header     -- MenuItem() instance
      tab_sec.items = [ item 0 -- MenuItem() instance named 'P0'
                      , item 1 -- MenuItem() instance named 'P1' 
                      ]
                      
    and will be displayed as:
                       
      -------------
      P0
      P1
      -------------
      
    ---------------------------------------------

    This same items will be shown across all pages:
    
    For page 0:
    
      -------------
      * P0
        P1
      -------------
    
    For page 1:
    
      -------------
        P0
      * P1
      -------------
    
    >>> tab_sec= P0.tab_sec    
         
    >>> tab_sec.header    # doctest: +NORMALIZE_WHITESPACE
    MenuSecHeader(name="Tab_Sec", isShown=0, selected=0, enabled=0, 
    isSecHead=1, page.name="P0", sec.name="Tab_Sec", keepAlive=0)
       
    >>> tab_sec.items     # doctest: +NORMALIZE_WHITESPACE
    [MenuItem(name="P0", isShown=1, selected=1, enabled=1, isSecHead=0,
      page.name="P0", sec.name="Tab_Sec", keepAlive=1)]
            
    .get_labels: get the labels of visibal header/items/etc:
    
    >>> tab_sec.get_labels()
    ['?P0']
    
    >>> tab_sec.header.isShown
    0
    >>> tab_sec.header.isShown= 1
    >>> tab_sec.get_labels()
    ['*______ Tab_Sec ______', '?P0']
    >>> tab_sec.header.isShown= 0
    
    tab_sec.isShown is 0 if menu.pages= only one page:
    
    >>> tab_sec.isShown
    0
    
    So even tab_sec.header.isShown, the entire sec will not be visible.
    
    The '*' in front of item label is to disable the item (make it un-clickable)
    See https://sourceforge.net/p/synwrite/wiki/py%20menu%20id/

        If item is one char "-", separator will be shown.
        If item begins with "?" or "!" char, "radio checkmark" or 
           "usual checkmark" will be shown.
        If item begins with "*" char, it will be disabled.
        Hotkeys must be preceded with "&" char.    

    ---------------------------------------------

    ## Ctrl_sec
    
    Ctrl_sec contains no items, but a header and an exit_btn. The exit_btn,
    default name = 'Done', is the only clickable item in this sec:
        
        .header
        .exit_btn
            
    >>> ctrl_sec = P0.ctrl_sec
    >>> ctrl_sec.header   # doctest: +NORMALIZE_WHITESPACE
    MenuSecHeader(name="Ctrl_Sec", isShown=0, selected=0, enabled=0, 
    isSecHead=1, page.name="P0", sec.name="Ctrl_Sec", keepAlive=0)
        
    >>> ctrl_sec.items
    []
    
    >>> ctrl_sec.exit_btn    # doctest: +NORMALIZE_WHITESPACE
    MenuItem(name="Done", isShown=1, selected=0, enabled=1, isSecHead=0,
     page.name="P0", sec.name="Ctrl_Sec", keepAlive=0)
    
    
 
    and is shown as:
    
      ------------------------
      Done    
   
    ---------------------------------------------
   
    >>> ctrl_sec.get_labels()
    ['Done']
    
    >>> ctrl_sec.header.isShown=1
     
    >>> ctrl_sec.header      # doctest: +NORMALIZE_WHITESPACE
    MenuSecHeader(name="Ctrl_Sec", isShown=1, selected=0, enabled=0, isSecHead=1, page.name="P0", sec.name="Ctrl_Sec", keepAlive=0)
    
    >>> ctrl_sec.get_labels()
    ['*______ Ctrl_Sec ______', 'Done']
   
    ---------------------------------------------    
    
    ### Page
    
    >>> P0= mysec.page
    >>> [sec.name for sec in P0.get_all_secs()]
    ['Tab_Sec', 'my_sec', 'radio_sec', 'Ctrl_Sec']

    ---------------------------------------------
    
    P0.user_secs:
    
    >>> [sec.name for sec in P0.user_secs ]
    ['my_sec', 'radio_sec']
   
    P0.secs contains all sections:
    
    >>> [sec.name for sec in P0.secs ]
    ['Tab_Sec', 'my_sec', 'radio_sec', 'Ctrl_Sec']
    
    P0.get_all_shown_secs() contains all visible secs:
    
    >>> [sec.name for sec in P0.secs if sec.isShown ]
    ['my_sec', 'radio_sec', 'Ctrl_Sec']
    
   
    ---------------------------------------------
        
    Note that the menu contains only one page, so 
    the tab_sec is not shown by <page>.get_labels() 
    (only user_secs and ctrl_sec are shown):
     
    >>> P0.tab_sec.isShown
    0
    >>> P0.get_labels()     # doctest: +NORMALIZE_WHITESPACE
    ['-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n',
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']
        
    You can force tab_sec to show:
    
    >>> P0.tab_sec.isShown =1
    >>> P0.get_labels()   # doctest: +NORMALIZE_WHITESPACE
    ['?P0', '-', 
    '-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n',
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']
    >>> P0.tab_sec.isShown=0
      
    ---------------------------------------------
    
    >>> P0.get_labels(hasline=1)   # doctest: +NORMALIZE_WHITESPACE
    ['-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n',
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']   

    
    >>> P0.get_sec('my_sec').isShowHeader = 1
    >>> P0.get_labels(hasline=1)   # doctest: +NORMALIZE_WHITESPACE
    ['-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n', 
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']  
       
    >>> [sec.isShowHeader for sec in P0.secs ]
    [0, 1, 1, 1]
        
    Hide section with .isShown = 0:
    
    >>> P0.get_sec('my_sec').isShown = 0
    >>> P0.get_labels()   # doctest: +NORMALIZE_WHITESPACE
    ['*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']
    >>> P0.get_sec('my_sec').isShown = 1
      
    ---------------------------------------------
    
    ## Add page: Adding a page adds a a tab (MenuItem instance) to tab_sec
    
    >>> P1= menu.add_page()  # Page name auto set to "P1"
    >>> [page.name for page in menu.pages]
    ['P0', 'P1']
    
    Note that tab_sec.isShown are auto set to 1 when menu.pages > 1:
    
    >>> [ p.tab_sec.isShown for p in menu.pages ]
    [1, 1]
  
    Lets check P0 after P1 is added:
    
    >>> [sec.name for sec in P0.secs if sec.isShown ]
    ['Tab_Sec', 'my_sec', 'radio_sec', 'Ctrl_Sec']
              
    Note that 'Tab_Sec' shows up here, but not before
    P1 is added. Lets check Tab_Sec
    
    >>> P0.tab_sec == P0.get_sec('Tab_Sec')
    True

    >>> P0.tab_sec.get_labels(hasline=1)
    ['?P0', 'P1', '-']
    
    >>> P1.tab_sec.get_labels(hasline=1)
    ['P0', '?P1', '-']
      
    >>> P1.tab_sec.get_labels(hasline=1)
    ['P0', '?P1', '-']
        
    >>> P0.get_labels(hasline=1) # doctest: +NORMALIZE_WHITESPACE
    ['?P0', 'P1', '-', 
     '-', '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n',
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']
    
    >>> P0.get_sec('my_sec').hasTopLine=0
    >>> P0.get_labels(hasline=1) # doctest: +NORMALIZE_WHITESPACE
    ['?P0', 'P1', '-', 
     '*______ my_sec ______', 'x0', '!y0', 'z0', 'a', 'b', 'c', 'm', 'n', 
     '-', '*______ radio_sec ______', 'A', '?C', 
     '-', '*______ Ctrl_Sec ______', 'Done']
      
    P1:
        
    >>> [sec.name for sec in P1.secs]
    ['Tab_Sec', 'Ctrl_Sec']

    >>> P1.tab_sec.get_labels(hasline=1)  
    ['P0', '?P1', '-']
        
    >>> P1.get_labels(hasline=1)
    ['P0', '?P1', '-', '-', 'Done']
    
    ----------------------------------------
    
    .add_radiobox and .add_checkbox:
    
    Both has "section name" as the first arg:
    
    
    
    ----------------------------------------

    
    <page>.add_radiobox and <page>.add_checkbox 
    
    >>> rbox= P1.add_radiobox( 'color', ('blue','green') )
    
    >>> [x.name for x in P1.user_secs ]
    ['color']
            
    >>> [x.name for x in P1.secs ]
    ['Tab_Sec', 'color', 'Ctrl_Sec']
         
    >>> P1.get_labels(hasline=1)  # doctest: +NORMALIZE_WHITESPACE
    ['P0', '?P1', '-', 
    '*______ color ______', 'blue', '?green', '-', 
    'Done']
     
    >>> P1.ctrl_sec.header.isShown
    0
    
    >>> chbox= P1.add_checkbox('Set_var', ('width','height'))
    
    >>> [x.name for x in P1.secs ]
    ['Tab_Sec', 'color', 'Set_var', 'Ctrl_Sec']

    >>> P1.get_labels(hasline=1)  # doctest: +NORMALIZE_WHITESPACE
    ['P0', '?P1', '-', 
    '*______ color ______', 'blue', '?green', 
    '*______ Set_var ______', 'width', 'height', '-', 
    'Done']
 
    ----------------------------------------
    
    >>> [x.name for x in P0.tab_sec.get_sel_items()]
    ['P0']
        
    >>> [ (sec.name, sec.typ, [x.name for x in sec.get_sel_items()]) 
    ... for sec in P0.user_secs]      # doctest: +NORMALIZE_WHITESPACE
    [('my_sec', 'checkbox', ['y0']), ('radio_sec', 'radiobox', ['C'])]
   
    ----------------------------------------
    
    .get_data():
    
    >>> P0.user_secs[0].get_data()
    {'a': 0, 'c': 0, 'b': 0, 'm': 0, 'n': 0, 'y0': 1, 'x0': 0, 'z0': 0}
        
    >>> P0.user_secs[1].get_data()
    'C'
        
    >>> P0.get_data()     # doctest: +NORMALIZE_WHITESPACE
    {'radio_sec': 'C', 
     'my_sec': {'a': 0, 'c': 0, 'b': 0, 'm': 0, 'n': 0, 
                'y0': 1, 'x0': 0, 'z0': 0}}
    
    >>> P1.get_data()     # doctest: +NORMALIZE_WHITESPACE
    {'color': 'green', 'Set_var': {'width': 0, 'height': 0}}        
      
    >>> menu.get_data()   # doctest: +NORMALIZE_WHITESPACE
    {'color': 'green', 
     'radio_sec': 'C', 
     'Set_var': {'width': 0, 'height': 0}, 
     'my_sec': {'a': 0, 'c': 0, 'b': 0, 'm': 0, 'n': 0, 
                'y0': 1, 'x0': 0, 'z0': 0}}
                              
    >>> menu.get_data_by_page()    # doctest: +NORMALIZE_WHITESPACE
    {'P0': {'radio_sec': 'C', 
            'my_sec': {'a': 0, 'c': 0, 'b': 0, 'm': 0, 'n': 0, 
                       'y0': 1, 'x0': 0, 'z0': 0}}, 
     'P1': {'color': 'green', 'Set_var': {'width': 0, 'height': 0}}}    
        
 
    .get_loadable_data() returns a data that can be loaded into another
    menu using menu.load(data):
    
  
    >>> data= menu.get_loadable_data()    
    >>> data     # doctest: +NORMALIZE_WHITESPACE
    [('P0', 
      [('my_sec', [('x0', 0), ('y0', 1), ('z0', 0), ('a', 0), 
                   ('b', 0), ('c', 0), ('m', 0), ('n', 0)]), 
       ('radio_sec', ['C', ['A', 'C']])]), 
    ('P1', 
      [('color', ['green', ['blue', 'green']]), 
       ('Set_var', [('width', 0), ('height', 0)])])]
 
      
    >>> menu2= Menu()
    >>> menu2.load( data )
    >>> menu2.get_loadable_data() == data
    True
    
    ---------------------------------------
    
    
    
  '''

                
#. doctest             
if __name__=='__main__':
  
  def menu_test():
  
    def test_docstr(func): 
        print('>>> doctesting: '+ func.__name__ +'() ')
        doctest.run_docstring_examples(func, globals())                                                
    
    menu= Menu(dlg_menu=None)
    #page= MenuPage(dlg_menu=None, parent=menu)
    #sec = MenuSec('Section', parent=page)
    #print( 'sec = ', sec)
    funcs = ( 
             'Menu Test'
            , Menu_test
            #, MenuSec.add_items        
            )
    def dumb():pass
    
    for f in funcs:
      if type(f)==str:
        print('====== %s ======'%f)
      else:
        test_docstr( f ) 

  menu_test() 
  #menu = Menu()

  '''
  If item is one char "-", separator will be shown.
  If item begins with "?" or "!" char, "radio checkmark" or "usual checkmark" will be shown.
  If item begins with "*" char, it will be disabled.
  Hotkeys must be preceded with "&" char.
  '''
   
  '''
    Menu Display 

    Checkbox:
    
      [name1]
          item1
        v item2
        v item3
          
      ==> Set menu.name1['item1']= False
              menu.name1['item2']= True
              menu.name1['item3']= True
      
        
    Radiobox:

      (name2)
          item1
        * item2
          item3
    
      ==> Set menu.name2= 'item2'
              

  '''
    
    
  '''
    menu
      page1
      page2

  ntType    ## 'CheckBox','RadioBox',ToggleBt','Item', 'Separator'
                , name      ## 
                , enabled=1
                , keepAlive=1
                , prefix= ""
                #, func= None
                #, linkObj = None
                #, linkDict= None
                ):
  '''