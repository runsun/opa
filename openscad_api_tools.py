'''
openscad_api_tools.py

This is an OpenSCAD api managing tool, designed to be used 
by SynWrite editor as a plug-in to display calltip to the 
output panel in SynWrite. It's imported by the plugin's 
__init__.py (in the same folder) in the following manner:

  import sys, os
  from sw import *

  this_dir = os.path.dirname(os.path.abspath(__file__))
  sys.path.insert(0, this_dir)
  import openscad_api_tools as opapi

  class Command:
    def on_key(self, ed_self, key, state):
       
      if state=="s" and key==57:  # when "(" is entered
      
        xy = ed.get_caret_xy()
        word_data = ed.get_word(xy[0], xy[1] )  #=>(offset, len, word)
        offset = word_data[0]
        unitname = word_data[2]               # func or module name
        api= opapi.get_api(unitname)
        app_log( LOG_ADD, api )

by: Runsun Pan 2016.01
'''

import collections, doctest

# Used in self.get_consistent_typename(t)
TYPE_MAP = ( 
  # For utype
   ( 's', ('s','str','Str','STR', 'STRING','string', 'String') )
  ,( 'i', ('i','int','Int','INT', 'integer', 'Integer','INTEGER') )
  ,( 'n', ('n','num','Num','NUM', 'number', 'Number','NUMBER') )
  ,( 'li', ('l','li','Li','LI', 'list','List','LIST') )
  ,( 'li2', ('l2','li2','Li2','LI2', 'list2','List2','LIST2') )
  ,( 'li3', ('l3','li3','Li3','LI3', 'list3','List3','LIST3') )
  ,( 'b', ('b','B','bool','Bool','BOOL','boolean','Boolean','BOOLEAN') )
  # For cat
  ,( 'func', ('f','func','Func','FUNC','function','Function', 'FUNCTION') )
  ,( 'mod',('m','mod','MOD','Mod','module','Module','MODULE'))
  ,( 'var',('v','var','Var','VAR','variable','Variable','VARIABLE'))
  )

#. Utils

def ntApi_to_Unit(ntApi):
  ''' Convert an ntApi to Unit instance. 
  
      A ntApi is an api data in the named-tuple format:
  
      api(typ='module', name='mod', rng=(5, 38), rel_rng=(0, 5, 0, 38)
         , txt='module mod(a,b=2,c=g(d),d=g(2,3))'
         , argtxt='a,b=2,c=g(d),d=g(2,3)'
         , argnames=['a', 'b', 'c', 'd']
         , reqargs=['a']
         , optargs=[('b', '2'), ('c', 'g(d)'), ('d', 'g(2,3)')]
     )
          
      The function, parseTopApi(s) in re_utils.py returns a list of ntApi's.
      
  '''
  if ntApi.typ == 'function':
    rtn = Func( name= ntApi.name
               , args= ( [ Var(name=a) for a in ntApi.reqargs] + 
                         [ Var(name=a[0], default=a[1]) for a in ntApi.optargs] )
               )          
  elif ntApi.typ == 'module':
    rtn = Mod( name= ntApi.name
               , args= ( [ Var(name=a) for a in ntApi.reqargs] + 
                         [ Var(name=a[0], default=a[1]) for a in ntApi.optargs] )
               )          
  return rtn
  
  
def get_args_to_be_entered( expected_ntApi, entered_ntApi ):
  '''
    Given an expected api, check if the api(args) currently 
    editing (where the caret is) to see what else arg haven't been
    entered. This is needed for the feature 'Tab-Argument' when 
    user hit TAB inside arguments. 
    
    ntApi example (returned by ru.parseTopApi):
    
         api(typ='module', name='mod', rng=(5, 33), rel_rng=(0, 5, 0, 33), 
   txt='module mod(a,b=2,c,d=g(2,3))', argtxt='a,b=2,c,d=g(2,3)', 
   argnames=['a', 'c', 'b', 'd'], reqargs=['a', 'c'], 
   optargs=[('b', '2'), ('d', 'g(2,3)')])
    
    Note: parseTopApi is designed to parse a "func/mod definition", which is 
          prefixed with either 'function' or 'module'. So if we make use of 
          parseTopApi to parse "func/mod USAGE", not only the text to be parsed 
          has to be prefixed with "function" or "module" manually, but also, what
          is obtained as "optional argument" doesn't necessarily optional.
          For the above example, if user uses:
          
            mod( a=3 ) 
            
          it'd be parsed by parseTopApi to have an optional ('a','3'), but
          in fact 'a' is defined as required in the definition:
          "module mod(a,b=2 ...)" 
          
          So this has to be taken care in this function. 
            
    return a list of two items: [ req_argnames, opt_args ]
    Example: [ ['a','b'], [('c', 'g(d)'), ('d', 'g(2,3)')] ]
    
    >>> import re_utils as ru 
    
    >>> expected_ntApi= ru.ntApi(typ='module', name='palm', rng=(348, 368)
    ... , rel_rng=(14, 0, 14, 20), txt='module palm(a,b,c=0)'
    ... , argtxt='a,b,c=0', argnames=['a', 'b', 'c'], reqargs=['a', 'b']
    ... , optargs=[('c', '0')])
    
    >>> entered_ntApi = ru.ntApi(typ='function', name='palm', rng=(0, 16) 
    ... , rel_rng=(0, 0, 0, 16), txt='function palm( )', argtxt=' '
    ... , argnames=[], reqargs=[], optargs=[])
    
    >>> get_args_to_be_entered( expected_ntApi, entered_ntApi )
    [['a', 'b'], [('c', '0')]]
        
  '''
  import re_utils as ru
  need_args= expected_ntApi.argnames
  need_req = expected_ntApi.reqargs
  need_opt = expected_ntApi.optargs
  #print( '### In get_args_to_be_entered(): need_args: ', need_args )  
  #print( '### In get_args_to_be_entered(): need_req: ', need_req )  
  #print( '### In get_args_to_be_entered(): need_opt: ', need_opt )  
  
  ## when user enters arg with an assignment (like a=3 but not 3), 
  ## del the corresponding names (like "a") from need_args 
  for name,val in entered_ntApi.optargs:
    if name in need_args:
      del need_args[ need_args.index(name) ]
  
  #print( '### need_args: ', need_args )  
  need_args = need_args[ len(entered_ntApi.reqargs): ]
  #print( '### need_args: ', need_args )  
    
  need_req = [ x for x in expected_ntApi.reqargs if x in need_args ]
  need_opt = [ x for x in expected_ntApi.optargs if x[0] in need_args ]
  #print( '### need_args: ', need_args )  
  #print( '### need_req: ', need_req )  
  #print( '### need_opt: ', need_opt )  
   
  return [need_req, need_opt]   
  
#. Class
  


#  rtn={}
#  for k,v in kargs: rtn[k]= v(name=k)
#  return rtn 
  
class Unit(dict):
  ''' Unit is a dict wrapper. It could be for functions, modules
      or variables. Variables include the function's or module's
      argument. 
  
        size = Unit( name='size', cat='var', optional_args) 
      
      Arguments 'name' and 'cat' are required. 
      
      Update data with either way:
        
        size.name = 'new name'
        size['name'] = 'new name'
        
      Clone:
      
        size2 = size()
          
      Clone with new data :
      
        size3 = size(default=2)  
        
  '''
  def __init__( self
              , name = ''
              , cat  = ''# var|func|mod
              , utype   = None  # "#","i","f","s","l" or "#|i|f", etc  
              , optional= True  # True|False
              , default = None 
              , tags    = []
              , args    = [] # for mod/func, list of Unit instances 
              , ret     = None
              , ver     = None
              , rel     = []  # related, list of str
              , hint    = '' # Short doc, make it one-liner whenever possible
              , doc     = ''  # long doc
              ):
      loc = locals()
      for k in loc.keys(): 
        if k!='self': self.__dict__[k] = loc[k]
  
  def __setitem__(self,k,v): 
    self.__dict__[k]= self.get_consistent_typename(k,v)
    
  def __getitem__(self,k): return self.__dict__[k]

  def __setattr__(self,k,v): 
    self.__dict__[k]= self.get_consistent_typename(k,v)
  
  def __getattr__(self,k):
    if k=='attr_names':
      return ['name', 'cat', 'utype', 'tags', 'optional', 'default'
                      , 'hint', 'doc','args','ret', 'ver', 'rel']
    else:
      return self.__dict__[k]


  def __str__(self):    
    return ('{'+ ', '.join( [ '"%s": %s'%(k, 
                              type(self.__dict__[k])==str and 
                              '"'+self.__dict__[k]+'"' or self.__dict__[k] )
                             for k in self.attr_names ] ) 
            +'}')   
             #for k in ['name', 'cat', 'utype', 'tags', 'optional', 'default'
             #         , 'hint', 'doc','args','ret', 'ver', 'rel'] ] ) +'}'
                
  def __repr__(self): return self.__str__()           
         
  def get_arg_names(self):
    return self.get_required_arg_names()+ self.get_optional_arg_names()

  def to_ntApi(self):
    '''
      Convert .args (which is a list of Unit instances) to 
      a list of ntApi. This is for get_args_to_be_entered()

      ntApi example ( usually returned by re_utils.parseTopApi): 
      
      api(typ='module', name='mod', rng=(5, 33), rel_rng=(0, 5, 0, 33), 
          txt='module mod(a,b=2,c,d=g(2,3))', argtxt='a,b=2,c,d=g(2,3)', 
          argnames=['a', 'c', 'b', 'd'], reqargs=['a', 'c'], 
          optargs=[('b', '2'), ('d', 'g(2,3)')])
     
      3016.3.26
    '''
    rtn= collections.namedtuple( 'api',
          ['typ', 'name','rng','rel_rng','txt'
          ,'argtxt', 'argnames', 'reqargs','optargs' ])( 
              self.cat
            , self.name
            , None # nt.rng can't be obtained from here
            , None # nt.rel_rng can't be obtained from here
            , str(self.cat) + ' ' + self.get_api_str() 
            , self.get_arg_str()
            , [x.name for x in self.args]
            , [x.name for x in self.args if not x.optional]
            , [ (x.name,x.default) for x in self.args if x.optional]
            )
    return rtn       
    

    
  def get_required_arg_names(self):
    return [x["name"] for x in self.args if not x.optional ]

  def get_optional_arg_names(self):
    return [x["name"] for x in self.args if x.optional ]
                
  def get_arg_str(self): # for cat="var"
    '''
        sphere= Unit( name='sphere'
                    , args=[ common_units('r')(default=1)
                           , common_units('d')
                           ]
                    )
        sphere.args[0].get_arg_str()
        ==> "r:num= 1"
    '''    
    s = '%(argname)s:%(type)s%(df)s'
    df = self.default
    arg_str= s%( { "argname": self.name #arg.get("name")
               , "type" : self.utype #arg.get("type")
               , "df" : df!=None and ("=" + str(df)) or ""
               }
             )
    return arg_str          
        
  def get_arg_by_name(self, argname):
      return [ arg for arg in self.args if arg.name==argname ][0]
      
  def get_consistent_typename(self,k,v):
      if (k=='utype' or k=='cat') and v!=None:
        if len(v.split('|')) > 1:
          return '|'.join(
                  [ vv and self.get_consistent_typename(k,vv) or vv
                    for vv in v.split('|')
                    
                  ] )   
        for kk,vs in TYPE_MAP:
          if v in vs:
            return kk
      else:
        return v  
          
  def get_args_str(self): # for cat="func"|"mod"
    ''' 
        sphere= Unit( name='sphere'
                    , args=[ common_units('r')(default=1)
                           , common_units('d')
                           ]
                    )
        sphere.get_args()          
        ==> "r:num= 1, d:num"
        
        sphere.args[0].as_arg_str()
        ==> "r:num= 1"
        
        tests:
        func1= Func('func1')
        print('func1= ', func1)
        print('func2= ', func1(name='func2'))
        print('func3= ', func1(name='func2')(name='func3'))
    '''    

    args= self.args
    return ", ".join( [ arg.get_arg_str() for arg in args ] )

  def get_api_str(self):
    s = "%(unitname)s( %(args)s )%(ret)s"
    rtn = self.cat=="func" and ("=> " + str(self.ret)) or ""
    return s%( { "unitname": self.name
               , "args": self.get_args_str()
               , "ret": rtn
               }
             )  
  
  def __call__(self, name='', cat='', **kargs):
    '''
        Make the instanse callable to create a clone with new data:
        
         r = Unit(name="r",default=1)
         r2 = r(name="r2",default=2)
         r3 = r(name="r3")(default=3)
         
       More tests:
        func1= Func('func1', tags=["xxx"])
        func2 = func1(name='func2')
        func3 = func2('func3')
        func4 = Func('func4-0')(ver=1)(name='func4-1')
        print('\n>>> func1= ', func1)
        print('\n>>> func2= ', func2)
        print('\n>>> func3= ', func3)
        print('\n>>> func1= ', func1)
        print('\n>>> func4= ', func4)
        func4.name= 'func4-2'
        print('\n>>> func4= ', func4)
        func4['name']= 'func4-3'
        print('\n>>> func4= ', func4)  
         
    '''   
    #    print( '\n----------\n'
    #        , 'in __call__, self.name= ', self.name
    #        , ', self.tags= ', self.tags
    #        , '\n' )
    #print('in __call__, self.name="%s", name="%s"'%(self.name,name)) 
    #loc = locals()
    #del(loc["self"])   # all args given except 'self'
    if name!="": self.name=name
    if cat!="": self.cat = cat       
    
    u = Unit( name, cat ) # Create a new Unit
    
    for k in self.attr_names:   # Copy all values from self 
      u[k] = self.__dict__[k]
      
    for k in kargs.keys(): # Update new data from arguments
      if kargs[k]!=None:
        u[k] = kargs[k]

    #print( 'u after update = ', u)
    return u              

def Var( name='', **kargs ): 
    #print('  In Var, name = ', name)
  
    u= Unit(name,cat='')
    for k,v in kargs.items():
        #print('>>> in Var, k= %s, v= %s'%(k,v))
        u[k] = v
    u.cat= 'var'
    u.args= None
    u.ret = None 
    return u

def Num( name='', **kargs ): return Var(name, **kargs)(utype='num')
def Integer( name='', **kargs): return Var(name, **kargs)(utype='int')
def Bool( name='', **kargs ): return Var(name, **kargs)(utype='bool')
def Li( name='', **kargs ): 
  #print('In Li, name = ', name)
  return Var(name, **kargs)(utype='list')
     
def Func( name='', **kargs ): 
 
    u= Unit(name,cat='')
    for k,v in kargs.items():
        u[k] = v
    u.cat= 'func'
    u.optional = None
    u.default = None
    return u

def Mod( name='', **kargs ):  

    u= Unit(name,cat='')
    for k,v in kargs.items():
        u[k] = v
    u.cat= 'mod'
    u.utype= None
    u.optional = None
    u.default = None
    u.ret = None
    return u
  

#
# openscad_api is defined in openscad_api_data.py, which 
# requires the tools above, so we import it only after 
# the above tools are defined
#

#. Doctest
  
def re_test():
  ''' Test the re and token using doctest
  ''' 
  def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                
  
  funcs = ( 
          get_args_to_be_entered
          ,
          )
  def dumb():pass
  for f in funcs: 
    if type(f)== type(dumb):
      test_docstr( f ) 
    else:
      print('====== %s ======'%f)
      
if __name__=='__main__':
    re_test()