'''
Opa features:

1) Tab-Argument
2) In-doc api for user functions (include files in include/use)
3) Pop-up api menu ( handled by OpenscadCommandMenu )

'''

from sw import *
import os, sys, pprint, time, re, inspect
this_dir = os.path.dirname(os.path.abspath(__file__))
#folder_Settings = sw.app_ini_dir()  # => "...\SynWrite\Setting"
sys.path.insert(0, this_dir)
import re_utils as ru
import opa 
import sw_menu
import openscad as opcad
import openscad_api_data as opapi 

'''
Tab-Argument:

  If the caret is inside a call arg ( name(...) ), hitting tab will 
  show the arg name one by one. 
  
  This requires combination of several complex operations:
  
  A. Api needs to be pre-defined, including those for built-in fmod
     (openscad_api) and the in-doc api that's been defined by users (indoc_api).
     The global var, api, contains both (handled by set_api).
     
  B. Need to know if the caret is inside an arg. This includes situations:
  
    B1. When user click into the arg of an already-typed calling statement. 
        (handled by on_click)
    B2. When user type "fmod(" --- means user is calling a fmod. 
        (handled by cooperation of on_key and on_caret_move (and the 
         on_left_paren))  

    In both cases, it also needs to avoid a fmod definition: "function func("
    or "module mod("
    
  C. Need to get the arg names already entered and compare it to the api
      to see what are left to be entered (handled by get_args_to_be_entered). 

'''

#. Vars

isToggling = False 
selected = None
#idu = InDocUi()
keyhist=[]

#IS_ON = 1
#IS_AUTOSAVE = 0

IS_CHECK_EVENT_ORDER= 0 ## This is a development option. Each event handler
                        ## that is to be checked need to have this:
                        ##    check_event_handler_order()
                        ## When set it to 1, event handlers to be checked
                        ## will print out stuff
                        
IS_TAB_ARGUMENT=1 ## Turn on the tab-argument feature                        
IS_CLICK_CHANGE=1
IS_TOGGLE_BLOCK=1

IS_AUTO_SAVE=1
                        
nEventHandler=0    ## Used for checking event order

in_call = ""   ## Set to <funcname> or <modname> if i is inside a func/mod 
              ## arg like within the () of : func(a,b) 
              ## or if a "(" is typed after func like func( 
skey     = ""
argnames = [] ## Used in on_caret_move for Tab-Arguments

prev_xy =(0,0)## Keep trace of previous i. This allows for caret to
prev_i  = 0   ## jump back to its previous pos. For example, when
              ## let i = 5 and hit "a":
              ##   in on_key: set prev_i = ed.get_caret_pos() = 5
              ##   in on_caret_move: i = ed.get_caret_pos() = 6
              ## We can set (in on_caret_move) to jump back to 5 and 
              ## delete doc[5:6], then "a" will never be entered.

indoc_api = {} ## = the api of func/mod defined in the current doc
                ## Reset in on_click and on_change             
imported_api= {}
 
api = {} ## Combination of openscad_api, imported_api and indoc_api
          ## combine them in set_api

## The following two are set in on_open
filename = '' ## name of currently editing file 
filepath = '' ## paht of currently editing file 
external_files=[]

#DOUBLE_CLICK_SEC = 2  # msec to recognize a double click
#clicktime = time.clock()  ## init the clock for use in double-click 
prev_sel_rng=(None,None)
    
prev_word = (None,None,None)

#. Utils


      
def check_event_handler_order(handlerName=None):
    '''
      Place this inside a couple of event handlers
      to see the order of these event handlers are called
      and see how sw handles i and text change
    '''
    if IS_CHECK_EVENT_ORDER:
      global nEventHandler, skey
      nEventHandler= nEventHandler+1
      
      if not handlerName:
        handlerName = inspect.stack()[1][3]  # this get the name of caller of 
                                             # this function      
      i = ed.get_caret_pos()
      doc= ed.get_text_all() 
      if doc and i>=0 and i< len(doc):
        print('%s) %s: skey="%s", prev3="%s", i=%s, doc[%s]="%s"'%(
            nEventHandler, handlerName.ljust( len('on_caret_move')+1)
            ,skey, doc[ i-3:i ], i,i,doc[i]) )
     
  
 
def doc_parts(keyEntered="", doc = ed.get_text_all()):
  ''' return 3-tuple: (doc_b4_i, doc_at_i, doc_after_i) '''
  i = ed.get_caret_pos()
  
  if i>=len(doc):
    return (doc, keyEntered, None)
  else:
    #print( 'i, len(doc) = %s, %s'%(i,len(doc)))
    #print('doc[i]= "%s"'%doc[i-1])
    return (doc[:i], doc[i], i<len(doc)-1 and doc[i+1:] or None)
   
    
def handle_tab_argument(ed_self, doc, i):
  ''' 
      Called by on_caret_move() to carry out the tab-argument feature
  '''
  
  global skey, in_call, prev_i
  #print( '\n>>> handle_tab_argument() ...\n' )
  ntCall = ru.ntCallArgs_at_i(doc,i)
           ## ntCall example:
           ## [Blk(typ='Call',rng=(5,16),rel_rng=(0,5,0,16),txt='g( b, h(d))'), 
           ## Blk(typ='Call',rng=(11,15),rel_rng=(0,11,0,15),txt='h(d)')]
  
  if ntCall:
  
    ntCall= ntCall[-1] ## The deepest call. For example, f(3, g(2)),
                       ## If caret is btw g( and 2, g is the func that's
                       ## call, but not f
                    
       
    entered_ntApi= ru.parseTopApi( 'function '+ ntCall.txt )
             ## Check how many args are already typed when TAB is entered.   
             ## parseTopApi is originally intended to parse func/mod defs,
             ## so it requires "function f(...)" or "module m(...)".
             ## So we need to prefix either "function" or "module".
             ## This returns a list of ntApi :
             ## [
             ##  api(typ='module',name='mod',rng=(5,38), rel_rng=(0,5,0,38), 
             ##    txt='module mod(a,b=2,c=g(d),d=g(2,3))', 
             ##    argtxt='a,b=2,c=g(d),d=g(2,3)', 
             ##    argnames=['a', 'b', 'c', 'd'], reqargs=['a'], 
             ##    optargs=[('b', '2'), ('c', 'g(d)'), ('d', 'g(2,3)')]
             ##    ) 
             ## ]
             
    if entered_ntApi: entered_ntApi = entered_ntApi[-1]  
    #print('>>> entered_ntApi: \n', entered_ntApi)       
    
    expected_ntApi = api[ in_call ]      
    #argnames_expected = api[ in_call ].argnames
    #print( '>>> argnames_expected: %s'%(argnames_expected))
    #print( '>>> api[ in_call ]: ', api[in_call])
    #print( '>>> expected_ntApi: ', expected_ntApi)        
    args_to_be_entered= opapi.get_args_to_be_entered( expected_ntApi, entered_ntApi )
        ##  >>> args_to_be_entered: [['b'], [('c', '0')]]
        
    ## print( '>>> args_to_be_entered:\n', args_to_be_entered )
    
    ##
    ## Need to re-place caret. The TAB is just to trigger Tab-argument
    ## feature inside args, so need to del it and move caret backword:
    ## 
    skey=''
    ed_self.replace( prev_i,i-prev_i, "")
    ed_self.set_caret_pos( prev_i)

    ## At least one of req args or opt_args are still needed: 
    ##
    if args_to_be_entered[0] or args_to_be_entered[1]: 
    
      last_char= doc[:prev_i].strip()[-1]
      camma= last_char not in ('(',',') and "," or ''
      if args_to_be_entered[0]:
        ed_self.insert( camma+ args_to_be_entered[0][0]+'=')
        
      elif args_to_be_entered[1]:
        ed_self.insert( camma+ '%s=%s'%(args_to_be_entered[1][0]))
        #args_to_be_entered= args_to_be_entered and args_to_be_entered[1:] or []
    
    ## Nothing more is needed. So a TAB moves caret beyond ');' or ')'
    else:
      pdoc = doc[i:].strip()
      if pdoc.startswith(');'):
        ed_self.set_caret_pos( i + pdoc.index(';'))
      elif pdoc.startswith(')'):
        ed_self.set_caret_pos( i + pdoc.index(')') )          
       
   #in_call = ''

def handle_toggle_block(ed):
    
    prev_selc,prev_sellen = ed.get_sel()
    prev_selected = ed.get_text_all()[prev_selc:prev_selc+prev_sellen]
    #print('\handle_toggle_block, selected: "%s"'%(prev_selected )) #ed.get_sel()) )
    
    ## Word at moust click
    w_bi, w_len, word_at_i = ed.get_word(*ed.get_caret_xy())  # (i, len, word)
    doc = ed.get_text_all()
    #print( 'word at i= "%s" '%( doc[ w_bi:w_bi+w_len ] ))
    isClickOnPrevSel = w_len and (w_bi,w_len)==(prev_selc,prev_sellen)
    #print( 'Current word == prev selection ? ', isClickOnPrevSel )
    
    
    if isClickOnPrevSel:  
         
         opa.toggleBlock( ed, dlg_menu, MENU_STD
                    , w_bi, w_len, word_at_i
                   # , isAutoSave= isAutoSave 
                   # , file_save_func= file_save
                    )    
    
def keyToSwKey(keychr):
  i = ord(keychr)
  return i
  
KEY_IDX_MAP= { i: chr(i) for i in range(260) }

def opa_msg_box(s, msgid= MSG_CONFIRM_Q ):
  s = ('Opa>\n'
      +'----------------------------------------------------\n\n'
      + s
      )
  return msg_box( msgid, s )

  
def parseImportedTopApi(s, file_path=None, importCmdPtns=None):
  '''
    Given doc string s, extract filenames in include <...> or use <...>,
    then go into them one by one to parse api recursively (means secondary
    or more levels of include/use are also taken care of). Save the results
    to global var, imported_api, as a dict, in which key = fmod name and
    value is a ntApi (returned by parseTopApi; see definition in re_utils.ntApi)
    
    Global:     
    imported_api
    filename = name of currently editing file 
    filepath = path of currently editing file
    external_files    
  '''
  #print('\n>>> parseImportedTopApi(), file_path="%s", filepath="%s"'%(file_path, filepath))
  global imported_api, external_files
  
  ## filepath is global; file_path is not 
  file_path = file_path and file_path or filepath
  
  if not file_path in external_files:
  
    external_files.append( file_path )
    
    if importCmdPtns==None:
      p= ru._1ms()+ ru._nc('\<')+'(.*)'+ru._nc('\>$')
      importCmdPtns= [ ru._or('include','use')+p ] 
    
    ptns = [ re.compile(x) for x in importCmdPtns ]
    import_file_names = []
     
    for line in s.split('\n'):
      for p in ptns:
        x= p.match( line )
        if x: import_file_names.append( x.groups()[1] )
       
    #print( 'import_file_names= ', import_file_names)
    
    for f in import_file_names:
      
      path = os.path.join(file_path, f)   
      #print( '... getting doc from "%s"'%(path))
      impdoc = (open( path, 'r' ).read())
      dicApi = { nt.name:nt 
                  for nt in ru.parseTopApi( impdoc ) }
      imported_api.update( dicApi )   
      #print( 'imported_api.keys()=', imported_api.keys() )
      
      ## Check impdoc to see if it has include/use 
      parseImportedTopApi( impdoc, file_path=file_path, importCmdPtns=None)         
              
        
def partialdoc(include=False):
  ''' return partial doc till i. If include, include char at i'''
  a,b,c= doc_parts()
  return a + (include and b or "")
  
def partialdoc2(include=False):
  ''' return partial doc starting from i+1. If include, include char at i'''
  a,b,c= doc_parts()
  return (include and b or "")+c
    
def set_api():
  '''
    Set global var, api and indoc_api
    
    api is a dict contain ntApis. The value of each item is a ntApi
    with the following signature:
    
      api(typ='module', name='mod', rng=(5, 33), rel_rng=(0, 5, 0, 33), 
      txt='module mod(a,b=2,c,d=g(2,3))', argtxt='a,b=2,c,d=g(2,3)', 
      argnames=['a', 'c', 'b', 'd'], reqargs=['a', 'c'], 
      optargs=[('b', '2'), ('d', 'g(2,3)')])
        
    and is created by:
    
      collections.namedtuple( 'api',
          ['typ', 'name','rng','rel_rng','txt'
          ,'argtxt', 'argnames', 'reqargs','optargs' ])( 
              typ, name, ... 
            )
         
  '''    
  global indoc_api, api
  #print('>>> in set_api, opapi.openscad_api.keys()=', opapi.openscad_api.keys())
  oapi = { name:unit.to_ntApi() 
            for name, unit in opapi.openscad_api.items() } 
  indoc_api = { nt.name:nt 
                    for nt in ru.parseTopApi( ed.get_text_all() ) }
  #print('>>> in set_api, oapi.keys()=', oapi.keys())
  
  api= {}
  api.update( oapi)
  api.update( indoc_api )
  api.update( imported_api )
  
  #print('\n>>> In set_api, api.keys= ', api.keys())
  
    
def set_api_old():
  '''
    A dict containing entire api. The value of each item is an instance 
    of openscad_api.tools.Unit class. Signature:
      ( name
      , cat # var|func|mod
      , utype   = None  # "#","i","f","s","l" or "#|i|f", etc  
      , optional= True  # True|False
      , default = None 
      , tags    = []
      , args    = [] # for mod/func, list of Unit instances 
      , ret     = None
      , ver     = None
      , rel     = []  # related, list of str
      , hint    = '' # Short doc, make it one-liner whenever possible
      , doc     = ''  # long doc
      )
      
    where each arg in args is a Unit of cat="var".
      
    To get the expected arg names of a func:
    
      [ unit.name for unit in api[ func_name ].args ]
         
  '''    
  global indoc_api, api 
  indoc_api = dict([ (x.name, opapi.ntApi_to_Unit(x)) 
                    for x in ru.parseTopApi( ed.get_text_all() )] )
  api= {}
  api.update( opapi.openscad_api)
  api.update( indoc_api )
 
def set_in_call(ed_self, doc, i):# Call this only when "(" key is pressed
    '''
      Call this when user type any identifier + '(', like:
        cube(
      and return "cube"  
    '''
    global in_call
#    pdoc = partialdoc(include=True).split('\n')[-1]+'('
#    ptn = re.compile( '('+ru.RE_ID + ')\s*\(?' )
#    m = ptn.search( pdoc )
#    #print('in is_in_open_arg, pdoc="%s", ptn="%s", m='%(pdoc,ptn.pattern),m)
#    if m:
#      #print( 'In open arg of ', m.groups())
#      fname = m.groups()[0]
#      in_call= fname 
#      #return fname
      
    pdoc = doc[:i]  
    in_call = ''
  
    p_def= ( ru._nc(ru._or('function','module')+ ru._1ms())
              + ru.RE_ID
              +ru._nc(ru._0ms()+'\($') )
              ## p_def is pattern to match partial func/mod definition :
              ##   "module mod("  or "function funcname("
    isdef = re.compile( p_def ).search( pdoc )
    print('i=%s, pdoc="...%s", isdef=%s'%(i, pdoc[-10:],isdef))
    if not isdef:
      in_call = re.compile( ru.RE_ID+ru._nc(ru._0ms()+'\($')).search( pdoc )
      in_call = in_call and in_call.group()[:-1] or None  
                ## [:-1] is to remove "(" 
      if in_call:
        
        print( 'Entering the %s() calling arg'%in_call ) 
        skey=''  # Null the skey BEFORE leaving this func
        ed_self.insert(");")
        ed_self.set_caret_pos(i)
        
    return in_call     

       
#. Classes  
   
class OpenscadCommandMenu(sw_menu.Menu):

  def __init__(self):
    sw_menu.Menu.__init__(self, dlg_menu)
    #print( '>>> In OpenscadCommandMenu.__init__, .pages=', self.pages)
    self.setup()
    
  def setup(self):
  
    user_funcs = list(indoc_api.keys()) + list(imported_api.keys())
    user_funcs.sort()
    
#    data= [
#      ('Object', 
#        [ ('obj', ['cube', opcad.BUILTIN_OBJ] )
#        #, ('func_hint', None)
#        ]
#      ),
#      ('Action',
#        [ ('action', ['color', opcad.BUILTIN_ACTION])
#        ]
#      ),
#      ('Function',
#        [ ('func', ['pow', opcad.BUILTIN_CORE])
#        ]
#      ),
#      ('User', [('user', ['palm', ['Axes', 'palm']])])
#    ]
#    self.load(data)
    
    data = [('Object',
  [('obj',
    ['text',
     ['circle',
      'cube',
      'cylinder',
      'polygon',
      'polyhedron',
      'sphere',
      'square',
      'text']]),
   ('func_hint', None)]),
 ('Action',
  [('action',
    ['union',
     ['color',
      'difference',
      'hull',
      'intersection',
      'intersection_for',
      'linear_extrude',
      'minkowski',
      'mirror',
      'multmatrix',
      'offset',
      'render',
      'resize',
      'rotate',
      'rotate_extrude',
      'scale',
      'translate',
      'union']])]),
 ('Function',
  [('func',
    ['version_num',
     ['$children',
      'abs',
      'acos',
      'asin',
      'atan',
      'atan2',
      'ceil',
      'children',
      'chr',
      'concat',
      'cos',
      'cross',
      'echo',
      'exp',
      'floor',
      'len',
      'let',
      'ln',
      'log',
      'lookup',
      'max',
      'min',
      'norm',
      'parent_module',
      'pow',
      'rands',
      'round',
      'search',
      'sign',
      'sin',
      'sqrt',
      'str',
      'tan',
      'version',
      'version_num']])]),
 ('User', [('user', ['palm', ['Axes', 'palm']])])]
    
    #self.load(data)
    
    self.pages[0].name = 'Object'
    self.pages[0].tab_sec.items[0].isShownHeader = 1
    self.pages[0].secs[-1].isShownHeader=1
    self.add_radiobox('obj'
          , opcad.BUILTIN_OBJ_W_HOTKEY, isShowHeader=0)    
    self.pages[0].add_section( 'func_hint', typ='one-item'
    , isShowHeader=1, hasTopLine=1)
    
    self.add_page('Action').add_radiobox('action'
          , opcad.BUILTIN_ACTION_W_HOTKEY).isShowHeader=0
    self.add_page('Function').add_radiobox('func'
          , opcad.BUILTIN_CORE_W_HOTKEY).isShowHeader=0
    
    #user_funcs = list(indoc_api.keys()) + list(imported_api.keys())
    #user_funcs.sort()
    
    self.add_page('User').add_radiobox('user'
          , user_funcs).isShowHeader=0 
          
          
    #print('loadable data=\n', pprint.pprint( self.get_loadable_data()))
    
  def on_sel(self, item):
    #print('>>> on_sel, menu.page="%s", .section="%s", .item="%s"'%(
    #      self.page.name, self.sec.name,self.item.name))

    #print('>>> on_click, menu.page="%s", .sel_section="%s", .sel_item="%s"'%(self.sel_page.name, self.sel_sec.name,self.sel_item.name))
    
    sec_func_hint= self.pages[0].get_sec('func_hint')
    #print('__init__, .on_sel, self.item = ',self.item)  
    
    return 
    
    api_text= opapi.get_openscad_api_str(self.item.name.replace('&',''))
    #if self.item != self.get_exit_item():
    #  sec_func_hint.items= sec_func_hint.items[:1]
    if api_text:
      #print( 'opapi.get_openscad_api_str(menu.item.name) ="%s"'% sel_func_hint)
      
      #print('sec_func_hint.items= %s'%str(sec_func_hint.items))
      sec_func_hint.add_item( api_text, selected=1 )
    #elif self.sec.name=='func_hint':
    #  sec_func_hint.items= sec_func_hint.items[:1]
            
  def on_exit(self):
    
    global in_call 
    
    #print('>>> on_exit:\n selected on each page:', self.get_data() )
    #print('... menu.page="%s", .section="%s", .item="%s"'%(
    #  self.page, self.sec,self.item))  
    
    print('... menu.page="%s", .section="%s", .item="%s"'%(
      self.page.name, self.sec.name,self.item.name))  
    
    sec_fhint_items= self.pages[0].get_sec('func_hint').items 
    print('... fhint items len= %s'%len(sec_fhint_items))
    
    
    ibeg, sel_len = ed.get_sel()
    iend = ibeg+sel_len
    doc = ed.get_text_all()
    sel_text= doc[ibeg:iend]
    sel_lines = sel_text.split('\n')
    print('... Selected text: from %s to %s= "%s"'%(ibeg, iend, sel_text))
    
    itemname = self.item.name.replace('&','')
    
    if itemname: ## When a valid itemname is returned by the menu
    
      ## Check if the selection contains a call (a call like, func(...)). 
      ## In this case, we put the itemname like this:
      ##     itemname(){
      ##        selection_line0
      ##        selection_line1
      ##        selection_line2
      ##     }
        
      has_call_in_sel= sel_len>0 and ru.findTopBlks( sel_text, name='call'
                        , ends=(ru.RE_ID+ru._0ms()+'\(', '\)') 
                        , skip_ends=('\(','\)')
                        )
      if sel_len>0 and has_call_in_sel:
      
        if self.sec.name=='action':
          tabsize= opa.INDENT_SIZE 
          print('... action "%s" selected '%itemname )                    
          ed.set_caret_pos( iend ) 
          ed.set_caret_pos( ibeg )
          ed.replace( ibeg, sel_len
                     , tabsize*' '+('\n'+' '*tabsize).join( sel_lines ) )
          iend = iend + tabsize*len(sel_lines) 
          ed.set_caret_pos( iend )
          ed.insert( '\n}')
          ed.set_caret_pos( ibeg)
          ed.insert( itemname+'(){\n')
          ed.set_caret_pos( ed.get_caret_pos()-3 )
        
      else:
        ## Either there's no selection, or a selection that doesn't 
        ## contain a call (a call like, func(...)). We put the itemname
        ## in front of the caret (or the selection, if any) 
        
        ## Collapse selection, make sure it's in the correct ibeg 
        ed.set_caret_pos( iend ) 
        ed.set_caret_pos( ibeg ) 
        
        ed.insert( itemname+'() ')
        ed.set_caret_pos( ed.get_caret_pos()-2 )
      
      in_call = itemname   
        
      print( self.sec.name + ': '+ 
        str((itemname in api) and api[ itemname ].txt or (itemname+' not found in api')))
    
class OpaMenu(sw_menu.Menu):
  def __init__(self):
    sw_menu.Menu.__init__(self, dlg_menu)
    self.setup()
    
  def setup(self):
  
    self.pages[0].name = 'Opa_Settings'
    
    item_names=('IS_TAB_ARGUMENT', 'IS_CHECK_EVENT_ORDER')
    
    self.add_checkbox( sec_name='Opa Settings'
                        , item_names= item_names
                          )
              
    # Load data from the doc            
    for name in item_names:
      self.pages[0].get_sec('Opa Settings'
        ).get_item( name ).selected = globals()[name]  
      
  def on_exit(self):
            
    '''
     >>> menu.get_data()   # doctest: +NORMALIZE_WHITESPACE
    {'color': 'blue', 
     'Sec0': {'abc': 1, 'def': 1}, 
     'Set_var': {'width': 1, 'height': 0}, 
     'my_sec': {'a': 1, 'c': 1, 'b': 1, 'm': 1, 'n': 1, 'q': 1, 
                'p': 1, 'r': 1, 'y0': 1, 'x0': 0, 'z0': 1}}
    '''            
    data = self.get_data()
    for k,v in data['Opa Settings'].items():
      globals()[k]=v
      
    print("Leaving OpaMenu.on_exit(), ('IS_TAB_ARGUMENT', 'IS_CHECK_EVENT_ORDER')= ", (IS_TAB_ARGUMENT, IS_CHECK_EVENT_ORDER) 
    )
     
#    for name in item_names:
#      globals()[name]= self.pages[0].get_sec('Opa Settings').get_item( name ).selected             

   
#class Xmenu(sw_menu.Menu):
#  def __init__(self):
#    sw_menu.Menu.__init__(self, dlg_menu)
#  def on_click(self):
#    
#    print('Xmenu clicked! selected= %s/%s/%s'%( 
#          self.page and self.page.name or None 
#          , self.sec and self.sec.name or None
#          , self.item and self.item.name or None))
#          
#    print( 'self.sel_item.keepAlive = %s'%self.item.keepAlive)       
#      
#  def on_menu_exit(self):
#      print('menu.get_data():\n%s'%self.get_data())
#      print('menu.get_data_by_page():\n%s'%self.get_data_by_page())

#  def on_exit(self):
#      print('menu.get_data():\n%s'%self.get_data())
#      print('menu.get_data_by_page():\n%s'%self.get_data_by_page())
#    
 
#. Command
            
class Command:
  '''
    Note 2016.3.23:
    
      doc changes could due to:
      
        1) key press (enter text; ctrl-v; del...)
        2) mouse click=> delete or paste
        
    ----------------------------------------------    
    Note about several event handlers:
                                    
    Let the doc=ed.get_text_all()= "0123456", and i = ed.get_caret_pos() 
    and doc[i] are checked inside each haldner: 
    
    When clicked between "34", the following handlers are called in order. 
    Note that on_caret_move is called twice:
    
                            i  doc[i]
    29) on_caret_move : prev3="123", i=4, doc[4]="4"
    30) on_select     : prev3="123", i=4, doc[4]="4"
    31) on_caret_move : prev3="123", i=4, doc[4]="4"
    32) on_click      : prev3="123", i=4, doc[4]="4"
  
    When enter an 'a':  "0123456"
                        "0123a456"      
    34) on_key        : prev3="123", i=4, doc[4]="4"
    35) on_change     : prev3="123", i=4, doc[4]="a"
    36) on_change     : prev3="123", i=4, doc[4]="a"
    37) on_caret_move : prev3="23a", i=5, doc[5]="4"
      
    Note: I. "a" isn't placed to doc until after on_key;
          II. on_change is called twice AFTER "a" is placed, but BEFORE
              i is moved beyond "a"
          III. on_caret_move is called after i is moved to 5
    
    In short:
    
      0123I456 ==> type "a" ==> on_key() ==> Put "a" on, i hasn't moved  
      ==> 0123Ia456 ==> on_change() ==> on_change() ==> Move I to next 
      ==> 0123aI456 ==> on_caret_move()
            
    No idea why on_caret_move() is called twice in clicking, and 
    on_change() is called twice in key in.
    
    If: click between "45" and drag it between "23" (i.e., select 34):
        
    80) on_caret_move : prev3="234", i=5, doc[5]="5"
    81) on_select     : prev3="234", i=5, doc[5]="5"
    82) on_caret_move : prev3="234", i=5, doc[5]="5"
    
    83) on_caret_move : prev3="123", i=4, doc[4]="4"
    84) on_select     : prev3="123", i=4, doc[4]="4"
    85) on_caret_move : prev3="123", i=4, doc[4]="4"
    
    86) on_caret_move : prev3="012", i=3, doc[3]="3"
    87) on_select     : prev3="012", i=3, doc[3]="3"
    88) on_caret_move : prev3="012", i=3, doc[3]="3"
    
    89) on_click      : prev3="012", i=3, doc[3]="3"
   
    ==> 012[34]56
    
    Then hit "a" to replace "34": doc = "0123456" ==> "012a56"
         
    90) on_key        : prev3="012", i=3, doc[3]="3"
    91) on_change     : prev3="012", i=3, doc[3]="5"
    
    92) on_caret_move : prev3="012", i=3, doc[3]="5"
    93) on_select     : prev3="012", i=3, doc[3]="5"
    94) on_change     : prev3="012", i=3, doc[3]="5"
    95) on_change     : prev3="012", i=3, doc[3]="a"
    96) on_change     : prev3="012", i=3, doc[3]="a"
    97) on_caret_move : prev3="12a", i=4, doc[4]="5" 

      012I(34)56 ==> type "a" ==> on_key() ==> Delete "34" ==> 012I56 
      ==> on_change ==> on_caret_move ==> on_select ==> on_change 
      ==> Put "a" on ==> 012Ia56 
      ==> on_change ==> on_change() ==> move I  
      ==> 012aI56 
      
     
    =============================== click-drag to select text:
    text = ...1234567890...
    click btw 23 and drag right (3=>5) to select 345
    
    10) on_caret_move : prev3="cde", i=795, doc[795]="f"
    11) on_select     : prev3="cde", i=795, doc[795]="f"
    
    12) on_caret_move : prev3=".12", i=801, doc[801]="3"
    13) on_select     : prev3=".12", i=801, doc[801]="3"
    14) on_caret_move : prev3=".12", i=801, doc[801]="3"
    
    15) on_caret_move : prev3="123", i=802, doc[802]="4"
    16) on_select     : prev3="123", i=802, doc[802]="4"
    17) on_caret_move : prev3="123", i=802, doc[802]="4"
    
    18) on_caret_move : prev3="234", i=803, doc[803]="5"
    19) on_select     : prev3="234", i=803, doc[803]="5"
    20) on_caret_move : prev3="234", i=803, doc[803]="5"
    
    21) on_caret_move : prev3="345", i=804, doc[804]="6"
    22) on_select     : prev3="345", i=804, doc[804]="6"
    23) on_caret_move : prev3="345", i=804, doc[804]="6"
    
    24) on_click      : prev3="345", i=804, doc[804]="6"
 
    click btw 5_6 and drag right to left (3<=5) to select 345 :
    
    307) on_select     : prev3=".12", i=801, doc[801]="3"
    
    312) on_caret_move : prev3="345", i=804, doc[804]="6"
    313) on_select     : prev3="345", i=804, doc[804]="6"
    314) on_caret_move : prev3="345", i=804, doc[804]="6"
    
    315) on_caret_move : prev3="234", i=803, doc[803]="5"
    316) on_select     : prev3="234", i=803, doc[803]="5"
    317) on_caret_move : prev3="234", i=803, doc[803]="5"
    
    318) on_caret_move : prev3="123", i=802, doc[802]="4"
    319) on_select     : prev3="123", i=802, doc[802]="4"
    320) on_caret_move : prev3="123", i=802, doc[802]="4"
    
    321) on_caret_move : prev3=".12", i=801, doc[801]="3"
    322) on_select     : prev3=".12", i=801, doc[801]="3"
    323) on_caret_move : prev3=".12", i=801, doc[801]="3"
    
    324) on_click      : prev3=".12", i=801, doc[801]="3"   
     
    ================== hit [Delete] key to delete selected text:
    
    text = "...1234567890..."    
    
    hit [Delete] to del 4 when caret between 3_4
    
    8) on_key        : prev3="123", i=802, doc[802]="4"
    >>> Enter on_key, state=, key=46
    >>>on_key: skey="", key=".", in_call=""
    9) on_2keys      : prev3="123", i=802, doc[802]="4"
    10) on_3keys      : prev3="123", i=802, doc[802]="4"
    11) on_change     : prev3="123", i=802, doc[802]="5"
    12) on_change     : prev3="123", i=802, doc[802]="5"
    
    text = "...1234567890..."    
    
    hit [Delete] to del selected 567
    
    23) on_key        : prev3="567", i=805, doc[805]="8"
    >>> Enter on_key, state=, key=46
    >>>on_key: skey="46", key=".", in_call=""
    24) on_2keys      : prev3="567", i=805, doc[805]="8"
    25) on_3keys      : prev3="567", i=805, doc[805]="8"
    26) on_change     : prev3="123", i=802, doc[802]="8"
    27) on_caret_move : prev3="123", i=802, doc[802]="8"
    28) on_select     : prev3="123", i=802, doc[802]="8"
    29) on_change     : prev3="123", i=802, doc[802]="8"

    ================= minor-click and click on delete to delete selected "345":
   
    text = "...1234567890..."    
    
    236) on_change     : prev3=".12", i=801, doc[801]="6"
    237) on_caret_move : prev3=".12", i=801, doc[801]="6"
    238) on_select     : prev3=".12", i=801, doc[801]="6"
    239) on_change     : prev3=".12", i=801, doc[801]="6"

    ==================== click
    
    text = "... 1234567890 ..."
    When click between 9_0:
    
      if previous selection (on 345) exists, then:
    
        (when previous drag is 5=>3)
        
        95) on_caret_move : prev3=".12", i=801, doc[801]="3"
        96) on_select     : prev3=".12", i=801, doc[801]="3"
        97) on_caret_move : prev3="789", i=808, doc[808]="0"
        98) on_select     : prev3="789", i=808, doc[808]="0"
        99) on_caret_move : prev3="789", i=808, doc[808]="0"
        100) on_click      : prev3="789", i=808, doc[808]="0"

        or (when previous drag is 3=>5):
        
        60) on_caret_move : prev3="345", i=804, doc[804]="6"
        61) on_select     : prev3="345", i=804, doc[804]="6"
        62) on_caret_move : prev3="789", i=808, doc[808]="0"
        63) on_select     : prev3="789", i=808, doc[808]="0"
        64) on_caret_move : prev3="789", i=808, doc[808]="0"
        65) on_click      : prev3="789", i=808, doc[808]="0"
        
      else:    

        78) on_caret_move : prev3="789", i=808, doc[808]="0"
        79) on_select     : prev3="789", i=808, doc[808]="0"
        80) on_caret_move : prev3="789", i=808, doc[808]="0"
        81) on_click      : prev3="789", i=808, doc[808]="0"    
      
    When click between 3_4 (when 345 is selected 3=>5):
  
        142) on_click      : prev3="345", i=804, doc[804]="6"
        143) on_caret_move : prev3="123", i=802, doc[802]="4"
        144) on_caret_move : prev3="123", i=802, doc[802]="4"
        145) on_select     : prev3="123", i=802, doc[802]="4"  
    
    ====================== minor-click and paste

    text = "... 1234567890 ..."
    When caret between 9_0:   
        
    minor-click: Only on_caret_move is called
    
    56) on_caret_move : prev3="789", i=808, doc[808]="0"

    Select paste to paste "345":
    
    57) on_change     : prev3="789", i=808, doc[808]="3"
    58) on_change     : prev3="789", i=808, doc[808]="3"
    59) on_caret_move : prev3="345", i=811, doc[811]="0"
    
    ====================== ctrl-v

    text = "... 1234567890 ..."
    When caret between 9_0, ctrl-v to paste "345":      

    >>> Enter on_key, state=c, key=17
    194) on_key        : prev3="789", i=808, doc[808]="0"
    >>>on_key: skey="c17", key="", in_call=""
    195) on_2keys      : prev3="789", i=808, doc[808]="0"
    196) on_3keys      : prev3="789", i=808, doc[808]="0"

    >>> Enter on_key, state=c, key=86
    197) on_key        : prev3="789", i=808, doc[808]="0"
    >>>on_key: skey="c86", key="V", in_call=""
    198) on_2keys      : prev3="789", i=808, doc[808]="0"
    199) on_3keys      : prev3="789", i=808, doc[808]="0"
    200) on_change     : prev3="789", i=808, doc[808]="3"
    201) on_change     : prev3="789", i=808, doc[808]="3"
    202) on_caret_move : prev3="345", i=811, doc[811]="0"
     
  '''

  def on_caret_move(self, ed_self):
    '''
      Trigger Tab-Argument feature. 2016.2.11
      
      BELOW is OLD DOC
      
      Use on_caret_move to simulate an on_click event 'cos SynWrite
      doesn't seem to have that. 
      
      Issues:
       
      1) If the event handler causes the caret to move, for example,
        inserting or deleting text (in fact even replacing text with
        ed.replace causes caret move), then this on_caret_move will
        be triggered a second time with just one click. 
        
        To overcome this, use a global isToggling flag
        
      2) Sometimes continuous clicking on the same spot is desired
         For example, in our case we want to toggle a text between
        "[ ]" and "[x]" to see its effect on OpenSCAD code. So users
        could click on the same spots back and forth.
        
        But clicking on the same spot the 2nd time doesn't trigger
        on_caret_move 'cos it is not moving. So we add a statement
        in the end of each click:
        
          ed.set_sel(i+3,0)
          
        to move the caret away from the spot after the click. So
        clicking on the same spot the 2nd time will work.
        
        The set caret function,
        
          ed.set_caret_pos(i+3)
          
        should have been working, but it will select from the spot 
        of clicking to the spot it sets, resulting in a highlighting
        that we don't need.       
          
    '''

    check_event_handler_order() 
        ## For debug purpose. The content will be exectued if the global
        ## IS_CHECK_EVENT_ORDER is set to 1 
    
    global skey, in_call, prev_i
    
    set_api() ## set global var api = openscad_api + indoc_api
        
    doc = ed.get_text_all()
    x,y = ed.get_caret_xy()
    i   = ed.get_caret_pos()
    #print( '''>>> prev_i=%s, i=%s'''%(prev_i,i))
    
    ## =========================================================================
    ##
    ## Sets up the "Tab-Arguments" feature
    ##  
    ##  When typing a function name then "(", it triggers
    ##  something to set in_call = func_name, meaning that
    ##  the caret is in an open argument segment. Thus, user 
    ##  can type TAB to auto-enter the next argument name + '='
    ##
    ## =========================================================================
    if ( IS_TAB_ARGUMENT and 
         skey == opa.KEY_TAB and  
         ru.ntCallArgs_at_i(doc,i) and  
         in_call in api.keys() ):
      #print('>>> on_caret_move, ready to run handle_tab_argument...')
      handle_tab_argument(ed_self, doc, i)
      
      #in_call = '' 
    
    elif skey == opa.KEY_PAREN_L: # '('
      '''
      ===============================
      To set in_call to func/mod name when user enter "(". 
      The global skey is set to "s57" (= "(") in on_key.
      ===============================
      '''
      self.on_left_paren(ed_self, doc, i)
          
  def on_left_paren(self, ed_self, doc, i): 
    '''
      Customized event handler when left-parenthesis "(" is entered.
      
      Tasks: 
      1) make sure it's not a func/mod definition like
          "function func(" or "module mod("
      2) make sure it's a call like "func(" or "mod("
      3) if both valid, attach ")" and move caret back in "()"
            
    '''            
    global in_call, skey
    
    def _isdef():
      p_def= ( ru._nc(ru._or('function','module')+ ru._1ms())
              + ru.RE_ID
              +ru._nc(ru._0ms()+'\($') )
      return re.compile( p_def ).search( pdoc )
        
    def _iscall():
      in_call = re.compile( ru.RE_ID+ru._nc(ru._0ms()+'\($')).search( pdoc )
      in_call = in_call and in_call.group()[:-1] or ''  
      return in_call 
      
    pdoc = doc[:i]  
    in_call = _iscall()
    #print('i=%s, pdoc="...%s", isdef=%s'%(i, pdoc[-10:],isdef))
    if not _isdef() and in_call:
        #print( 'Entering the %s() calling arg'%in_call ) 
        print('>>> '+ api[ in_call ].txt )
        skey=''  # Null the skey BEFORE leaving this func. This func is 
                 # called when user hit "(", in which the skey is set to
                 # "s57" in on_key. After handling it, we have to null it
        ed_self.insert(")")
        ed_self.set_caret_pos(i)    

  def on_caret_move_old(self, ed_self):
    '''
      Trigger Tab-Argument feature. 2016.2.11
      
      BELOW is OLD DOC
      
      Use on_caret_move to simulate an on_click event 'cos SynWrite
      doesn't seem to have that. 
      
      Issues:
       
      1) If the event handler causes the caret to move, for example,
        inserting or deleting text (in fact even replacing text with
        ed.replace causes caret move), then this on_caret_move will
        be triggered a second time with just one click. 
        
        To overcome this, use a global isToggling flag
        
      2) Sometimes continuous clicking on the same spot is desired
         For example, in our case we want to toggle a text between
        "[ ]" and "[x]" to see its effect on OpenSCAD code. So users
        could click on the same spots back and forth.
        
        But clicking on the same spot the 2nd time doesn't trigger
        on_caret_move 'cos it is not moving. So we add a statement
        in the end of each click:
        
          ed.set_sel(i+3,0)
          
        to move the caret away from the spot after the click. So
        clicking on the same spot the 2nd time will work.
        
        The set caret function,
        
          ed.set_caret_pos(i+3)
          
        should have been working, but it will select from the spot 
        of clicking to the spot it sets, resulting in a highlighting
        that we don't need.       
          
    '''

    check_event_handler_order() 
        ## For debug purpose. The content will be exectued if the global
        ## IS_CHECK_EVENT_ORDER is set to 1 
    
    global skey, in_call, prev_i
    
    set_api() ## set global var api = openscad_api + indoc_api
        
    doc = ed.get_text_all()
    x,y = ed.get_caret_xy()
    i   = ed.get_caret_pos()
    #print( '''>>> prev_i=%s, i=%s'''%(prev_i,i))
    
    ## =========================================================================
    ##
    ## Sets up the "Tab-Arguments" feature
    ##  
    ##  When typing a function name then "(", it triggers
    ##  something to set in_call = func_name, meaning that
    ##  the caret is in an open argument segment. Thus, user 
    ##  can type TAB to auto-enter the next argument name + '='
    ##
    ## =========================================================================
    if skey == opa.KEY_TAB and in_call in api.keys():
      print('>>> on_caret_move, ready to run handle_tab_argument...')
      handle_tab_argument(ed_self, doc, i)
      #in_call = '' 
    
    elif skey == opa.KEY_PAREN_L: # '('
      '''
      ===============================
      To set in_call to func/mod name when user enter "(". 
      The global skey is set to "s57" (= "(") in on_key.
      ===============================
      '''
      pdoc = doc[:i]  
      in_call = ''
    
      #set_in_call(ed_self, doc, i)
      
      p_def= ( ru._nc(ru._or('function','module')+ ru._1ms())
                + ru.RE_ID
                +ru._nc(ru._0ms()+'\($') )
                ## p_def is pattern to match partial func/mod definition :
                ##   "module mod("  or "function funcname("
      isdef = re.compile( p_def ).search( pdoc )
      print('i=%s, pdoc="...%s", isdef=%s'%(i, pdoc[-10:],isdef))
      if not isdef:
        in_call = re.compile( ru.RE_ID+ru._nc(ru._0ms()+'\($')).search( pdoc )
        in_call = in_call and in_call.group()[:-1] or None  
                  ## [:-1] is to remove "(" 
        if in_call:
          
          print( 'Entering the %s() calling arg'%in_call ) 
          skey=''  # Null the skey BEFORE leaving this func
          ed_self.insert(");")
          ed_self.set_caret_pos(i)    

  def on_change(self, ed_self):
    '''
    
      For a key press like "a", on_change is called after "a" is placed 
      on the doc, but BEFORE the caret moves beyond "a". So if you do:
      
        doc = ed_self.get_text_all()
        i = ed_self.get_caret_pos()
        pdoc = doc[:i]    <==== cut the doc from 0 to before-i   
        
      then pdoc[-1] won't be "a"
      ---------------------------
      NOTE:
      
        When on_change is triggered by on_key, it runs twice:
        
            When enter an 'a':  "0123456"
                        "0123a456"      
            34) on_key        : prev3="123", i=4, doc[4]="4"
            35) on_change     : prev3="123", i=4, doc[4]="a"
            36) on_change     : prev3="123", i=4, doc[4]="a"
            37) on_caret_move : prev3="23a", i=5, doc[5]="4"
      
        But the two times of on_change behaves differently --- funcs below:
          ed_self.get_text_all()
          ed_self.get_caret_pos()
        only works in the 2nd time
            
    '''
  
    check_event_handler_order()
    
    global in_call ## name of func/mod if caret is in its arg like 
                   ##  func(  or mod(, in_call = "func" or "mod", resp.
    global skey ## skey is set by on_key to store key pressed. If skey !='', 
                ## means this change event is triggerred by on_key due to key 
                ## press 
                
    set_api()

  def on_click_dbl(self, ed_self, state):

    check_event_handler_order() 
          
    doc= ed_self.get_text_all()
    i = ed_self.get_caret_pos()
    x,y= ed_self.get_caret_xy()
    #num = ru.ntNum_at_i( doc, i )
    ntPt = ru.ntPt_at_i( doc, i )
    print('double-clicked, i=%s, word="%s", num=%s'%(i, ed_self.get_word(x,y), ntPt))
    if ntPt:
      ed_self.set_sel( ntPt.rng[0], ntPt.rng[1]-ntPt.rng[0] )
       
      
  def on_click(self, ed_self, state):
  
    check_event_handler_order() 

    global skey, in_call  
    ## skey and in_call are used in on_caret_move to trigger 
    ## Tab-Arguments. So if clicks, skey and in_call have to be reset
    ## to either break away from the Tab-Argument mode (when click 
    ## outside args) or enter the Tab-Argument (when click into args)
    skey = ''       
    
    doc= ed_self.get_text_all()
    i = ed_self.get_caret_pos()
    
    in_call = ru.ntCallArgs_at_i(doc,i)
    in_call = in_call and in_call[-1].txt.split('(')[0] or ""
    
    if IS_TOGGLE_BLOCK:
      handle_toggle_block(ed_self)
    return 
    
    #=============================================
    #    set_api()
    #    
    #    doc= ed_self.get_text_all()
    #    i = ed_self.get_caret_pos()
    #    
    #    #check_event_handler_order('on_click')
    #    ntCallArgs = ru.ntCallArgs_at_i(doc,i)
    #      ## ntCallArgs example (when i is inside h(d):
    #      ##  [Blk(typ='Call', rng=(0, 18), rel_rng=(0, 0, 0, 18), txt='f(a= g( b, h(d)) )'), 
    #      ##   Blk(typ='Call', rng=(5, 16), rel_rng=(0, 5, 0, 16), txt='g( b, h(d))'), 
    #      ##   Blk(typ='Call', rng=(11, 15), rel_rng=(0, 11, 0, 15), txt='h(d)')]
    #    in_call= ''
    #    if ntCallArgs:
    #    
    #      #handle_tab_argments( ed_self, doc, i)
    #      ntCallArg = ntCallArgs[-1]
    #      in_call = ntCallArg.txt.split('(')[0].strip() # func or mod name
    #        
    #    print( 'indoc_api=\n', indoc_api )  
    #      
    #    print('ntCallArgs=', ntCallArgs)
    #    pass
    #    return 
    #    
    #    if state=='a':
    #      menu = OpenscadCommandMenu()
    #      menu.show()      
    #      
    #    return

    
    #    menu = Xmenu() #sw_menu.Menu(dlg_menu)
    #    menu.pages[0].ctrl_sec.items[0].isShown = 1
    #    menu.add_page('Page2')
    #    menu.pages[0].add_radioboxes(name='color'
    #                       , item_names=('blue','green'))        
    #    menu.add_radioboxes( name='align'
    #                       , item_names=('left','center','right')
    #                       , page_id= 1)        
    #    menu.add_checkboxes(name='default', item_names=('on','autosave')) 
    #    menu.on_exit = lambda : on_menu_exit(menu)  
    #    
    #    menu.show()
    #    print('>>> exit menu')
    #    print('menu.get_data():\n%s'%menu.get_data())
    #    print('menu.get_data_by_page():\n%s'%menu.get_data_by_page())
                  
  def on_click0(self, ed_self, state):

    ed = ed_self
    doc = ed.get_text_all()
    c,r = ed.get_caret_xy()
    i   = ed.get_caret_pos()
     
    return 
            
    ## Selection before click:
    prev_selc,prev_sellen = ed_self.get_sel()
    prev_selected = ed.get_text_all()[prev_selc:prev_selc+prev_sellen]
    print('\non_click, selected: "%s"'%(prev_selected )) #ed.get_sel()) )
    
    ## Word at moust click
    w_bi, w_len, word_at_i = ed.get_word(*ed.get_caret_xy())  # (i, len, word)
    print( 'word at i= "%s" '%( doc[ w_bi:w_bi+w_len ] ))
    isClickOnPrevSel = w_len and (w_bi,w_len)==(prev_selc,prev_sellen)
    print( 'Current word == prev selection ? ', isClickOnPrevSel )
    
    
    if idu.options['cb_on']:
      
      isAutoSave = idu.options['cb_autosave']      
      
      if isClickOnPrevSel:  
         
         toggleBlock( ed, dlg_menu, MENU_STD
                    , w_bi, w_len, word_at_i
                    , isAutoSave= isAutoSave 
                    , file_save_func= file_save
                    )
      
      elif get_ntBool_at_i(ed):
       
        set_bool_onsite(ed, dlg_menu=dlg_menu, menu_id=MENU_STD
                  , isAutoSave= isAutoSave 
                  , file_save_func= file_save
                  , prec=6)
       
      else:     
        #doc = ed.get_text_all() 
        #i = ed.get_caret_pos()
        #c,r = ed.get_caret_xy()
        #print("SCANNER_RULES['NUM'] = ",SCANNER_RULES['NUM'])
        blk_n_part = get_idu_at_i(ed, SCANNER_RULES['NUM'])      
        ntBlk = ntPart = None
        if blk_n_part: 
          ntBlk,ntPart = blk_n_part
          print( 'idu> Clicked!! ntBlk=%s, ntPart=%s'%blk_n_part) # = ', blk_n_part )
         
        if ntBlk and ntBlk.typ == 'NUM': 
              
          set_val_onsite(ed, dlg_menu, menu_id= MENU_STD
                        , ntNum=ntBlk, isAutoSave=isAutoSave
                        , file_save_func = file_save
                        , prec=6)         

        else:
          
          blk_n_part = get_idu_at_i(ed, SCANNER_RULES['IDU_BLK'])
        
          ntBlk = ntPart = None
          if blk_n_part: 
            ntBlk,ntPart = blk_n_part
            conout( 'idu> Clicked!! ntBlk=%s, ntPart=%s'%blk_n_part) #
            
            if ntPart:
               
              if ntPart.typ=='BTN':
                ##
                ## ntPart.txt is something like '+', or '-', '+3', etc
                ##
                ntBtn = get_ntBtn_at_i(ed) 
                if ntBtn:     
                  if ntBtn.txt in '<>': 
                    a = assign_next_val(ed, ntBlk, ntBtn)
                    if a and isAutoSave: 
                       file_save()                 
                  else:    
                    a= assign_btn_val(ed, ntBlk, ntBtn)
                        
                    if a and isAutoSave: 
                       file_save() 
                      
              elif ntPart.typ == 'NUM_LIST': 
                ##
                ## ntPart.txt is something like [2,3,5] or [2:2:10] or [0:5]
                ##
                a= assign_list_val(ed, ntBlk) 
                if a and isAutoSave: 
                  file_save()        
                 
              elif ntPart.typ=='CHK_BOX':
                 toggle_box(ed, callback=None,
                           callback_scope={}, idu=idu)
                 if isAutoSave: 
                    file_save()        
        
    else:
      
      conout('idu> Clicked. idu is OFF. ctrl-click to turn it ON')


  def on_key(self, ed_self, key, state):
    '''
      NOTE: inside on_key, i = ed.get_caret_pos() is the index of
      the newly entered char. But the char is not actually added to 
      the doc UNTIL AFTER quiting from this on_key(). 
      
      Let I is the caret:
      
      012 345
      abcIdef   => i = ed.get_caret_pos() = 3
                => ed.get_text_all()[i] => "d"
      
      012345
      abcdefI   => i = ed.get_caret_pos() = 6
                => ed.get_text_all()[i] => index out of range
                
      If enter "g":
      
        1) on_key() triggered; state and key represent "g" but still:
                
                => i = ed.get_caret_pos() = 6
                => ed.get_text_all()[i] => index out of range
                         
        2) exit on_key()
        
        3) "g" placed onto the doc:
        
          0123456
          abcdefgI  => i = ed.get_caret_pos() = 7
                    => ed.get_text_all()[i] => index out of range
                            
        4) on_caret_move() triggered, still:
                
                => i = ed.get_caret_pos() = 7
                => ed.get_text_all()[i] => index out of range
         
    '''
    #print('\n>>> Enter on_key, state=%s, key=%s'%(state,key))
    
    global in_call, skey, prev_i, prev_xy, keyhist
    skey = state+str(key)
    check_event_handler_order()
    
    doc= ed_self.get_text_all()
    prev_i = ed.get_caret_pos()
    prev_xy = ed.get_caret_xy()
    keyhist.append( state+str(key) )
    keyhist= len(keyhist)>10 and keyhist[-10:] or keyhist
        
    #print('>>> on_key: skey="%s", key="%s", in_call="%s"'%( skey, KEY_IDX_MAP[key], in_call))
    
    #if skey==opa.KEY_TAB:
    #  set_in_call(ed_self, doc, ) 

      #isdef = isdef and isdef.group() or None
      #print( 'pdoc="%s", isdef=%s, iscall=%s'%(pdoc[-30:],isdef,iscall))                
    
    self.on_2keys( ed_self, keyhist[-2:] )
    self.on_3keys( ed_self, keyhist[-3:] )
    
     
  def on_2keys(self, ed_self, twokeys=[]):
    check_event_handler_order()
    #print('on_2keys, twokeys=%s'%(twokeys))
    if twokeys==[ opa.KEY_CAP, opa.KEY_CAP ]:
      menu = OpenscadCommandMenu()
      menu.show() 
      
    elif twokeys== [opa.KEY_QUESTION,opa.KEY_QUESTION]:
      print ('"??" is just entered') 
      menu = OpaMenu()
      menu.show() 
        
  def on_3keys(self, ed_self, twokeys=[]):
    check_event_handler_order()
    pass
    
  def on_key00(self, ed_self, key, state):	#Called when user presses a key. key is int key code. state is few chars 
    if state=='c':
      print(ed_self.get_sel())          
  
  def on_key0(self, ed_self, key, state):	
    #Called when user presses a key. key is int key code. state is few chars 
    ''' 
      Note: 
      if starting a line with "abc", and hit a "d", then in on_key():
      
      * The arg 'key' will indicate 'd'
      * The x from ed.get_caret_xy() will be 3
      * ed.get_text_line(-1) = "abc"  (without "d")
      
    '''
    ### Uncomment next line to check key/state value:
    # print( 'state= "%s", key=%s'%(state, key) )
    #   ctrl+shift+? => "sc", 191
    #   187 = "="
    
    return
    
    if (key== 187 and  get_prev_chars(ed,2)=='/*'):
      ## When user hits /*=, bring up a list of operator strings
      ##  for user to choose from
      
      MENU_CTRL_LIST = (
          ('=', '')
         ,('=*/', 'Bring up operator menu when clicked')
         ,('={+ -} */', 'Add Buttons')
         ,('={+ - +10 -7 +3 -2 +0.3 -0.2} */', 'Add more Buttons')
         ,('=[1,2,3] {< >} */', 'Add list')
         ,('=[0:2.5:10] {< >} */', 'Add range')
         ,('=[1,2,3] {+ - +10 -7 +3 -2 +0.3 -0.2 } */', 'List+Buttons')
      )    
      ctrl_list = '\n'.join(
                    ('\t'.join(x) for x in MENU_CTRL_LIST)
                    ) 
      m= dlg_menu( MENU_STD
                  , 'Select idu controls'
                  , ctrl_list
                  )        
      if (not str(m)=='None') and m >=0:
        newtxt = MENU_CTRL_LIST[m][0] #+ (m>0 and '*/' or '')
        conout('Inserting text: "%s"'%newtxt)
        ed_self.insert( newtxt )   
                 
      conout( 'm = '+ str(m))  
           
    elif state=='sc' and key== 191: 
      ''' Hit ctrl+shift+? to bring up the idu options. '''
       
      df_vars = ('on','autosave')
      
      ## Add * in front of var name if it is 1 
      ## See doc of dlg_checklist at
      ## http://sourceforge.net/p/synwrite/wiki/py%20functions%20dialog/
      ##
      df_vars_ch = [ (idu.options['cb_'+k] and '*' or '')+k
                     for k in df_vars ] 
                     
      conout('df_vars_ch = ', df_vars_ch )   
                 
      ## cl is [true, false ... ]    
      ##
      cl = dlg_checklist(caption='Set idu default'
           , columns=' \t0' 
           , items= '\n'.join( df_vars_ch) 
           , size_x= 230
           , size_y= 160
           )        
      conout('Current idu.options= ', idu.options)
      
      if cl:     
        for i,k in enumerate(df_vars):
          idu.options['cb_'+k]= cl[i] and 1 or 0 
                          
      conout('cl = ', cl)
      print('New idu.options= ', idu.options)
  
  def on_select0(self, ed_self):
    pass
    

  def on_select(self, ed_self):
    check_event_handler_order()
    
    global skey  # Null the skey
    skey=''
    
    pass 
    #check_event_handler_order('on_select')
              
    
        
  def on_open(self, ed_self): 
    pass 
        
  def on_open_old(self, ed_self): 
    ''' Initiate idu when opening file'''
    
    global api, filename, filepath
    
    filepath, filename = os.path.split( ed_self.get_filename() )
    
    print('\n>>> on_open: Opa (OpenSCAD plugin) opened for\n filename=> "%s", \n filepath=> "%s"'%(filename, filepath))
    #print('in doc apis= \n%s'%( '\n'.join( [x.txt for x in indoc_api])) )
    print(' indoc_api=> %s'%( list(indoc_api.keys() )))
    #parseImportedTopApi( ed_self.get_text_all() )      
    print(' imported_api=> %s'%( list(imported_api.keys() )))
    # set_api()
    
     
#    # test sw_menu.load
#    data= (
#          ('P0',( ('my_sec', (('a',1), ('c',0), ('b',1) ) ), ) # checkbox
#          ) 
#        , ('P1', ( ('color', ( 'green', ('red', 'blue','green') ))   # radiobox
#              , ('Sec0', ( ('abc',1), ('def',1)) ) 
#              , ('Set_var', (('width',1), ('height', 0)) )
#              )
#          )    
#        )   
#    menu= sw_menu.Menu( dlg_menu )
#    menu.load( data )
#    recovered_data = menu.get_loadable_data()
#    recovered_data = [ (k+'_x',v) for k,v in recovered_data ]
#    print('recovered_data = ', recovered_data )
#    menu.load( recovered_data )  
#    menu.show()     


#. SynWrite links
'''
# Py api categories
  http://sourceforge.net/p/synwrite/wiki/python%20API/      
  
# py api funcs home page
  http://sourceforge.net/p/synwrite/wiki/python%20API%20functions/ 
   
# py func dialog
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20dialog/ 
   
# py func misc
  http://sourceforge.net/p/synwrite/wiki/py%20functions%20misc/ 
   
# Install python plugin:
  http://sourceforge.net/p/synwrite/wiki/Plugins%20manual%20installation/ 
  
# Py event handler:
  http://sourceforge.net/p/synwrite/wiki/py%20event%20names/  
  
# app_log(id,..) : id constant
  http://sourceforge.net/p/synwrite/wiki/py%20log%20id/    
  
# msg_box(id) constant
  http://sourceforge.net/p/synwrite/wiki/py%20msgbox%20id/ 
  
# Py Editor class
  http://sourceforge.net/p/synwrite/wiki/py%20Editor%20class/  
  
# SynWrite addon repos
  http://sourceforge.net/projects/synwrite-addons/files/PyPlugins/  
'''  

