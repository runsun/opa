'''
openscad_api_data.py

This is an OpenSCAD api data, to be used by openscad_api_tools.py
in SynWrite editor as a plug-in to display calltip.

ref: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language

by: Runsun Pan 2016.01
'''
from openscad_api_tools import *

def setdata(**kargs):
  '''
    A tool to help with data input. Instead of 
    
      api= {
       "scale":
          Mod("scale"
              , args=[ Li('v', default=[1,1,1]) ]
              )
        , "resize":
          Mod("resize"
              , args=[ Li('newsize', default=[1,1,1]) ]
              )        
        , "rotate":
          Mod("rotate"
              , args=[ Num('a'), Li('v') ]
              )
      }
      
   We can do:
   
      api= setdata(
         scale= Mod( args=[ Li('v', default=[1,1,1]) ] )
       , resize= Mod( args=[ Li('newsize', default=[1,1,1]) ] )        
       , rotate= Mod( args=[ Num('a'), Li('v') ] )
      }
      
   2016/3/29 
   
  >>> api= setdata(
  ...    scale= Mod( args=[ Li('v', default=[1,1,1]) ] )
  ...  , resize= Mod( args=[ Li('newsize', default=[1,1,1]) ] )
  ... )
  
  >>> api   # doctest: +NORMALIZE_WHITESPACE
    {'scale': 
     {"name": "scale", "cat": "mod", "utype": None, "tags": [], "optional": None, "default": None, "hint": "", "doc": "", "args": [{"name": "v", "cat": "var", "utype": "li", "tags": [], "optional": True, "default": [1, 1, 1], "hint": "", "doc": "", "args": None, "ret": None, "ver": None, "rel": []}], "ret": None, "ver": None, "rel": []}, 
     'resize': 
      {"name": "resize", "cat": "mod", "utype": None, "tags": [], "optional": None, "default": None, "hint": "", "doc": "", "args": [{"name": "newsize", "cat": "var", "utype": "li", "tags": [], "optional": True, "default": [1, 1, 1], "hint": "", "doc": "", "args": None, "ret": None, "ver": None, "rel": []}], "ret": None, "ver": None, "rel": []}}
    
  >>> api['scale']['args'][0].name
  'v'
  
    
         
  '''
  #print('In setdata, kargs = '+ str(kargs.keys()) )
  
  #for k,v in kargs.items():
    #print('k = '+ k + ', type= %s'%type(v))
  rtn= {k:v(name=k) for k,v in kargs.items() }
  rtn={}
  for k,v in kargs.items():
    #print('In setdata, k="%s", v.name="%s"'%(k, type(v(name=k))))
    rtn[k] = v(name=k)
    
  return rtn

  
def get_openscad_api_str( unitname ):
  if unitname in openscad_api.keys():
    unit = openscad_api[ unitname ]   
    #print( '/// In get_api(), get unit "'+unit.name+'" : ', str(dir(unit)) )
    return openscad_api[ unitname ].get_api_str()
  
common= setdata( 

  r= Var( utype= "num"  # str|list  
        , optional=True  # True|False
        , default= None 
        , hint='Radius of object'
        )
  ,d= Var( utype= "num"  # str|list  
          , optional=True  # True|False
          , default= None 
          , hint='Diameter of object. Overwrite r.'
          )
  ,h= Var( utype= "num"  # str|list  
          , optional=True  # True|False
          , default= None 
          , hint='Height of object.'
          )
  ,center= Var( name= 'center'
               , utype= 'bool'
               , optional = True 
               , default = 'false'
               , hint= "Set true to center the obj at (0,0,0)"
               , doc =''    
               )
  , convexity= Var( utype = 'i'
                  , optional = True
                  , default = 1
                  , hint = 'max faces a ray crosses, for preview'
                  ) 
  , faces= Var( utype= "v(of 3 i's)"
              , optional = False
              , hint = 'Indices of pts for a face, clockwise'
              )                  
                               
  , fa= Num( tags= ['Special Variable', 'angle']
        , optional= True   # True|False
        , default= 12
        , rel = ['$fs','$fn']
        , hint="Special variable for the minimum angle of a fragment."
        , doc='''Even a huge circle does not have more fragments than 360 divided by this number. The default value is 12 (i.e. 30 fragments for a full circle). The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
        )            
  , fs = Num( tags= ['Special Variable']
          , optional= True   # True|False
          , default= 2
          , rel = ['$fa','$fn']
          , hint = "Special variable for the minimum size of a fragment."
          , doc = '''Because of this variable very small circles have a smaller number of fragments than specified using $fa. The default value is 2. The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
          )
  , fn = Num( tags= ['Special Variable']
          , optional= True   # True|False
          , default= 0
          , rel = ['$fs','$fa']
          , hint = "Special variable controlling the number of fragments." 
          , doc = '''Usually 0. When this variable has a value greater than zero, the other two variables are ignored and full circle is rendered using this number of fragments. The default value is 0.'''
          )    
       
                     
)

globals().update(common)

#r= common['r']
#d= common['d']
#h= common['h']
#center= common['center']
#convexity= common['convexity']
#faces = common['faces']
#
#fa= common['fa']
#fs= common['fs']
#fn= common['fn']


               
#common.update(               
# {               
#  "$fs": Var( name = "$fs"
#            , utype =  "num"
#            , tags= ['Special Variable']
#            , optional= True   # True|False
#            , default= 2
#            , rel = ['$fs','$fn']
#            , hint = "Special variable for the minimum size of a fragment."
#            , doc = '''Because of this variable very small circles have a smaller number of fragments than specified using $fa. The default value is 2. The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
#            )
#            
#  ,"$fn": Var( name = "$fn"
#            , utype =  "num"
#            , tags= ['Special Variable']
#            , optional= True   # True|False
#            , default= 0
#            , rel = ['$fs','$fn']
#            , hint = "Special variable controlling the number of fragments." 
#            , doc = '''Usually 0. When this variable has a value greater than zero, the other two variables are ignored and full circle is rendered using this number of fragments. The default value is 0.'''
#            )
#}
   
#common={
#
#  "r":    Var( name="r"
#              , utype= "num"  # str|list  
#              , optional=True  # True|False
#              , default= None 
#              , hint='Radius of object'
#              )
#  ,"d":   Var( name="d"
#              , utype= "num"  # str|list  
#              , optional=True  # True|False
#              , default= None 
#              , hint='Diameter of object. Overwrite r.'
#              )
#
#  ,"h":   Var( name="h"
#              , utype= "num"  # str|list  
#              , optional=True  # True|False
#              , default= None 
#              , hint='Height of object.'
#              )
#  ,"center": Var( name= 'center'
#               , utype= 'bool'
#               , optional = True 
#               , default = 'false'
#               , hint= "Set true to center the obj at (0,0,0)"
#               , doc =''    
#               )
#                
#
#  ,"$fs": Var( name = "$fs"
#            , utype =  "num"
#            , tags= ['Special Variable']
#            , optional= True   # True|False
#            , default= 2
#            , rel = ['$fs','$fn']
#            , hint = "Special variable for the minimum size of a fragment."
#            , doc = '''Because of this variable very small circles have a smaller number of fragments than specified using $fa. The default value is 2. The minimum allowed value is 0.01. Any attempt to set a lower value will cause a warning.'''
#            )
#            
#  ,"$fn": Var( name = "$fn"
#            , utype =  "num"
#            , tags= ['Special Variable']
#            , optional= True   # True|False
#            , default= 0
#            , rel = ['$fs','$fn']
#            , hint = "Special variable controlling the number of fragments." 
#            , doc = '''Usually 0. When this variable has a value greater than zero, the other two variables are ignored and full circle is rendered using this number of fragments. The default value is 0.'''
#            )
#}    

     
#center = Bool( name= 'center'
#               , optional = True 
#               , default = 'false'
#               , hint= "Set true to center the obj at (0,0,0)"
#               , doc =(
#                  'false: 1st (positive) octant, one corner at (0,0,0)',)
#               )           
#  

openscad_api= setdata(

  cube= Mod( tags='obj, 3d'
           , args= [
                Var( name= 'size'
                   , utype= 'list|num'
                   , optional = True 
                   , default = [1,1,1]
                   , hint= 'Size of the object'
                   , doc= (
                        'single value, cube with all sides this length;' 
                       ,'3 value array [x,y,z], cube with dimensions x,y,z.'
                       )
                   )
                , center
             ]        
          ) 

  , sphere= Mod(  tags='obj, 3d'
           , args=[
                    r(default=1)
                  , d 
                  , fa 
                  , fs 
                  , fn 
                  ]        
          ) 
            
  , cylinder= Mod( tags='obj, 3d'
               , args=[
                        h(default=1)
                      , Num('r1', default=1) 
                      , Num('r2', default=1) 
                      , r 
                      , d 
                      , Num('d1') 
                      , Num('d2') 
                      , center
                      , fa
                      , fs 
                      , fn 
                      ]        
              ) 
              
  , square= Mod( tags= 'obj,2d'
              ,args= [Var( name= 'size'
                         , utype= 'list|num'
                         , optional = True 
                         , default = [1,1]
                         , hint= 'Size of the object'
                         , doc= (
                              'single value, cube with all sides this length;' 
                             ,'3 value array [x,y,z], cube with dimensions x,y,z.'
                             )
                         )
                      , center     
                      ]
             )   
       
  , polyhedron= Mod( tags= 'obj,3d'
                 , args= [ Var( name= 'points'
                              , utype = 'v(of 3 pts)'
                              , optional = False
                              , hint = 'Points of the polyhedron'
                              )
                         , faces
                         , convexity
                         ] 
                 )            
                               
  #===============================================           
  , scale= Mod( args=[ Li('v', default=[1,1,1]) ])
  , resize= Mod( args=[ Li('newsize', default=[1,1,1]) ] )        
  , rotate= Mod( args=[ Num('a'), Li('v') ] )
  , translate= Mod( args = [ Li('v') ] )
  , mirror= Mod( args = [Li( 'v') ] )
  , projection= Mod( args=[ Bool('cut') ])
  , linear_extrude= Mod( 
               args=[ Num('height')
                    , center(hint='Center along z-axis')
                    , convexity(hint='10 is a good value to try')
                    , Num('twist', hint='Degree of twisting')
                    , Num('slices', hint='Increase the resolution') 
                    , Num('scale',utype='list|num', default=1, optional=1
                          , hint='Scale the 2D to form a tapered obj')
                    ])
  , rotate_extrude= Mod(
               args=[ Num('angle')
                    , convexity
                    ]
              , hint='Spin a 2D around z-axis'      
              )
  , multmatrix= Mod(
                args= [ Li('m', hint='A 4x4 matrix') ]
                )
                
  #===============================================           
  , color= Mod( args=[ Var( 'color'
                          , utype='v3|v4|str'
                          , hint='Values are 0~1'
                          )
                     , Num( 'transparency', hint='0~1' )
                     ]) 
  , offset= Mod( args=[ 
                 Num('r', optional=1)
                 ,Num('delta', optional=1)
                 ,Bool('chamfer', hint='Is cut tips')
                 ] )
  , minkowski= Mod(hint='Display the minkowski sum of child nodes')
  , hull= Mod( hint='Display the convex hull of child nodes')  
  , union= Mod()
  , difference= Mod()
  , intersection = Mod()
  , render= Mod()
  #===============================================           
  ,  abs= Func( args=[ Num() ] )
  ,  sign= Func( args=[ Num() ] )
  ,  sin= Func( args=[ Num('degrees') ] )
  ,  cos= Func( args=[ Num('degrees') ] )
  ,  tan= Func( args=[ Num('degrees') ] )
  ,  acos= Func( args=[] )
  ,  asin= Func( args=[] )
  ,  atan= Func( args=[] )
  ,  atan2= Func( args=[] )
  ,  floor= Func( args=[ Num() ] )
  ,  round= Func( args=[ Num() ] )
  ,  ceil= Func( args=[ Num() ] )
  ,  ln= Func( args=[ Num() ] )
  ,  len= Func( args=[] )
  ,  let= Func( args=[] )
  ,  log= Func( args=[] )
  ,  pow= Func( args=[] )
  ,  sqrt= Func( args=[] )
  ,  exp= Func( args=[] )
  ,  rands= Func( args=[] )
  ,  min= Func( args=[] )
  ,  max= Func( args=[] )


                                               
#===================================================          
)

                  
#openscad_api={
#
#  "cube": 
#    Mod( name='cube'
#       , tags='obj, 3d'
#       , args=[
#            Var( name= 'size'
#               , utype= 'list|num'
#               , optional = True 
#               , default = [1,1,1]
#               , hint= 'Size of the object'
#               , doc= (
#                    'single value, cube with all sides this length;' 
#                   ,'3 value array [x,y,z], cube with dimensions x,y,z.'
#                   )
#               )
#            , center
#        ]        
#      ) #cube
#
#  ,"sphere": 
#    Mod( name='sphere'
#       , tags='obj, 3d'
#       , args=[
#                Num('r', default=1)
#              , Num('d') 
#              , fa 
#              , fs 
#              , fn 
#              ]        
#      ) #sphere  
#  , "cylinder":
#    Mod( name='cylinder'
#       , tags='obj, 3d'
#       , args=[
#                Num('h', default=1)
#              , Num('r1', default=1) 
#              , Num('r2', default=1) 
#              , Num('r') 
#              , Num('d') 
#              , Num('d1') 
#              , Num('d2') 
#              , center #Bool('center') #common['center']
#              , fa
#              , fs #common["$fs"]
#              , fn #common["$fn"]
#              ]        
#      ) #cylinder
#  , "square":
#    Mod( name= 'square'
#        ,tags= 'obj,2d'
#        ,args= [Var( name= 'size'
#                   , utype= 'list|num'
#                   , optional = True 
#                   , default = [1,1]
#                   , hint= 'Size of the object'
#                   , doc= (
#                        'single value, cube with all sides this length;' 
#                       ,'3 value array [x,y,z], cube with dimensions x,y,z.'
#                       )
#                   )
#                , center     
#                ]
#       ) #square  
#       
#  , "polyhedron":
#    Mod( name= 'polyhedron'
#       , tags= 'obj,3d'
#       , args= [ Var( name= 'points'
#                    , utype = 'v(of 3 pts)'
#                    , optional = False
#                    , hint = 'Points of the polyhedron'
#                    )
#               , Var( name = 'faces'
#                    , utype= "v(of 3 i's)"
#                    , optional = False
#                    , hint = 'Indices of pts for a face, clockwise'
#                    )
#               , Var( name = 'convexity'
#                    , utype = 'i'
#                    , optional = True
#                    , default = 1
#                    , hint = 'max faces a ray crosses, for preview'
#                    )
#               ]     
#                               
#  #===============================================           
#  , "scale":
#    Mod("scale"
#        , args=[ Li('v', default=[1,1,1]) ]
#        )
#  , "resize":
#    Mod("resize"
#        , args=[ Li('newsize', default=[1,1,1]) ]
#        )        
#  , "rotate":
#    Mod("rotate"
#        , args=[ Num('a'), Li('v') ]
#        )
#  , "translate":
#    Mod("translate"
#        , args = [ Li('v') ]
#        )
#  , "mirror":
#     Mod("mirror"
#         ,args = [Li( 'v') ]                     
#        )
#  , "projection":
#    Mod( 'projection'
#       , args=[ Bool('cut') ]
#       )
#  , "linear_extrude":
#    Mod('linear_extrude'
#       , args=[ Num('height')
#              , common('center')
#              , Num('convexity             
##===================================================          
#} 

#. Doctest
  
def re_test():
  ''' Test the re and token using doctest
  ''' 
  def test_docstr(func): 
      print('>>> doctesting: '+ func.__name__ +'() ')
      doctest.run_docstring_examples(func, globals())                                                
  
  funcs = ( 
          setdata
          ,
          )
  def dumb():pass
  for f in funcs: 
    if type(f)== type(dumb):
      test_docstr( f ) 
    else:
      print('====== %s ======'%f)
      
if __name__=='__main__':
    re_test()